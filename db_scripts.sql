-- START -- Script to make the asana integration work 
USE `iphealthproject`;

-- Add by jorge, store access_token of asana
ALTER TABLE `user` 
ADD COLUMN `asana_key` VARCHAR(1024) NULL AFTER `password`;

-- Add by george, store access_token of g2webinar
ALTER TABLE `user` 
ADD COLUMN `g2webinar_key` VARCHAR(1024) NULL AFTER `asana_key`;

-- Add by george, store access_token of linkedin
ALTER TABLE `user` 
ADD COLUMN `linkedin_key` VARCHAR(1024) NULL;

-- Add by jorge, store access_token of dropbox
ALTER TABLE `user` 
ADD COLUMN `dropbox_key` VARCHAR(1024) NULL AFTER `g2webinar_key`;

-- Add by jorge, store access_token of twitter
ALTER TABLE `user` 
ADD COLUMN `twitter_key` VARCHAR(1024) NULL AFTER `dropbox_key`;

-- Add by jorge, store access_token of evernote
ALTER TABLE `user` 
ADD COLUMN `evernote_key` VARCHAR(1024) NULL AFTER `twitter_key`;

-- Add by jorge, store access_token of slack
ALTER TABLE `user` 
ADD COLUMN `slack_key` VARCHAR(1024) NULL AFTER `evernote_key`;

-- Add by jorge, store access_token of github
ALTER TABLE `user` 
ADD COLUMN `github_key` VARCHAR(1024) NULL AFTER `slack_key`;

-- Add by jorge, store access_token of box
ALTER TABLE `user` 
ADD COLUMN `box_key` VARCHAR(1024) NULL AFTER `github_key`;

-- Add by jorge, store access_token of google drive
ALTER TABLE `user` 
ADD COLUMN `googledrive_key` VARCHAR(1024) NULL AFTER `box_key`;

-- Add by jorge, store access_token of pivotal tracker
ALTER TABLE `user` 
ADD COLUMN `pivotaltracker_key` VARCHAR(1024) NULL AFTER `googledrive_key`;

-- Add by jorge, store access_token of bitbucket
ALTER TABLE `user` 
ADD COLUMN `bitbucket_key` VARCHAR(1024) NULL AFTER `pivotaltracker_key`;

-- Created by jorge, get apis tokens
-- Add by george, column g2webinar_key
-- Add by jorge, column dropbox_key
-- Add by jorge, column twitter_key
-- Add by jorge, column evernote_key
-- Add by george, column linkedin_key
-- Add by jorge, column github_key
-- Add by jorge, column box_key
-- Add by jorge, column googledrive_key
-- Add by jorge, column pivotaltracker_key
-- Add by jorge, column bitbucket_key
DROP PROCEDURE IF EXISTS get_user_by_id;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_by_id`(IN p_uid INT)
BEGIN
		
select id, user_name, name, asana_key, g2webinar_key, 
	   dropbox_key, twitter_key, evernote_key, slack_key, 
	   linkedin_key, github_key, box_key, googledrive_key,
	   pivotaltracker_key, bitbucket_key from user where id = p_uid;

END$$
DELIMITER ;


-- Created by jorge, update asana key
DROP PROCEDURE IF EXISTS update_user_asana_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_asana_key`(IN p_uid INT, IN p_asanaKey VARCHAR(1024))
BEGIN

update user
set asana_key = p_asanaKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update dropbox key
DROP PROCEDURE IF EXISTS update_user_dropbox_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_dropbox_key`(IN p_uid INT, IN p_dropboxKey VARCHAR(1024))
BEGIN

update user
set dropbox_key = p_dropboxKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update twitter key
DROP PROCEDURE IF EXISTS update_user_twitter_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_twitter_key`(IN p_uid INT, IN p_twitterKey VARCHAR(1024))
BEGIN

update user
set twitter_key = p_twitterKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update evernote key
DROP PROCEDURE IF EXISTS update_user_evernote_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_evernote_key`(IN p_uid INT, IN p_evernoteKey VARCHAR(1024))
BEGIN

update user
set evernote_key = p_evernoteKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update slack key
DROP PROCEDURE IF EXISTS update_user_slack_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_slack_key`(IN p_uid INT, IN p_slackKey VARCHAR(1024))
BEGIN

update user
set slack_key = p_slackKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update github key
DROP PROCEDURE IF EXISTS update_user_github_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_github_key`(IN p_uid INT, IN p_githubKey VARCHAR(1024))
BEGIN

update user
set github_key = p_githubKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update box key
DROP PROCEDURE IF EXISTS update_user_box_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_box_key`(IN p_uid INT, IN p_boxKey VARCHAR(1024))
BEGIN

update user
set box_key = p_boxKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update google drive key
DROP PROCEDURE IF EXISTS update_user_googledrive_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_googledrive_key`(IN p_uid INT, IN p_googledriveKey VARCHAR(1024))
BEGIN

update user
set googledrive_key = p_googledriveKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update pivotal tracker key
DROP PROCEDURE IF EXISTS update_user_pivotaltracker_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_pivotaltracker_key`(IN p_uid INT, IN p_pivotalTrackerKey VARCHAR(1024))
BEGIN

update user
set pivotaltracker_key = p_pivotalTrackerKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by jorge, update bitbucket key
DROP PROCEDURE IF EXISTS update_user_bitbucket_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_bitbucket_key`(IN p_uid INT, IN p_bitbucketKey VARCHAR(1024))
BEGIN

update user
set bitbucket_key = p_bitbucketKey
where id = p_uid;

END$$
DELIMITER ;

-- Created by george, update linkedin key
DROP PROCEDURE IF EXISTS update_user_linkedin_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_linkedin_key`(IN p_uid INT, IN p_linkedinKey VARCHAR(1024))
BEGIN

update user
set linkedin_key = p_linkedinKey
where id = p_uid;

END$$
DELIMITER ;

-- Add by george, update g2webinar_key
-- Modify by george, add g2webinar_orgid store id of organizer
DROP PROCEDURE IF EXISTS update_user_g2webinar_key;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_g2webinar_key`(IN uid INT, IN g2webinarKey VARCHAR(128))
BEGIN
		
update user set g2webinar_key= g2webinarKey where id = uid;

END$$
DELIMITER ;

-- Created by jorge, Delete all messages from DB from user
DROP PROCEDURE IF EXISTS x_delete_all_messages;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `x_delete_all_messages`(IN uid INT)
BEGIN

delete from message where user_id = uid;

select * from message where user_id = uid;

END$$
DELIMITER ;
-- END -- Script to make the asana integration work 


-- Start -- Script to make the g2webinar
DROP TABLE IF EXISTS user_meetings;

-- Add by George, store access_token of asana
ALTER TABLE `user` 
ADD COLUMN `xero_key` VARCHAR(1024) NULL;

ALTER TABLE `user` 
ADD COLUMN `xero_privateKey` BLOB NULL;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_xero_auth`(IN pUid INT, IN XeroKey VARCHAR(1024), IN xeroPrivateKey BLOB)
BEGIN

update user set xero_key = XeroKey where id = pUid;
update user set xero_privateKey = xeroPrivateKey where id = pUid;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_userXero_by_id`(IN p_uid INT)
BEGIN
		
select xero_key, xero_privateKey from user where id = p_uid;

END$$
DELIMITER ;

CREATE TABLE user_meetings(
	id integer not null auto_increment,
	uid integer not null,
	organiser_uid integer not null,
	organiser_id varchar(128) not null,
	webinar_key varchar(128) not null,
	join_url varchar(128),
	if_joined integer not null default 0,
	primary key(id),
	constraint fk_um_uid foreign key(uid) references user(id) on delete cascade
) engine=InnoDB DEFAULT charset=utf8;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_meetings`(IN p_uid INT)
BEGIN

select id, organiser_uid, organiser_id, webinar_key, join_url, if_joined from user_meetings where uid = p_uid;

END$$
DELIMITER ;

-- Get All User;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_user`()
BEGIN

select id, user_name, name from user;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_user_meeting`(IN uid INT, IN organiserUid INT, IN organiserId VARCHAR(128), IN webinarKey VARCHAR(128))
BEGIN

insert into user_meetings(uid, organiser_uid,organiser_id, webinar_key) values(uid,organiserUid,organiserId,webinarKey);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_meeting`(IN p_id INT, IN joinUrl VARCHAR(128))
BEGIN

update user_meetings set join_url = joinUrl where id = p_id;
update user_meetings set if_joined = 1 where id = p_id;

END$$
DELIMITER ;

-- Get Branch by unique name - Jorge
DROP PROCEDURE IF EXISTS get_branch_by_name;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_branch_by_name`(IN p_name VARCHAR(250))
BEGIN
		
select id, name from branch where name = p_name;

END$$
DELIMITER ;

