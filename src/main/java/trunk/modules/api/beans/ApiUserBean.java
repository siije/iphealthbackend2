package trunk.modules.api.beans;

public class ApiUserBean extends trunk.modules.user.beans.UserBean {

	private String asana_key;
	private String g2webinar_key;
	private String dropbox_key;
	private String twitter_key;
	private String evernote_key;
	private String xero_key;
	private byte[] xero_privateKey;
	private String slack_key;
	private String linkedin_key;
	private String github_key;
	private String box_key;
	private String googledrive_key;
	private String pivotaltracker_key;
	private String bitbucket_key;
	
	public ApiUserBean(){
		
	}

	public String getAsana_key() {
		return asana_key;
	}

	public void setAsana_key(String asana_key) {
		this.asana_key = asana_key;
	}
	
	public String getG2webinar_key(){
		return g2webinar_key;
	}
	
	public void setG2webinar_key(String g2webinar_key){
		this.g2webinar_key = g2webinar_key;
	}

	public String getDropbox_key() {
		return dropbox_key;
	}

	public void setDropbox_key(String dropbox_key) {
		this.dropbox_key = dropbox_key;
	}

	public String getTwitter_key() {
		return twitter_key;
	}

	public void setTwitter_key(String twitter_key) {
		this.twitter_key = twitter_key;
	}

	public String getEvernote_key() {
		return evernote_key;
	}

	public void setEvernote_key(String evernote_key) {
		this.evernote_key = evernote_key;
	}
	
	public String getXero_key(){
		return xero_key;
	}
	
	public void setXero_key(String xero_key){
		this.xero_key = xero_key;
	}
	
	public byte[] getXero_privateKey(){
		return xero_privateKey;
	}
	
	public void setXero_privateKey(byte[] xero_privateKey){
		this.xero_privateKey = xero_privateKey;
	}
		
	public String getSlack_key() {
		return slack_key;
	}

	public void setSlack_key(String slack_key) {
		this.slack_key = slack_key;
	}
	
	public String getLinkedin_key(){
		return linkedin_key;
	}
	
	public void setLinkedin_key(String linkedin_key){
		this.linkedin_key = linkedin_key;
	}

	public String getGithub_key() {
		return github_key;
	}

	public void setGithub_key(String github_key) {
		this.github_key = github_key;
	}

	public String getBox_key() {
		return box_key;
	}

	public void setBox_key(String box_key) {
		this.box_key = box_key;
	}

	public String getGoogledrive_key() {
		return googledrive_key;
	}

	public void setGoogledrive_key(String googledrive_key) {
		this.googledrive_key = googledrive_key;
	}

	public String getPivotaltracker_key() {
		return pivotaltracker_key;
	}

	public void setPivotaltracker_key(String pivotaltracker_key) {
		this.pivotaltracker_key = pivotaltracker_key;
	}

	public String getBitbucket_key() {
		return bitbucket_key;
	}

	public void setBitbucket_key(String bitbucket_key) {
		this.bitbucket_key = bitbucket_key;
	}
}
