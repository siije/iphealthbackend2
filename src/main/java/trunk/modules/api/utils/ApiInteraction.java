package trunk.modules.api.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

public class ApiInteraction {

	public static String getAccessToken(ApiResultBase apiResult) {
		try {
			URL obj = new URL(apiResult.baseTokenUrl);

			java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj
					.openConnection();

			// Add request header
			con.setRequestMethod("POST");
			con.setConnectTimeout(10000);
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			String urlParameters = apiResult.tokenParamsUrl;

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			// Get response code
			int responseCode = con.getResponseCode();

			// If is correct
			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// Return result
				return response.toString();
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getErrorStream()));
				String inputLine;
				StringBuffer errorResponse = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					errorResponse.append(inputLine);
				}
				in.close();

				return "Error: " + responseCode + " message: " + errorResponse;
			}
		} catch (MalformedURLException e) {
			return "Error: " + e.getMessage();
		} catch (ProtocolException e) {
			return "Error: " + e.getMessage();
		} catch (IOException e) {
			return "Error: " + e.getMessage();
		}
	}

	public static String HTTPRequestPostFile(String serviceUrl,
			MultipartFile file, String authHeader, String accessToken,
			String urlParams, String fileParam) {
		try {
			String urlParamaters = urlParams == null ? "" : urlParams;
			URL obj = new URL(serviceUrl + urlParamaters);

			java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj
					.openConnection();

			// Just generate some unique random value.
			String twoHyphens = "--";
			String crlf = "\r\n";
			String boundary = Long.toHexString(System.currentTimeMillis());

			// Add request header
			con.setRequestMethod("POST");
			con.setConnectTimeout(20000);
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=" + boundary);

			if (authHeader != null && accessToken != null)
				con.setRequestProperty("Authorization", authHeader
						+ accessToken);

			if (file != null && fileParam != null) {

				// Send post request
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(
						con.getOutputStream());
				wr.writeBytes(twoHyphens + boundary + crlf);
				wr.writeBytes("Content-Disposition: form-data; name=\""
						+ file.getName() + "\";filename=\""
						+ file.getOriginalFilename() + "\";file=\""
						+ file.getOriginalFilename() + "\"" + crlf);
				wr.writeBytes(crlf);
				wr.write(file.getBytes());
				wr.writeBytes(crlf);
				wr.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
				wr.flush();
				wr.close();
			}

			// Get response code
			int responseCode = con.getResponseCode();

			// If is correct
			if (responseCode == 200 || responseCode == 204) {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// Return result
				return response.toString();
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getErrorStream()));
				String inputLine;
				StringBuffer errorResponse = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					errorResponse.append(inputLine);
				}
				in.close();

				return "Error: " + responseCode + " message: " + errorResponse;
			}
		} catch (MalformedURLException e) {
			return "Error: " + e.getMessage();
		} catch (ProtocolException e) {
			return "Error: " + e.getMessage();
		} catch (IOException e) {
			return "Error: " + e.getMessage();
		}
	}

	public static <T> String HTTPRequest(String serviceUrl,
			String httpRequestMethod, T content, String authHeader,
			String accessToken, String params, String contentType,
			boolean customHeader, OAuthConsumer consumer) {
		try {
			String urlParameters = params == null ? "" : params;
			URL obj = new URL(serviceUrl + urlParameters);

			java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj
					.openConnection();

			// Add request header
			con.setRequestMethod(httpRequestMethod);
			con.setConnectTimeout(20000);
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setRequestProperty("Accept", "application/json");

			if (consumer != null) {
				try {
					consumer.sign(con);
				} catch (OAuthMessageSignerException e) {
					return "Error: " + e.getMessage();
				} catch (OAuthExpectationFailedException e) {
					return "Error: " + e.getMessage();
				} catch (OAuthCommunicationException e) {
					return "Error: " + e.getMessage();
				} catch (Exception e) {
					return "Error: " + e.getMessage();
				}
			}

			if (authHeader != null && accessToken != null && !customHeader)
				con.setRequestProperty("Authorization", authHeader
						+ accessToken);
			else if (authHeader != null && accessToken != null && customHeader)
				con.setRequestProperty(authHeader, accessToken);

			if (contentType != null) {
				con.setRequestProperty("Content-Type", contentType);
			} else if (httpRequestMethod == "GET" && contentType == null) {
				con.setRequestProperty("Content-Type",
						"application/x-www-form-urlencoded");
			} else if (contentType == null) {
				con.setRequestProperty("Content-Type", "application/json");
			}

			if (content != null
					&& contentType == "text/xml"
					&& (httpRequestMethod == "POST" || httpRequestMethod == "PUT")) {
				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(
						con.getOutputStream());
				wr.writeBytes((String) content);
				wr.flush();
				wr.close();
				urlParameters = (String) content;
			} else if (content != null) {

				// Already JSON object
				if (content.getClass() != String.class) {
					Gson gson = new Gson();
					urlParameters = gson.toJson(content);
				} else {
					urlParameters = (String) content;
				}

				con.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(
						con.getOutputStream());
				wr.writeBytes(urlParameters);
				wr.flush();
				wr.close();
			}

			// Get response code
			int responseCode = con.getResponseCode();

			// If is correct
			if (responseCode == 200 || responseCode == 201) {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// Return result
				return response.toString();
			} else if (responseCode == 400 || responseCode == 401) {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getErrorStream()));
				String inputLine;
				StringBuffer errorResponse = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					errorResponse.append(inputLine);
				}
				in.close();

				return "Error: " + responseCode + " message: " + errorResponse
						+ " data: " + urlParameters;
			} else {
				return "Error: " + responseCode + " message: "
						+ con.getResponseMessage() + " data: " + urlParameters;
			}
		} catch (MalformedURLException e) {
			return "Error: " + e.getMessage();
		} catch (ProtocolException e) {
			return "Error: " + e.getMessage();
		} catch (IOException e) {
			return "Error: " + e.getMessage();
		}
	}
}
