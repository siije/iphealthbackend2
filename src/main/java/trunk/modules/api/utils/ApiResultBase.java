package trunk.modules.api.utils;

public abstract class ApiResultBase {

	protected String appName ;
	protected String clientId;
	protected String clientSecret;
	protected String redirectUrl; 
	protected String baseAuthUrl;
	protected String baseTokenUrl;
	protected String authUrl;
	protected String tokenParamsUrl;
	protected String authCode;
	protected String accessToken;
	
	public ApiResultBase(){
		
	}
	
	protected abstract void generateAuthUrl();
	public abstract void generateTokenParamsUrl(boolean renew, String refreshToken);
	
	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getBaseAuthUrl() {
		return baseAuthUrl;
	}

	public void setBaseAuthUrl(String baseAuthUrl) {
		this.baseAuthUrl = baseAuthUrl;
	}

	public String getAuthUrl() {
		return authUrl;
	}

	public void setAuthUrl(String authUrl) {
		this.authUrl = authUrl;
	}
	
	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getBaseTokenUrl() {
		return baseTokenUrl;
	}

	public void setBaseTokenUrl(String baseTokenUrl) {
		this.baseTokenUrl = baseTokenUrl;
	}

	public String getTokenParamsUrl() {
		return tokenParamsUrl;
	}

	public void setTokenParamsUrl(String tokenParamsUrl) {
		this.tokenParamsUrl = tokenParamsUrl;
	}
}
