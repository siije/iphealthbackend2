package trunk.modules.alternative.login;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.module.SimpleModule;
import org.springframework.stereotype.Controller;

import trunk.Providers;
import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.auth.base.IAuthProvider;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.UIElement;
import trunk.base.module.ui.UIElementSerializer;
import trunk.base.store.ModuleStore;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.alternative.login.dao.AlternativeLoginDao;
import trunk.modules.authentication.beans.UIPermission;
import trunk.modules.user.dao.UserDao;

@Controller
public class AlternativeLoginController extends TrunkModuleBase implements
		IAlternativeLoginController {

	@Override
	public String verifyLogin(String name, String email,
			String branchName, HttpServletRequest req) {

		IAuthProvider authProv = Providers.AUTHENTICATION;

		User u = null;
		if (SessionProvider.hasUser(req.getSession())) {
			// Log out
			SessionProvider.clearSession(req.getSession());
		}

		String userName = email;
		String pass = name.toUpperCase() + "_#_" + name.toLowerCase(); // Possible security flaw
		
		// Not logged in, create a new user
		AlternativeLoginDao fDao = (AlternativeLoginDao) ApplicationContextProvider.getContext()
				.getBean(AlternativeLoginDao.class);
					
		int branchId = fDao.getBranchInfoByUniqueName(branchName).getId();
		
		u = new User(userName, pass, branchName);

		if (!authProv.login(u)) {

			// Not logged in, create a new user
			UserDao uDao = (UserDao) ApplicationContextProvider.getContext()
					.getBean(UserDao.class);
			try {
				uDao.createAUser(false, name, userName, pass, branchId);
				
				if (!authProv.login(u))
				{
					return "Not authorized";
				}
			} catch (Exception ex) {
				return ex.getCause().getMessage();
			}
		}

		HashMap<String, ArrayList<String>> am = authProv.getAllowedModules(u);

		ArrayList<UIPermission> ups = new ArrayList();
		for (String m : am.keySet()) {
			ModuleUI ui = ModuleStore.getModuleImplementationFromId(m)
					.getModuleUI();
			UIPermission uip = new UIPermission(ui, m, (ArrayList) am.get(m));
			ups.add(uip);
		}
		SessionProvider.addUser(u, req.getSession());

		return serialize(ups, u, authProv, am);
	}

	private String serialize(ArrayList<UIPermission> o, User u,
			IAuthProvider ap, HashMap<String, ArrayList<String>> uaf) {
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule("UIS", new Version(0, 1, 0, "1"));
		module.addSerializer(UIElement.class, new UIElementSerializer(u, ap,
				uaf));
		mapper.registerModule(module);
		try {
			return mapper.writeValueAsString(o);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}
