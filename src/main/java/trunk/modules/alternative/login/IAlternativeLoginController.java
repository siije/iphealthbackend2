package trunk.modules.alternative.login;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;

@RequestMapping("/alternativeLogin")
@TrunkModuleFeature(id = "alternativeLogin", displayName = "Alternative Login", enabled = true, freeAccess = true)
public interface IAlternativeLoginController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "verify", displayName = "Alternative Login Check")
	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	@ResponseBody
	String verifyLogin(
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "branchName", required = true) String branchName,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
