package trunk.modules.alternative.login.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.alternative.login.maps.BranchInfo;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class AlternativeLoginDao extends JdbcDaoSupportBase {
	public BranchInfo getBranchInfoByUniqueName(String uid) {
		return invokeStoredProc(BranchInfo.class, "get_branch_by_name",
				new StoredProcParam(Types.VARCHAR, "uid", uid));		
	}
}

