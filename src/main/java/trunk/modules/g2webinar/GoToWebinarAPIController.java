package trunk.modules.g2webinar;

import java.lang.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SingleSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.base.store.ModuleStore;
import trunk.base.store.TrunkFeature;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.g2webinar.beans.*;
import trunk.modules.g2webinar.admin.GoToWebinarAPIInterceptor;
import trunk.modules.g2webinar.dao.GoToWebinarAPIDao;
import trunk.modules.api.beans.*;

@Controller
public class GoToWebinarAPIController extends TrunkModuleBase implements IGoToWebinarAPIController {
 	private BufferingClientHttpRequestFactory bufferingClientHttpRequestFactory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
	private RestTemplate restTemplate = new RestTemplate(bufferingClientHttpRequestFactory);
	
	@Override
	public List<UserMeetingBean> getUserInvitedMeeting(HttpServletRequest req){
		List<UserMeetingBean> userMeetings = new ArrayList<UserMeetingBean>();
		
		try{	
			GoToWebinarAPIDao g2webinarDao = ApplicationContextProvider.getContext().getBean(GoToWebinarAPIDao.class);
			userMeetings = g2webinarDao.getG2WebinarUserInvitedMeetings(SessionProvider.getUser(req).getId());
		
		}catch(HttpStatusCodeException exception){
		}catch(NullPointerException exception){}
		
		return userMeetings;
	}
	
	@Override
	public UserMeetingBean inviteUserIntoMeeting(int uid, String organiserId, String webinarKey,HttpServletRequest req){
		UserMeetingBean userMeeting = new UserMeetingBean();
		
		try{
			GoToWebinarAPIDao g2webinarDao = ApplicationContextProvider.getContext().getBean(GoToWebinarAPIDao.class);
			g2webinarDao.saveG2WebinarUserInvitedMeeting(uid, SessionProvider.getUser(req).getId(), organiserId, webinarKey);
			
			userMeeting.setOrganiserUid(SessionProvider.getUser(req).getId());
			userMeeting.setOrganiserId(organiserId);
			
		}catch(HttpStatusCodeException exception){
		}catch(NullPointerException exception){}
		
		return userMeeting;
	}
	
	@Override
	public List<UserHereBean> getAllUser(){
		List<UserHereBean> users = new ArrayList<UserHereBean>();
		
		try{
			
			GoToWebinarAPIDao g2webinarDao = ApplicationContextProvider.getContext().getBean(GoToWebinarAPIDao.class);
			users = g2webinarDao.getAllUser();
			
		}catch(HttpStatusCodeException exception){
		}catch(NullPointerException exception){}
		
		return users;
	}
	
	@Override
	public UserBean getOAuth(int uid, HttpServletRequest req){
		if (uid == 0){
			uid = SessionProvider.getUser(req).getId();
		}
		
		UserBean g2wUser= new UserBean();
		
		try{
			GoToWebinarAPIDao g2webinarDao = ApplicationContextProvider.getContext().getBean(GoToWebinarAPIDao.class);
			ApiUserBean g2webinarUser = g2webinarDao.getG2WebinarUserAuthKey(uid);
			
			String accessToken_orgId = g2webinarUser.getG2webinar_key();
			if (accessToken_orgId != null){
				g2wUser.setStatus(true);
				g2wUser.setAccessToken(accessToken_orgId.split("-")[0]);
				g2wUser.setOrganizerId(accessToken_orgId.split("-")[1]);
			} else{
				g2wUser.setStatus(false);
			}
				
		}catch(HttpStatusCodeException exception){
		}catch(NullPointerException exception){}
		
		return g2wUser;
	}
			
	@Override
	public UserBean oAuthConfig(String mail, String pwd, HttpServletRequest req){
		UserBean g2wUser= new UserBean();
		String url = "https://api.citrixonline.com//oauth/access_token?grant_type=password&client_id={clientId}&user_id={userId}&password={password}";
		
		try {
			Map user = restTemplate.getForObject(url, Map.class, g2wUser.getClientId(), mail, pwd);
			
			GoToWebinarAPIDao g2webinarDao = ApplicationContextProvider.getContext().getBean(GoToWebinarAPIDao.class);
			
			g2wUser.setStatus(true);
			g2wUser.setAccessToken((String) user.get("access_token"));
			g2wUser.setOrganizerId((String) user.get("organizer_key"));
			
			g2webinarDao.saveG2WebinarUserAuthKey(SessionProvider.getUser(req).getId(), g2wUser.getAccessToken() +"-"+ g2wUser.getOrganizerId());
		} catch (HttpStatusCodeException exception) {
			g2wUser.setStatus(false);
		}
		
		return g2wUser;
	}
	
	@Override 
	public MeetingBean getSingleMeeting(int organiserUid,String organiserId, String webinarKey,HttpServletRequest req){
		UserBean g2wUser = getOAuth(organiserUid, req);
		
		String url = "https://api.citrixonline.com/G2W/rest/organizers/" + organiserId + "/webinars/" + webinarKey;
		restTemplate.setInterceptors(Collections.<ClientHttpRequestInterceptor>singletonList(new GoToWebinarAPIInterceptor(g2wUser.getAccessToken())));
		
	    Map meeting = restTemplate.getForObject(url, Map.class);
	    MeetingBean mt = new MeetingBean();
	    
	    mt.setTimeZone((String) meeting.get("timeZone"));
    	mt.setSubject((String) meeting.get("subject"));
    	mt.setDescription((String) meeting.get("description"));
    	mt.setTimes((List<Map<String,String>>) meeting.get("times"));
    	mt.setWebinarKey((String) meeting.get("webinarKey").toString());
    	mt.setRegistrationUrl((String) meeting.get("registrationUrl"));
    	mt.setOrganiserId(meeting.get("organizerKey").toString());
	    
	    return mt;
	}
	
	@Override
	public List<MeetingBean> listMeetings(HttpServletRequest req) {
		/*
		 * this part will be changed;
		 */
		UserBean g2wUser = getOAuth(0,req);
		
		String url = "https://api.citrixonline.com/G2W/rest/organizers/" + g2wUser.getOrganizerId() + "/upcomingWebinars";
		restTemplate.setInterceptors(Collections.<ClientHttpRequestInterceptor>singletonList(new GoToWebinarAPIInterceptor(g2wUser.getAccessToken())));
		
	    List<Map> meetings = restTemplate.getForObject(url, List.class);
	    
	    //Save into object "MeetingBean"
	    List<MeetingBean> mts = new ArrayList<MeetingBean>();
	    
	    ListIterator<Map> litr = meetings.listIterator();
	    while (litr.hasNext()){
	    	Map meeting = litr.next();
	    	MeetingBean mt = new MeetingBean();
	    	
	    	mt.setTimeZone((String) meeting.get("timeZone"));
	    	mt.setSubject((String) meeting.get("subject"));
	    	mt.setDescription((String) meeting.get("description"));
	    	mt.setTimes((List<Map<String,String>>) meeting.get("times"));
	    	mt.setWebinarKey((String) meeting.get("webinarKey").toString());
	    	mt.setRegistrationUrl((String) meeting.get("registrationUrl"));
	    	mt.setOrganiserId(meeting.get("organizerKey").toString());
	    	
	    	mts.add(mt);
	    }
	    
	    return mts;
	}
	
	@Override
	public List<RegistrantBean> getRegistrants(Long webinarKey, HttpServletRequest req){
		/*
		 * this part will be changed;
		 */
		UserBean g2wUser= getOAuth(0, req);
		
		String url = "https://api.citrixonline.com/G2W/rest/organizers/{organizerKey}/webinars/{webinarKey}/registrants";
		restTemplate.setInterceptors(Collections.<ClientHttpRequestInterceptor>singletonList(new GoToWebinarAPIInterceptor(g2wUser.getAccessToken())));
		
		List<Map> registrantMap = restTemplate.getForObject(url, List.class, g2wUser.getOrganizerId(), webinarKey.toString());
		
		List<RegistrantBean> rgts = new ArrayList<RegistrantBean>();
		
		ListIterator<Map> litr = registrantMap.listIterator();
	    while (litr.hasNext()){
	    	Map registrant = litr.next();
	    	RegistrantBean rgt = new RegistrantBean();
	    	
	    	rgt.setFirstName((String) registrant.get("firstName"));
	    	rgt.setLastName((String) registrant.get("lastName"));
	    	rgt.setRegistrantEmail((String) registrant.get("email"));
	    	rgt.setRegistrantKey((String) registrant.get("registrantKey").toString());
	    	rgt.setRegistrantDate((String) registrant.get("registrationDate")); 
	    	rgt.setJoinUrl((String) registrant.get("joinUrl"));
	    	
	    	rgts.add(rgt);
	    }
	    
		return rgts;
	}
	
	@Override
	public RegistrantBean joinMeeting(int id, int organiserUid, String lastName, String firstName, String mail, String webinarKey, HttpServletRequest req){
		RegistrantBean registrant = new RegistrantBean();
		UserBean g2wUser = getOAuth(organiserUid, req);
		
		Map<String, String> postData = new HashMap<String, String>();
		postData.put("lastName", lastName);
		postData.put("firstName", firstName);
		postData.put("email", mail);
		
		String url = "https://api.citrixonline.com/G2W/rest/organizers/" + g2wUser.getOrganizerId() + "/webinars/" + webinarKey+ "/registrants";
		restTemplate.setInterceptors(Collections.<ClientHttpRequestInterceptor>singletonList(new GoToWebinarAPIInterceptor(g2wUser.getAccessToken())));
		
		HttpEntity<Map> httpEntity = new HttpEntity<Map>(postData);
		ParameterizedTypeReference<Map> typeRef = new ParameterizedTypeReference<Map>() {};
		ResponseEntity<Map> response = null;
		response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, typeRef);
		
		// Update the status of joining meeting 
		try{
			GoToWebinarAPIDao g2webinarDao = ApplicationContextProvider.getContext().getBean(GoToWebinarAPIDao.class);
			g2webinarDao.updateG2WebinarUserJoinMeetingStatus(id, (String) response.getBody().get("joinUrl"));
			
			registrant.setJoinUrl((String) response.getBody().get("joinUrl"));
		}catch(HttpStatusCodeException exception){
		}catch(NullPointerException exception){}
		
		
		return registrant;
	}
	
	@Override
	public void initializeUI(ModuleUI ui) {
		ui.addJsFile(new JsFile(ui, "/g2webinarApi.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-plus-square");
		msm.setName("G2Webinar API");
		msm.setId("g2webinarManues");
		
		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setHref("/g2webinarApiMyWebinars");
		ssm.setName("My Webinars ");
		ssm.setTemplate("/myWebinar/g2webinarApiMyWebinars.html");
		ssm.setId("g2wMyWebinars");
		
		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setHref("/g2webinarApiMyMeetings");
		ssm2.setName("My Meetings ");
		ssm2.setTemplate("/myMeeting/g2webinarApiMyMeetings.html");
		ssm2.setId("g2wMyMeetings");
		
		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
		ui.addSideMenu(msm);
	}
}