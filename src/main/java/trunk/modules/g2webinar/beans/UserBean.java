package trunk.modules.g2webinar.beans;

import java.util.List;
import java.util.Map;

import trunk.modules.api.beans.ApiUserBean;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
public class UserBean extends ApiUserBean{
	private String mail;
	private boolean status;
	private String accessToken;
	private String organizerId;
	private String clientId = "nqw2dyrQYEKz17qmsdt0A6lbGDryiQIq"; // developer key
	
	/*
	 * A default public empty constructor has to be present for json
	 * serialization to work
	 */
	public UserBean() {
	}
	
	public void setMail(String mail){
		this.mail = mail;
	}
	
	public String getMail(){
		return mail;
	}
	
	public String getClientId(){
		return clientId;
	}
	
	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}
	
	public String getAccessToken(){
		return accessToken;
	}
	
	public void setOrganizerId(String organizerId){
		this.organizerId = organizerId;
	}
	
	public String getOrganizerId(){
		return organizerId;
	}
	
	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean getStatus(){
		return status;
	}
}
