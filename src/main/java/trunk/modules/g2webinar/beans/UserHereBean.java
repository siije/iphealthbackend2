package trunk.modules.g2webinar.beans;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
public class UserHereBean{
	private int id;
	private String user_name;
	private String name;
	
	/*
	 * A default public empty constructor has to be present for json
	 * serialization to work
	 */
	public UserHereBean() {
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return id;
	}
	
	public String getUserName(){
		return user_name;
	}
	
	public void setUserName(String userName){
		this.user_name = userName;
	}
	
	public String getName(){
		return name;
	}
	
	public void Name(String name){
		this.name = name;
	}
	
}
