package trunk.modules.g2webinar.beans;

import java.util.List;
import java.util.Map;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
public class MeetingBean {
	private int numberOfRegistrants = 0;
	private List<Map<String,String>> times;
	private String description;
	private String subject = "No Change";
	private boolean inSession;
	private String organiserId;
	private String webinarKey;
	private String timeZone;
	private int numberOfOpenedInvitations = 0;
	private String registrationUrl;

	/*
	 * A default public empty constructor has to be present for json
	 * serialization to work
	 */
	public MeetingBean() {
	}
	
	public void setTimes(List<Map<String,String>> time){
		this.times = time;
	}
	
	public List<Map<String,String>> getTimes(){
		return times;
	}
	
	public void setTimeZone(String timeZone){
		this.timeZone = timeZone;
	}
	
	public String getTimeZone(){
		return timeZone;
	}
	
	public void setSubject(String subj) {
		this.subject = subj;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setDescription(String desc){
		this.description = desc;
	}
	
	public String getDescription(){
		return description;
	}
	
	public void setWebinarKey(String webinarKey){
		this.webinarKey = webinarKey;
	}
	
	public String getWebinarKey(){
		return webinarKey;
	}
	
	public void setRegistrationUrl(String regUrl){
		this.registrationUrl = regUrl;
	}

	public String getRegistrationUrl(){
		return registrationUrl;
	}
	
	public void setOrganiserId(String organiserId){
		this.organiserId = organiserId;
	}
	
	public String getOrganiserId(){
		return organiserId;
	}
}
