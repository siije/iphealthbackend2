package trunk.modules.g2webinar.beans;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
public class UserMeetingBean{
	private int id;
	private int organiser_uid; // user id in this template
	private String organiser_id;
	private String webinar_key;
	private String join_url;
	private int if_joined;
	private String clientId = "nqw2dyrQYEKz17qmsdt0A6lbGDryiQIq"; // developer key
	
	/*
	 * A default public empty constructor has to be present for json
	 * serialization to work
	 */
	public UserMeetingBean() {
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return id;
	}
	
	public void setOrganiserUid(int uid){
		this.organiser_uid = uid;
	}
	
	public int getOrganiserUid(){
		return organiser_uid;
	}
	
	public String getClientId(){
		return clientId;
	}
	
	public void setWebinarKey(String webinarKey){
		this.webinar_key = webinarKey;
	}
	
	public String getWebinarKey(){
		return webinar_key;
	}
	
	public void setOrganiserId(String organiser_id){
		this.organiser_id = organiser_id;
	}
	
	public String getOrganiserId(){
		return organiser_id;
	}
	
	public void setJoinUrl(String joinUrl){
		this.join_url = joinUrl;
	}

	public String getJoinUrl(){
		return join_url;
	}
	
	public void setIfJoined(int ifJoined){
		this.if_joined = ifJoined;
	}

	public int getIfJoined(){
		return if_joined;
	}
}

