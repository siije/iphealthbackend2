package trunk.modules.g2webinar.beans;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
public class RegistrantBean {
	private String sessionKey;
	private String registrantKey;
	private String registrantDate;
	private String firstName;
	private String lastName;
	private String registrantEmail;
	private String joinUrl;

	public void setSessionKey(String sessionKey){
		this.sessionKey = sessionKey;
	}
	
	public String getSessionKey(){
		return sessionKey;
	}
	
	public void setRegistrantKey(String registrantKey){
		this.registrantKey = registrantKey;
	}
	
	public String getRegistrantKey(){
		return registrantKey;
	}
	
	public void setRegistrantDate(String registrantDate){
		this.registrantDate = registrantDate;
	}
	
	public String getRegistrantDate(){
		return registrantDate;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public void setRegistrantEmail(String registrantEmail){
		this.registrantEmail = registrantEmail;
	}
	
	public String getRegistrantEmail(){
		return registrantEmail;
	}

	public void setJoinUrl(String joinUrl){
		this.joinUrl = joinUrl;
	}
	
	public String getJoinUrl(){
		return joinUrl;
	}
}