package trunk.modules.g2webinar.admin;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

/**
 * The {@link AdminInterceptor} class intercepts each call to the Admin API and performs common actions for each call.
 */
public class GoToWebinarAPIInterceptor implements ClientHttpRequestInterceptor {

    private String token;

    public GoToWebinarAPIInterceptor(String token) {
        this.token = token;
    }

    @Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        // each request to the API requires a valid token, therefore add the Authorization header
        HttpHeaders headers = request.getHeaders();
        headers.set("Authorization", token);

        // execute the request
        ClientHttpResponse response = execution.execute(request, body);

        // log request and response to standard out
        log(request, body, response);

        return response;
    }

    /**
     * Will create multiple log lines per request with the status code and the body of the request/response.
     */
    private void log(HttpRequest request, byte[] body, ClientHttpResponse response) throws IOException {
        StringBuilder sb = new StringBuilder(100);
        String requestBody = new String(body, Charset.defaultCharset());

        String responseBody = null;
        InputStream responseStream = response.getBody();
        if (responseStream != null) {
            try {
                responseBody = StreamUtils.copyToString(responseStream, Charset.defaultCharset());
            } catch (IOException e) {}
        }

        sb.append("method=" + request.getMethod())
                .append("\n")
                .append("uri=" + request.getURI())
                .append("\n")
                .append("requestBody=" + ((requestBody == null || requestBody.length() == 0) ? "none" : requestBody))
                .append("\n")
                .append("- - -")
                .append("\n")
                .append("responseCode=" + response.getStatusCode())
                .append("\n")
                .append("responseBody=" + ((responseBody == null || responseBody.length() == 0) ? "none" : responseBody))
                .append("\n")
                .append("\n");

        System.out.println(sb.toString());
    }
}
