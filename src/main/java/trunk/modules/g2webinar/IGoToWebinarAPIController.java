package trunk.modules.g2webinar;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.g2webinar.beans.*;

@RequestMapping("/g2webinar")
@TrunkModuleFeature(id = "g2webinar", displayName = "GoToWebinar API Tester")
public interface IGoToWebinarAPIController extends ITrunkModuleBase {
	
	@RequestMapping(value = "/listMeetings", method = RequestMethod.POST)
	@ResponseBody
	List<MeetingBean> listMeetings(
		@RequestParam(value = "req", required = true) HttpServletRequest req
	);
	
	@RequestMapping(value = "/getSingleMeeting", method = RequestMethod.POST)
	@ResponseBody
	MeetingBean getSingleMeeting(
		@RequestParam(value = "organiserUid", required = true) int organiserUid,
		@RequestParam(value = "organiserId", required = true) String organiserId,
		@RequestParam(value = "webinarKey", required = true) String webinarKey,
		@RequestParam(value = "req", required = true) HttpServletRequest req
	);
	
	@RequestMapping(value = "/getRegistrants", method = RequestMethod.POST)
	@ResponseBody
	List<RegistrantBean> getRegistrants(
		@RequestParam(value = "webinarKey", required = true) Long webinarKey,
		@RequestParam(value = "req", required = true) HttpServletRequest req
	);
	
	@RequestMapping(value = "/oAuthConfig", method = RequestMethod.POST)
	@ResponseBody
	UserBean oAuthConfig(
		@RequestParam(value = "mail", required = true) String mail,
		@RequestParam(value = "pwd", required = true) String pwd,
		@RequestParam(value = "req", required = true) HttpServletRequest req
	);
	
	@RequestMapping(value = "/getOAuth", method = RequestMethod.POST)
	@ResponseBody
	UserBean getOAuth (
		@RequestParam(value = "uid", required = true) int uid,
		@RequestParam(value = "req", required = true) HttpServletRequest req
	);
	
	@RequestMapping(value = "/joinMeeting", method = RequestMethod.POST)
	@ResponseBody
	RegistrantBean joinMeeting (
		@RequestParam(value = "id", required = true) int id,
		@RequestParam(value = "organiserUid", required = true) int organiserUid,
		@RequestParam(value = "lastName", required = true) String lastName,
		@RequestParam(value = "firstName", required = true) String firstName,
		@RequestParam(value = "mail", required = true) String mail,
		@RequestParam(value = "webinarKey", required = true) String webinarKey,
		@RequestParam(value = "req", required = true) HttpServletRequest req
	);
	
	@RequestMapping(value = "/getUserInvitedMeeting", method = RequestMethod.POST)
	@ResponseBody
	List<UserMeetingBean> getUserInvitedMeeting (
		@RequestParam(value = "req", required = true) HttpServletRequest req
	);	
	
	@RequestMapping(value = "/inviteUserIntoMeeting", method = RequestMethod.POST)
	@ResponseBody
	UserMeetingBean inviteUserIntoMeeting (
		@RequestParam(value = "uid", required = true) int uid,
		@RequestParam(value = "organiserId", required = true) String organiserId,
		@RequestParam(value = "webinarKey", required = true) String webinarKey,
		@RequestParam(value = "req", required = true) HttpServletRequest req
	);
	
	@RequestMapping(value = "/getAllUser", method = RequestMethod.POST)
	@ResponseBody
	List<UserHereBean> getAllUser();
}