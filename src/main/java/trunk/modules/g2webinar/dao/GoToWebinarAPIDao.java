package trunk.modules.g2webinar.dao;

import java.sql.Types;
import java.util.List;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.g2webinar.beans.*;

@Repository
public class GoToWebinarAPIDao extends JdbcDaoSupportBase {
	public ApiUserBean getG2WebinarUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "p_uid", uid));		
	}
	
	public List<UserHereBean> getAllUser(){
		return invokeStoredProcMulti(UserHereBean.class, "get_all_user");
	}
	
	public void saveG2WebinarUserAuthKey(int uid, String g2webinar_key){
		invokeStoredProc(ApiUserBean.class, "update_user_g2webinar_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "g2webinarKey", g2webinar_key));
	}
	
	public List<UserMeetingBean> getG2WebinarUserInvitedMeetings(int uid){
		return invokeStoredProcMulti(UserMeetingBean.class, "get_user_meetings",
				new StoredProcParam(Types.INTEGER, "uid", uid));
	}
	
	public void saveG2WebinarUserInvitedMeeting(int uid, int organiserUid, String organiserId, String webinarKey){
		invokeStoredProc(UserMeetingBean.class, "insert_user_meeting",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.INTEGER, "organiserUid", organiserUid),
				new StoredProcParam(Types.VARCHAR, "organiserId", organiserId),
				new StoredProcParam(Types.VARCHAR, "webinarKey", webinarKey));
	}
	
	public void updateG2WebinarUserJoinMeetingStatus(int id, String joinUrl){
		invokeStoredProc(UserMeetingBean.class, "update_user_meeting",
				new StoredProcParam(Types.INTEGER, "p_id", id),
				new StoredProcParam(Types.VARCHAR, "joinUrl", joinUrl));
	}
	
}
