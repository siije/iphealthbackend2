package trunk.modules.evernote;

import trunk.modules.api.utils.ApiResultBase;

public class EvernoteResult extends ApiResultBase {

	public EvernoteResult() {
		// Initialize variables for Evernote API
		appName = "iphealthevernote";
		clientId = "dynacjc87";
		clientSecret = "7afdc3286e266e78";
		redirectUrl = "http://localhost:8989/redirectEvernote.html";
		baseAuthUrl = "https://sandbox.evernote.com";
		baseTokenUrl = null;
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {	
		authUrl = null;
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {
		tokenParamsUrl = null;
	}
}
