package trunk.modules.evernote;

import java.util.List;

import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.evernote.dao.EvernoteDao;
import trunk.modules.evernote.maps.EvernoteAccessTokenInfo;

import com.evernote.auth.EvernoteAuth;
import com.evernote.auth.EvernoteService;
import com.evernote.clients.NoteStoreClient;
import com.evernote.clients.ClientFactory;
import com.evernote.edam.error.EDAMNotFoundException;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.notestore.NoteFilter;
import com.evernote.edam.notestore.NoteList;
import com.evernote.edam.type.*;
import com.evernote.thrift.TException;

import org.scribe.builder.api.EvernoteApi;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.*;
import org.scribe.oauth.OAuthService;

@Controller
public class EvernoteController extends TrunkModuleBase implements
		IEvernoteController {

	private EvernoteService getClient() {

		return EvernoteService.SANDBOX;
	}

	private OAuthService getOAuthClient(EvernoteResult evernoteResult) {
		Class<? extends EvernoteApi> providerClass = EvernoteApi.Sandbox.class;
		if (getClient() == EvernoteService.PRODUCTION) {
			providerClass = org.scribe.builder.api.EvernoteApi.class;
		}

		return new ServiceBuilder().provider(providerClass)
				.apiKey(evernoteResult.getClientId())
				.apiSecret(evernoteResult.getClientSecret())
				.callback(evernoteResult.getRedirectUrl()).build();
	}

	private String getAuthTokenDB(User u, boolean config) {
		// Get Evernote user authentication key from database
		EvernoteDao eDao = ApplicationContextProvider.getContext().getBean(
				EvernoteDao.class);
		ApiUserBean evernoteUser = eDao.getEvernoteUserAuthKey(u.getId());

		String accessTokenJson = evernoteUser.getEvernote_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u,
			EvernoteResult evernoteResult, String verifier,
			HttpServletRequest req) {

		try {
			// Get token and token secret from session
			String requestToken = req.getSession().getAttribute("requestToken")
					.toString();
			String requestTokenSecret = req.getSession()
					.getAttribute("requestTokenSecret").toString();

			// Send an OAuth message to the Provider asking to exchange the
			// existing Request Token for an Access Token
			Token scribeRequestToken = new Token(requestToken,
					requestTokenSecret);
			Verifier scribeVerifier = new Verifier(verifier);

			OAuthService service = getOAuthClient(evernoteResult);
			Token scribeAccessToken = service.getAccessToken(
					scribeRequestToken, scribeVerifier);

			EvernoteAuth evernoteAuth = EvernoteAuth.parseOAuthResponse(
					getClient(), scribeAccessToken.getRawResponse());

			String accessToken = evernoteAuth.getToken();
			String noteStoreUrl = evernoteAuth.getNoteStoreUrl();

			EvernoteAccessTokenInfo evernoteTokenInfo = new EvernoteAccessTokenInfo();
			evernoteTokenInfo.setAccessToken(accessToken);
			evernoteTokenInfo.setNoteStoreUrl(noteStoreUrl);

			// Save if not error, and return response
			Gson gson = new Gson();
			String accessTokenJson = gson.toJson(evernoteTokenInfo);

			EvernoteDao eDao = ApplicationContextProvider.getContext().getBean(
					EvernoteDao.class);

			eDao.saveEvernoteUserAuthKey(u.getId(), accessTokenJson);

			req.getSession().removeAttribute("requestToken");
			req.getSession().removeAttribute("requestTokenSecret");
			return accessTokenJson;
		} catch (Exception e) {
			e.printStackTrace();
			// Save error for debug
			return "Error: " + e.getMessage();
		}
	}

	@Override
	public EvernoteResult configEvernote(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		EvernoteResult evernoteResult = new EvernoteResult();
		String authCode = getAuthTokenDB(u, true);

		evernoteResult.setAuthCode(authCode);

		// 3 step flow in this case
		if (authCode == null) {
			try {
				OAuthService service = getOAuthClient(evernoteResult);

				// Send an OAuth message to the Provider asking for a new
				// RequestToken because we don't have access to the current
				// user's account.
				Token scribeRequestToken = service.getRequestToken();

				String requestToken = scribeRequestToken.getToken();
				String requestTokenSecret = scribeRequestToken.getSecret();

				// Set new auth url based on 3 step flow
				req.getSession().setAttribute("requestToken", requestToken);
				req.getSession().setAttribute("requestTokenSecret",
						requestTokenSecret);
				evernoteResult.setAuthUrl(getClient().getAuthorizationUrl(
						requestToken));
			} catch (Exception e) {
				e.printStackTrace();
				evernoteResult.setAuthCode("Error:" + e.getMessage());
			}
		}

		// Authentication code can be null or not, verified in the front end
		return evernoteResult;
	}

	@Override
	public EvernoteResult redirectEvernote(String token, String verifier,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Token already granted
		EvernoteResult evernoteResult = new EvernoteResult();
		String accessTokenJson = getAuthTokenDB(u, false);
		// System.out.println(token + "____" + verifier);

		if (accessTokenJson == null) {
			// Create a new token
			accessTokenJson = createRenewSaveAuthTokenDB(u, evernoteResult,
					verifier, req);

			evernoteResult.setAccessToken(accessTokenJson);

		} else {
			evernoteResult.setAccessToken(accessTokenJson);
		}

		// Access Token can be with or without error, verified in the front end
		return evernoteResult;
	}

	@Override
	public List<Notebook> listNotebooks(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Token already granted
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			try {
				Gson gson = new Gson();
				EvernoteAccessTokenInfo accessToken = gson.fromJson(
						accessTokenJson, EvernoteAccessTokenInfo.class);

				EvernoteAuth evernoteAuth = new EvernoteAuth(getClient(),
						accessToken.getAccessToken());
				NoteStoreClient noteStoreClient = new ClientFactory(
						evernoteAuth).createNoteStoreClient();

				List<Notebook> notebooks = noteStoreClient.listNotebooks();
				return notebooks;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public List<Note> listNotes(String guid, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Token already granted
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			try {
				Gson gson = new Gson();
				EvernoteAccessTokenInfo accessToken = gson.fromJson(
						accessTokenJson, EvernoteAccessTokenInfo.class);

				EvernoteAuth evernoteAuth = new EvernoteAuth(getClient(),
						accessToken.getAccessToken());
				NoteStoreClient noteStoreClient = new ClientFactory(
						evernoteAuth).createNoteStoreClient();

				// Next, search for the first 100 notes in this notebook,
				// ordering
				// by creation date
				NoteFilter filter = new NoteFilter();
				filter.setNotebookGuid(guid);
				filter.setOrder(NoteSortOrder.CREATED.getValue());
				filter.setAscending(true);

				NoteList noteList = noteStoreClient.findNotes(filter, 0, 10);
				List<Note> notes = noteList.getNotes();

				return notes;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public Note getNote(String guid, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Token already granted
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			try {
				Gson gson = new Gson();
				EvernoteAccessTokenInfo accessToken = gson.fromJson(
						accessTokenJson, EvernoteAccessTokenInfo.class);

				EvernoteAuth evernoteAuth = new EvernoteAuth(getClient(),
						accessToken.getAccessToken());
				NoteStoreClient noteStoreClient = new ClientFactory(
						evernoteAuth).createNoteStoreClient();

				Note note = noteStoreClient.getNote(guid, true, true, true,
						true);
				return note;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public String updateNote(String guid, String content, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		String nBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		nBody += "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">";
		nBody += "<en-note>" + content + "</en-note>";

		// Token already granted
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			try {
				Gson gson = new Gson();
				EvernoteAccessTokenInfo accessToken = gson.fromJson(
						accessTokenJson, EvernoteAccessTokenInfo.class);

				EvernoteAuth evernoteAuth = new EvernoteAuth(getClient(),
						accessToken.getAccessToken());
				NoteStoreClient noteStoreClient = new ClientFactory(
						evernoteAuth).createNoteStoreClient();

				Note note = noteStoreClient.getNote(guid, true, false, false,
						false);
				note.setContent(nBody);
				noteStoreClient.updateNote(note);

				return "OK";
			} catch (EDAMUserException e) {
				e.printStackTrace();
				return "Error User " + e.getMessage()
						+ e.getErrorCode().toString();
			} catch (EDAMSystemException e) {
				e.printStackTrace();
				return "Error System " + e.getMessage();
			} catch (EDAMNotFoundException e) {
				e.printStackTrace();
				return "Error Not Found " + e.getMessage();
			} catch (TException e) {
				e.printStackTrace();
				return "Error T " + e.getMessage();
			} catch (Exception e) {
				e.printStackTrace();
				return "Error General " + e.getMessage();
			}
		}

		return "NO_AUTH_TOKEN";
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/evernote.css"));
		ui.addJsFile(new JsFile(ui, "/evernote.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-envelope");
		msm.setName("Evernote Integration");
		msm.setId("evernoteMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/evernote.html");
		ssm.setId("evernoteMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Notebooks");
		ssm2.setHref("/notebooks");
		ssm2.setTemplate("/notebooks.html");
		ssm2.setId("evernoteMainMultiSub2");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
	}
}