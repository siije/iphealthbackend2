package trunk.modules.evernote.maps;

public class EvernoteAccessTokenInfo {
	
	private String accessToken;
	private String noteStoreUrl;
	
	public EvernoteAccessTokenInfo(){
		
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getNoteStoreUrl() {
		return noteStoreUrl;
	}

	public void setNoteStoreUrl(String noteStoreUrl) {
		this.noteStoreUrl = noteStoreUrl;
	}	
}
