package trunk.modules.evernote.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class EvernoteDao extends JdbcDaoSupportBase {
	public ApiUserBean getEvernoteUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void saveEvernoteUserAuthKey(int uid, String evernoteKey){
		invokeStoredProc(ApiUserBean.class, "update_user_evernote_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "evernoteKey", evernoteKey));
	}
}

