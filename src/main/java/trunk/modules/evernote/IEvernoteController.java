package trunk.modules.evernote;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;

@RequestMapping("/evernote")
@TrunkModuleFeature(id = "evernote", displayName = "Evernote", enabled = true)
public interface IEvernoteController extends ITrunkModuleBase {
	
	@TrunkModuleFeature(id = "config", displayName = "Evernote Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	EvernoteResult configEvernote(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
		
	@TrunkModuleFeature(id = "redirect", displayName = "Evernote Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	EvernoteResult redirectEvernote(
			@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "verifier", required = true) String verifier,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "notebooks", displayName = "Evernote List Notebooks")
	@RequestMapping(value = "/notebooks", method = RequestMethod.GET)
	@ResponseBody
	List<Notebook> listNotebooks(
			@RequestParam(value = "req", required = true) HttpServletRequest req);	
	
	@TrunkModuleFeature(id = "notes", displayName = "Evernote List Notes from Notebook")
	@RequestMapping(value = "/notes", method = RequestMethod.GET)
	@ResponseBody
	List<Note> listNotes(
			@RequestParam(value = "guid", required = true) String guid,
			@RequestParam(value = "req", required = true) HttpServletRequest req);	
	
	@TrunkModuleFeature(id = "note", displayName = "Evernote Get Note")
	@RequestMapping(value = "/note", method = RequestMethod.GET)
	@ResponseBody
	Note getNote(
			@RequestParam(value = "guid", required = true) String guid,
			@RequestParam(value = "req", required = true) HttpServletRequest req);	
	
	@TrunkModuleFeature(id = "note", displayName = "Evernote Update Note")
	@RequestMapping(value = "/note", method = RequestMethod.POST)
	@ResponseBody
	String updateNote(
			@RequestParam(value = "guid", required = true) String guid,
			@RequestParam(value = "content", required = true) String content,
			@RequestParam(value = "req", required = true) HttpServletRequest req);	
}
