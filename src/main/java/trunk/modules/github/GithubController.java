package trunk.modules.github;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.egit.github.core.Issue;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.IssueService;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.eclipse.egit.github.core.service.UserService;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.api.utils.ApiInteraction;
import trunk.modules.github.dao.GithubDao;
import trunk.modules.github.maps.GithubAuthToken;
import trunk.modules.github.maps.RepositoryNotification;

@Controller
public class GithubController extends TrunkModuleBase implements
		IGithubController {

	private GitHubClient getClient(String accessToken) {
		// OAuth2 token authentication
		GitHubClient client = new GitHubClient();
		client.setOAuth2Token(accessToken);

		return client;
	}

	private String getAuthTokenDB(User u, boolean config) {
		// Get GitHub user authentication key from database
		GithubDao gDao = ApplicationContextProvider.getContext().getBean(
				GithubDao.class);
		ApiUserBean githubUser = gDao.getGithubUserAuthKey(u.getId());

		String accessTokenJson = githubUser.getGithub_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u,
			GithubResult githubResult, boolean renew, String refreshToken) {

		// Generate parameters url
		githubResult.generateTokenParamsUrl(renew, refreshToken);

		// Get authentication token
		String accessTokenJson = ApiInteraction.getAccessToken(githubResult);

		// Save if not error, and return response
		if (!accessTokenJson.contains("Error")) {
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			GithubDao gDao = ApplicationContextProvider.getContext().getBean(
					GithubDao.class);

			gDao.saveGithubUserAuthKey(u.getId(), accessTokenJson);
			return githubAuthToken.getAccess_token();
		} else {
			// Save error for debug
			return accessTokenJson;
		}
	}

	@Override
	public GithubResult configGithub(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		GithubResult gitHubResult = new GithubResult();
		gitHubResult.setAuthCode(getAuthTokenDB(u, true));

		// Authentication code can be null or not, verified in the front end
		return gitHubResult;
	}

	@Override
	public GithubResult redirectGithub(String authCode, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		GithubResult gitHubResult = new GithubResult();
		gitHubResult.setAuthCode(authCode);

		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson == null) {
			// Create a new token
			gitHubResult.setAccessToken(createRenewSaveAuthTokenDB(u,
					gitHubResult, false, null));
		} else {
			// There is a token already
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			gitHubResult.setAccessToken(githubAuthToken.getAccess_token());
		}

		// Access Token can be with or without error, verified in the front end
		return gitHubResult;
	}

	@Override
	public List<Repository> getRepositoriesGitHub(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<Repository> repositories = new LinkedList<Repository>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			try {
				GitHubClient client = getClient(githubAuthToken
						.getAccess_token());

				RepositoryService service = new RepositoryService(client);
				repositories = service.getRepositories();

			} catch (Exception e) {
				Repository errorEntry = new Repository();
				errorEntry.setName("Error:" + e.getMessage());
				repositories.add(errorEntry);
			}
		} else {
			Repository errorEntry = new Repository();
			errorEntry.setName("NO_AUTH_TOKEN");
			repositories.add(errorEntry);
		}

		return repositories;
	}
	
	@Override
	public List<Repository> createRepositoryGitHub(String name,
			String description, String homepage, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<Repository> repositories = new LinkedList<Repository>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			try {
				GitHubClient client = getClient(githubAuthToken
						.getAccess_token());

				RepositoryService service = new RepositoryService(client);

				Repository repository = new Repository();
				repository.setName(name);
				repository.setDescription(description);
				repository.setHomepage(homepage);

				service.createRepository(repository);
				repositories = service.getRepositories();
			} catch (Exception e) {
				Repository errorEntry = new Repository();
				errorEntry.setName("Error:" + e.getMessage());
				repositories.add(errorEntry);
			}
		} else {
			Repository errorEntry = new Repository();
			errorEntry.setName("NO_AUTH_TOKEN");
			repositories.add(errorEntry);
		}

		return repositories;
	}

	@Override
	public RepositoryNotification[] getRepositoryNotificationsGitHub(
			String owner, String name, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		GithubResult githubResult = new GithubResult();
		RepositoryNotification[] notifications = new RepositoryNotification[100]; // Just 100

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			try {
				// Get projects
				String repositoryNotificationsJson = ApiInteraction.HTTPRequest(
						githubResult.getRepositoryNotificationsUrl() + "/" + owner + "/" + name +
						"/notifications" , "GET", null, "token ", githubAuthToken.getAccess_token(),
						null, null, false, null);

				// Verify if there is no error
				if (repositoryNotificationsJson.contains("Error")){
					RepositoryNotification errorEntry = new RepositoryNotification();
					errorEntry.setReason("Error:" + repositoryNotificationsJson);
					notifications = new RepositoryNotification[] { errorEntry };
				}
				else{
					notifications = gson.fromJson(repositoryNotificationsJson,
							RepositoryNotification[].class);
				}
			} catch (Exception e) {
				RepositoryNotification errorEntry = new RepositoryNotification();
				errorEntry.setReason("Error:" + e.getMessage());
				notifications = new RepositoryNotification[] { errorEntry };
			}
		} else {
			RepositoryNotification errorEntry = new RepositoryNotification();
			errorEntry.setReason("NO_AUTH_TOKEN");
			notifications = new RepositoryNotification[] { errorEntry };
		}

		return notifications;
	}
	
	@Override
	public List<Issue> getRepositoryIssuesGitHub(
			String owner, String name, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<Issue> issues = new LinkedList<Issue>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			try {
				GitHubClient client = getClient(githubAuthToken
						.getAccess_token());

				IssueService service = new IssueService(client);
				issues = service.getIssues(owner, name, null);

			} catch (Exception e) {
				Issue errorEntry = new Issue();
				errorEntry.setTitle("Error:" + e.getMessage());
				issues.add(errorEntry);
			}
		} else {
			Issue errorEntry = new Issue();
			errorEntry.setTitle("NO_AUTH_TOKEN");
			issues.add(errorEntry);
		}

		return issues;
	}
	
	@Override
	public List<Issue> createIssueGitHub(String owner, String name,
			String title, String body, String assignee, 
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<Issue> issues = new LinkedList<Issue>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			try {
				GitHubClient client = getClient(githubAuthToken
						.getAccess_token());

				IssueService service = new IssueService(client);
				
				UserService uService = new UserService(client);
				Issue issue = new Issue();
				issue.setTitle(title);
				issue.setBody(body);
				issue.setAssignee(uService.getUser(assignee));
				
				service.createIssue(owner, name, issue);
				issues = service.getIssues(owner, name, null);

			} catch (Exception e) {
				Issue errorEntry = new Issue();
				errorEntry.setTitle("Error:" + e.getMessage());
				issues.add(errorEntry);
			}
		} else {
			Issue errorEntry = new Issue();
			errorEntry.setTitle("NO_AUTH_TOKEN");
			issues.add(errorEntry);
		}

		return issues;
	}
	
	@Override
	public List<Issue> modifyIssueGitHub(String owner, String name, int id,
			String title, String body, String assignee, 
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<Issue> issues = new LinkedList<Issue>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			try {
				GitHubClient client = getClient(githubAuthToken
						.getAccess_token());

				IssueService service = new IssueService(client);
				UserService uService = new UserService(client);
				
				Issue issue =  service.getIssue(owner, name, id);
				issue.setTitle(title);
				issue.setBody(body);
				issue.setAssignee(uService.getUser(assignee));
				
				service.editIssue(owner, name, issue);
				issues = service.getIssues(owner, name, null);

			} catch (Exception e) {
				Issue errorEntry = new Issue();
				errorEntry.setTitle("Error:" + e.getMessage());
				issues.add(errorEntry);
			}
		} else {
			Issue errorEntry = new Issue();
			errorEntry.setTitle("NO_AUTH_TOKEN");
			issues.add(errorEntry);
		}

		return issues;
	}
	
	@Override
	public List<org.eclipse.egit.github.core.User> getUsersGitHub(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<org.eclipse.egit.github.core.User> users = new LinkedList<org.eclipse.egit.github.core.User>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GithubAuthToken githubAuthToken = gson.fromJson(accessTokenJson,
					GithubAuthToken.class);

			try {
				GitHubClient client = getClient(githubAuthToken
						.getAccess_token());

				UserService service = new UserService(client);
				users = service.getFollowers();

			} catch (Exception e) {
				org.eclipse.egit.github.core.User errorEntry = new org.eclipse.egit.github.core.User();
				errorEntry.setName("Error:" + e.getMessage());
				users.add(errorEntry);
			}
		} else {
			org.eclipse.egit.github.core.User errorEntry = new org.eclipse.egit.github.core.User();
			errorEntry.setName("NO_AUTH_TOKEN");
			users.add(errorEntry);
		}

		return users;
	}
	
	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/github.css"));
		ui.addJsFile(new JsFile(ui, "/github.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-github");
		msm.setName("GitHub Integration");
		msm.setId("githubMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/github.html");
		ssm.setId("githubMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Repositories");
		ssm2.setHref("/repositories");
		ssm2.setTemplate("/repositories.html");
		ssm2.setId("githubMainMultiSub2");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
	}
}
