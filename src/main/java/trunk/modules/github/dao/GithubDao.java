package trunk.modules.github.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class GithubDao extends JdbcDaoSupportBase {
	public ApiUserBean getGithubUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void saveGithubUserAuthKey(int uid, String githubKey){
		invokeStoredProc(ApiUserBean.class, "update_user_github_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "githubKey", githubKey));
	}
}

