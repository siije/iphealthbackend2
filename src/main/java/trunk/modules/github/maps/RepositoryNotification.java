package trunk.modules.github.maps;

import org.eclipse.egit.github.core.Repository;

public class RepositoryNotification {

	long id;
	Repository repository;
	Subject subject;
	String reason;
	boolean unread;
	String updated_at;
	String last_read_at;
	String url;
	
	public RepositoryNotification(){
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Repository getRepository() {
		return repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public boolean isUnread() {
		return unread;
	}

	public void setUnread(boolean unread) {
		this.unread = unread;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getLast_read_at() {
		return last_read_at;
	}

	public void setLast_read_at(String last_read_at) {
		this.last_read_at = last_read_at;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
