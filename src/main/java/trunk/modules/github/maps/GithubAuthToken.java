package trunk.modules.github.maps;

// Representation of the GitHub authentication token
public class GithubAuthToken {
	
	String access_token;
	String token_type;
	String scope;
	
	public GithubAuthToken(){
		
	}
	
	public String getAccess_token() {
		return access_token;
	}
	
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	
	public String getToken_type() {
		return token_type;
	}
	
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
}
