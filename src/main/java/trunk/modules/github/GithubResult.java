package trunk.modules.github;

import trunk.modules.api.utils.ApiResultBase;

public class GithubResult extends ApiResultBase {
	
	private String repositoryNotificationsUrl = "https://api.github.com/repos"; 
	
	public GithubResult() {
		// Initialize variables for Github API
		appName = "iphealth";
		clientId = "eeefafe942089b09c026";
		clientSecret = "cc0f8a9d245648111bce177ee9954ce99a85cbe1";
		redirectUrl = "http://localhost:8989/redirectGithub.html";
		baseAuthUrl = "https://github.com/login/oauth/authorize";
		baseTokenUrl = "https://github.com/login/oauth/access_token";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		StringBuilder str = new StringBuilder();
		str.append(baseAuthUrl);
		str.append("?scope=user, repo");
		//str.append("&app_name=");
		//str.append(appName);
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);

		authUrl = str.toString();
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {
		StringBuilder str = new StringBuilder();
		
		str.append("code=");
		str.append(authCode);
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);
		str.append("&client_secret=");
		str.append(clientSecret);

		tokenParamsUrl = str.toString();
	}

	public String getRepositoryNotificationsUrl() {
		return repositoryNotificationsUrl;
	}

	public void setRepositoryNotificationsUrl(String repositoryNotificationsUrl) {
		this.repositoryNotificationsUrl = repositoryNotificationsUrl;
	}
}
