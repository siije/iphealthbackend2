package trunk.modules.github;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.egit.github.core.Issue;
import org.eclipse.egit.github.core.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.auth.User;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.github.maps.RepositoryNotification;

@RequestMapping("/github")
@TrunkModuleFeature(id = "github", displayName = "GitHub", enabled = true)
public interface IGithubController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "GitHub Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	GithubResult configGithub(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "GitHub Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	GithubResult redirectGithub(
			@RequestParam(value = "authCode", required = true) String code,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "repositories", displayName = "GitHub Get Repositories")
	@RequestMapping(value = "/repositories", method = RequestMethod.GET)
	@ResponseBody
	List<Repository> getRepositoriesGitHub(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
		
	@TrunkModuleFeature(id = "createRepository", displayName = "GitHub Create a Repository")
	@RequestMapping(value = "/createRepository", method = RequestMethod.POST)
	@ResponseBody
	List<Repository> createRepositoryGitHub(
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "homepage", required = true) String homepage,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "notifications", displayName = "GitHub Get Repository Notifications")
	@RequestMapping(value = "/notifications", method = RequestMethod.GET)
	@ResponseBody
	RepositoryNotification[] getRepositoryNotificationsGitHub(
			@RequestParam(value = "owner", required = true) String owner,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "issues", displayName = "GitHub Get Repository Issues")
	@RequestMapping(value = "/issues", method = RequestMethod.GET)
	@ResponseBody
	List<Issue> getRepositoryIssuesGitHub(
			@RequestParam(value = "owner", required = true) String owner,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "createIssue", displayName = "GitHub Create a Issue")
	@RequestMapping(value = "/createIssue", method = RequestMethod.POST)
	@ResponseBody
	List<Issue> createIssueGitHub(
			@RequestParam(value = "owner", required = true) String owner,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "body", required = true) String body,
			@RequestParam(value = "assignee", required = true) String assignee,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "modifyIssue", displayName = "GitHub Modify a Issue")
	@RequestMapping(value = "/modifyIssue", method = RequestMethod.POST)
	@ResponseBody
	List<Issue> modifyIssueGitHub(
			@RequestParam(value = "owner", required = true) String owner,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "id", required = true) int id,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "body", required = true) String body,
			@RequestParam(value = "assignee", required = true) String assignee,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "users", displayName = "GitHub Get Users")
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	@ResponseBody
	List<org.eclipse.egit.github.core.User> getUsersGitHub(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
