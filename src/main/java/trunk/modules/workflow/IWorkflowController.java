package trunk.modules.workflow;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.base.store.TrunkFeature;

@RequestMapping("/workflow")
@TrunkModuleFeature(id = "workflow", displayName = "Workflow Feature Tester")
public interface IWorkflowController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "getActionSettings", displayName = "Get the settings associated with action")
	@RequestMapping(value = "/getActionSettings", method = RequestMethod.POST)
	@ResponseBody
	TrunkFeature getActionSettings(
			@RequestParam(value = "mid", required = true) String mid,
			@RequestParam(value = "fid", required = true) String fid,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);
}