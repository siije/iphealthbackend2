package trunk.modules.bitbucket;

import trunk.modules.api.utils.ApiResultBase;

public class BitbucketResult extends ApiResultBase {
	
	private String requestTokenUrl = "https://bitbucket.org/api/1.0/oauth/request_token";
	private String repositoriesUrl = "https://api.bitbucket.org/2.0/repositories";
	private String repositories1_0Url = "https://bitbucket.org/api/1.0/repositories";
	private String userUrl = "https://bitbucket.org/api/1.0/user"; 
	
	public BitbucketResult() {
		// Initialize variables for Bitbucket API
		appName = "iphealth";
		clientId = "CA4rFcdh77XYy8Xd3d";
		clientSecret = "rccaq7wRyv2QdT6tNqYgdFcgvH8yM4xM";
		redirectUrl = "http://localhost:8989/redirectBitbucket.html";
		baseAuthUrl = "https://bitbucket.org/api/1.0/oauth/authenticate";
		baseTokenUrl = "https://bitbucket.org/api/1.0/oauth/access_token";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		
		authUrl = null;
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {
		
		tokenParamsUrl = null;
	}	

	public String getRequestTokenUrl() {
		return requestTokenUrl;
	}

	public void setRequestTokenUrl(String requestTokenUrl) {
		this.requestTokenUrl = requestTokenUrl;
	}

	public String getRepositoriesUrl() {
		return repositoriesUrl;
	}

	public void setRepositoriesUrl(String repositoriesUrl) {
		this.repositoriesUrl = repositoriesUrl;
	}
	
	public String getRepositories1_0Url() {
		return repositories1_0Url;
	}

	public void setRepositories1_0Url(String repositories1_0Url) {
		this.repositories1_0Url = repositories1_0Url;
	}

	public String getUserUrl() {
		return userUrl;
	}

	public void setUserUrl(String userUrl) {
		this.userUrl = userUrl;
	}
}
