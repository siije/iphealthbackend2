package trunk.modules.bitbucket.maps;

public class BitbucketRepository1_0 {

	String scm;
	boolean has_wiki;
	boolean no_forks;
	String description;
	String email_mailinglist;
	boolean is_mq;
	boolean is_fork;
	boolean read_only;
	String name;
	String language;
	String slug;
	boolean has_issues;
	boolean email_writers;
	boolean no_public_forks;
	String owner;
	String logo;
	String website;
	String state;
	String resource_uri;
	long size;
	boolean is_private;
	String error;
	
	public BitbucketRepository1_0(){
		
	}

	public String getScm() {
		return scm;
	}

	public void setScm(String scm) {
		this.scm = scm;
	}

	public boolean isHas_wiki() {
		return has_wiki;
	}

	public void setHas_wiki(boolean has_wiki) {
		this.has_wiki = has_wiki;
	}

	public boolean isNo_forks() {
		return no_forks;
	}

	public void setNo_forks(boolean no_forks) {
		this.no_forks = no_forks;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail_mailinglist() {
		return email_mailinglist;
	}

	public void setEmail_mailinglist(String email_mailinglist) {
		this.email_mailinglist = email_mailinglist;
	}

	public boolean isIs_mq() {
		return is_mq;
	}

	public void setIs_mq(boolean is_mq) {
		this.is_mq = is_mq;
	}

	public boolean isIs_fork() {
		return is_fork;
	}

	public void setIs_fork(boolean is_fork) {
		this.is_fork = is_fork;
	}

	public boolean isRead_only() {
		return read_only;
	}

	public void setRead_only(boolean read_only) {
		this.read_only = read_only;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public boolean isHas_issues() {
		return has_issues;
	}

	public void setHas_issues(boolean has_issues) {
		this.has_issues = has_issues;
	}

	public boolean isEmail_writers() {
		return email_writers;
	}

	public void setEmail_writers(boolean email_writers) {
		this.email_writers = email_writers;
	}

	public boolean isNo_public_forks() {
		return no_public_forks;
	}

	public void setNo_public_forks(boolean no_public_forks) {
		this.no_public_forks = no_public_forks;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getResource_uri() {
		return resource_uri;
	}

	public void setResource_uri(String resource_uri) {
		this.resource_uri = resource_uri;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public boolean isIs_private() {
		return is_private;
	}

	public void setIs_private(boolean is_private) {
		this.is_private = is_private;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
