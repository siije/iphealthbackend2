package trunk.modules.bitbucket.maps;

public class BitbucketRepositories {

	int pagelen;
	BitbucketRepository[] values;
	int page;
	int size;

	public BitbucketRepositories() {

	}

	public int getPagelen() {
		return pagelen;
	}

	public void setPagelen(int pagelen) {
		this.pagelen = pagelen;
	}

	public BitbucketRepository[] getValues() {
		return values;
	}

	public void setValues(BitbucketRepository[] values) {
		this.values = values;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
