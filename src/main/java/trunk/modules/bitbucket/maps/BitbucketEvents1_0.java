package trunk.modules.bitbucket.maps;

public class BitbucketEvents1_0 {

	int count;
	BitbucketEvent1_0[] events;
	String error;
		
	public 	BitbucketEvents1_0(){
		
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public BitbucketEvent1_0[] getEvents() {
		return events;
	}

	public void setEvents(BitbucketEvent1_0[] events) {
		this.events = events;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}	
}
