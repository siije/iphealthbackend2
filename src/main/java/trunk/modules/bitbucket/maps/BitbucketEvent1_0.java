package trunk.modules.bitbucket.maps;

public class BitbucketEvent1_0 {

	String node;
	String description;
	BitbucketRepository1_0 repository;
	String created_on;
	//String user;
	String utc_created_on;
	String event;
	
	public BitbucketEvent1_0(){
		
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BitbucketRepository1_0 getRepository() {
		return repository;
	}

	public void setRepository(BitbucketRepository1_0 repository) {
		this.repository = repository;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUtc_created_on() {
		return utc_created_on;
	}

	public void setUtc_created_on(String utc_created_on) {
		this.utc_created_on = utc_created_on;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}
}
