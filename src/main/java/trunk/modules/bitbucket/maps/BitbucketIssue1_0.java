package trunk.modules.bitbucket.maps;

public class BitbucketIssue1_0 {

	String status;
	String priority;
	String title;
	BitbucketUser reported_by;
	String utc_last_updated;
	BitbucketUser responsible;
	int comment_count;
	BitbucketMetadata metadata;
	String content;
	String created_on;
	int local_id;
	int follower_count;
	String utc_created_on;
	String resource_uri;
	boolean is_spam;
	String error;
	
	public BitbucketIssue1_0(){
		
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BitbucketUser getReported_by() {
		return reported_by;
	}

	public void setReported_by(BitbucketUser reported_by) {
		this.reported_by = reported_by;
	}

	public String getUtc_last_updated() {
		return utc_last_updated;
	}

	public void setUtc_last_updated(String utc_last_updated) {
		this.utc_last_updated = utc_last_updated;
	}

	public BitbucketUser getResponsible() {
		return responsible;
	}

	public void setResponsible(BitbucketUser responsible) {
		this.responsible = responsible;
	}

	public int getComment_count() {
		return comment_count;
	}

	public void setComment_count(int comment_count) {
		this.comment_count = comment_count;
	}

	public BitbucketMetadata getMetadata() {
		return metadata;
	}

	public void setMetadata(BitbucketMetadata metadata) {
		this.metadata = metadata;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public int getLocal_id() {
		return local_id;
	}

	public void setLocal_id(int local_id) {
		this.local_id = local_id;
	}

	public int getFollower_count() {
		return follower_count;
	}

	public void setFollower_count(int follower_count) {
		this.follower_count = follower_count;
	}

	public String getUtc_created_on() {
		return utc_created_on;
	}

	public void setUtc_created_on(String utc_created_on) {
		this.utc_created_on = utc_created_on;
	}

	public String getResource_uri() {
		return resource_uri;
	}

	public void setResource_uri(String resource_uri) {
		this.resource_uri = resource_uri;
	}

	public boolean isIs_spam() {
		return is_spam;
	}

	public void setIs_spam(boolean is_spam) {
		this.is_spam = is_spam;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
