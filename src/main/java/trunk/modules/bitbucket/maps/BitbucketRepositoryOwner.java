package trunk.modules.bitbucket.maps;

public class BitbucketRepositoryOwner {

	String username;
	String display_name;
	String uuid;
	//String[] links;
	
	public BitbucketRepositoryOwner(){
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDisplay_name() {
		return display_name;
	}

	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
