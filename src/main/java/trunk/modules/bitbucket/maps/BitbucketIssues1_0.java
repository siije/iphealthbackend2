package trunk.modules.bitbucket.maps;

public class BitbucketIssues1_0 {

	int count;
	//String filter;
	String search;
	BitbucketIssue1_0[] issues;
	
	public BitbucketIssues1_0(){
		
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public BitbucketIssue1_0[]  getIssues() {
		return issues;
	}

	public void setIssues(BitbucketIssue1_0[]  issues) {
		this.issues = issues;
	}
}
