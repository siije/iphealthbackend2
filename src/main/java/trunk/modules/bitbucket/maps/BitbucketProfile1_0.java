package trunk.modules.bitbucket.maps;

public class BitbucketProfile1_0 {

	BitbucketRepository1_0[] repositories;
	BitbucketUser user;
	String error;
	
	public BitbucketProfile1_0(){
		
	}

	public BitbucketRepository1_0[] getRepositories() {
		return repositories;
	}

	public void setRepositories(BitbucketRepository1_0[] repositories) {
		this.repositories = repositories;
	}

	public BitbucketUser getUser() {
		return user;
	}

	public void setUser(BitbucketUser user) {
		this.user = user;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
