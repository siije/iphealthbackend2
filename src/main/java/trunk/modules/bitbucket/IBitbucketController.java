package trunk.modules.bitbucket;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.bitbucket.maps.BitbucketEvents1_0;
import trunk.modules.bitbucket.maps.BitbucketIssue1_0;
import trunk.modules.bitbucket.maps.BitbucketProfile1_0;
import trunk.modules.bitbucket.maps.BitbucketRepository;

@RequestMapping("/bitbucket")
@TrunkModuleFeature(id = "bitbucket", displayName = "BitBucket", enabled = true)
public interface IBitbucketController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "BitBucket Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	BitbucketResult configBitbucket(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "BitBucket Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	BitbucketResult redirectBitbucket(
			@RequestParam(value = "token", required = true) String code,
			@RequestParam(value = "verifier", required = true) String verifier,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "repositories", displayName = "BitBucket Get Repositories")
	@RequestMapping(value = "/repositories", method = RequestMethod.GET)
	@ResponseBody
	BitbucketRepository[] getRepositoriesBitbucket(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "createRepository", displayName = "BitBucket Create a Repository")
	@RequestMapping(value = "/createRepository", method = RequestMethod.POST)
	@ResponseBody
	BitbucketRepository[] createRepositoryBitbucket(
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "is_private", required = true) boolean is_private,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "events", displayName = "BitBucket Get Repository Notifications")
	@RequestMapping(value = "/events", method = RequestMethod.GET)
	@ResponseBody
	BitbucketEvents1_0 getRepositoryEventsBitbucket(
			@RequestParam(value = "owner", required = true) String owner,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "issues", displayName = "BitBucket Get Repository Issues")
	@RequestMapping(value = "/issues", method = RequestMethod.GET)
	@ResponseBody
	BitbucketIssue1_0[] getRepositoryIssuesBitbucket(
			@RequestParam(value = "owner", required = true) String owner,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "createIssue", displayName = "BitBucket Create a Issue")
	@RequestMapping(value = "/createIssue", method = RequestMethod.POST)
	@ResponseBody
	BitbucketIssue1_0[] createIssueBitbucket(
			@RequestParam(value = "owner", required = true) String owner,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "content", required = true) String body,
			@RequestParam(value = "responsible", required = true) String responsible,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "modifyIssue", displayName = "BitBucket Modify a Issue")
	@RequestMapping(value = "/modifyIssue", method = RequestMethod.POST)
	@ResponseBody
	BitbucketIssue1_0[] modifyIssueBitbucket(
			@RequestParam(value = "owner", required = true) String owner,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "id", required = true) int id,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "content", required = true) String content,
			@RequestParam(value = "responsible", required = true) String responsible,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "profile", displayName = "BitBucket Get User Profile")
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@ResponseBody
	BitbucketProfile1_0 getUserProfile(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
