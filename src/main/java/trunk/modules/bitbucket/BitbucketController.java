package trunk.modules.bitbucket;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.api.utils.ApiInteraction;
import trunk.modules.bitbucket.dao.BitbucketDao;
import trunk.modules.bitbucket.maps.BitbucketAuthToken;
import trunk.modules.bitbucket.maps.BitbucketEvents1_0;
import trunk.modules.bitbucket.maps.BitbucketIssue1_0;
import trunk.modules.bitbucket.maps.BitbucketIssues1_0;
import trunk.modules.bitbucket.maps.BitbucketProfile1_0;
import trunk.modules.bitbucket.maps.BitbucketRepositories;
import trunk.modules.bitbucket.maps.BitbucketRepository;

@Controller
public class BitbucketController extends TrunkModuleBase implements
		IBitbucketController {

	private String getAuthTokenDB(User u, boolean config) {
		// Get BitBucket user authentication key from database
		BitbucketDao bDao = ApplicationContextProvider.getContext().getBean(
				BitbucketDao.class);
		ApiUserBean bitbucketUser = bDao.getBitbucketbUserAuthKey(u.getId());

		String accessTokenJson = bitbucketUser.getBitbucket_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u,
			BitbucketResult bitBucketResult, boolean renew,
			String refreshToken, HttpServletRequest req, String token) {

		try {
			// Get token and token secret from session
			String requestToken = req.getSession().getAttribute("requestToken")
					.toString();
			String requestTokenSecret = req.getSession()
					.getAttribute("requestTokenSecret").toString();

			OAuthProvider provider = (OAuthProvider) req.getSession()
					.getAttribute("provider");

			// create a consumer object and configure it with the access
			// token and token secret obtained from the service provider
			OAuthConsumer consumer = new DefaultOAuthConsumer(
					bitBucketResult.getClientId(),
					bitBucketResult.getClientSecret());

			consumer.setTokenWithSecret(requestToken, requestTokenSecret);
			provider.retrieveAccessToken(consumer,
					bitBucketResult.getAuthCode());

			// Get authentication token
			BitbucketAuthToken bitBucketAuthToken = new BitbucketAuthToken();
			bitBucketAuthToken.setAccess_token(consumer.getToken());
			bitBucketAuthToken.setToken_type("Bearer");
			bitBucketAuthToken.setToken_secret(consumer.getTokenSecret());

			Gson gson = new Gson();
			String accessTokenJson = gson.toJson(bitBucketAuthToken);

			BitbucketDao bDao = ApplicationContextProvider.getContext()
					.getBean(BitbucketDao.class);

			bDao.saveBitbucketUserAuthKey(u.getId(), accessTokenJson);

			req.getSession().removeAttribute("provider");
			req.getSession().removeAttribute("requestToken");
			req.getSession().removeAttribute("requestTokenSecret");
			return bitBucketAuthToken.getAccess_token();
		} catch (OAuthMessageSignerException e) {
			return "Error: " + e.getMessage();
		} catch (OAuthNotAuthorizedException e) {
			return "Error: " + e.getMessage();
		} catch (OAuthExpectationFailedException e) {
			return "Error: " + e.getMessage();
		} catch (OAuthCommunicationException e) {
			return "Error: " + e.getMessage();
		}
	}

	@Override
	public BitbucketResult configBitbucket(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		BitbucketResult bitBucketResult = new BitbucketResult();
		bitBucketResult.setAuthCode(getAuthTokenDB(u, true));

		if (bitBucketResult.getAuthCode() == null) {
			try {
				// create a consumer object and configure it with the access
				// token and token secret obtained from the service provider
				OAuthConsumer consumer = new DefaultOAuthConsumer(
						bitBucketResult.getClientId(),
						bitBucketResult.getClientSecret());

				// create a new service provider object and configure it with
				// the URLs which provide request tokens, access tokens, and
				// the URL to which users are sent in order to grant permission
				// to your application to access protected resources
				OAuthProvider provider = new DefaultOAuthProvider(
						bitBucketResult.getRequestTokenUrl(),
						bitBucketResult.getBaseTokenUrl(),
						bitBucketResult.getBaseAuthUrl());

				// fetches a request token from the service provider and builds
				// a url based on AUTHORIZE_WEBSITE_URL and CALLBACK_URL to
				// which your app must now send the user
				String url = provider.retrieveRequestToken(consumer,
						bitBucketResult.getRedirectUrl());

				String requestToken = consumer.getToken();
				String requestTokenSecret = consumer.getTokenSecret();

				// Set new auth url based on 3 step flow
				req.getSession().setAttribute("provider", provider);
				req.getSession().setAttribute("requestToken", requestToken);
				req.getSession().setAttribute("requestTokenSecret",
						requestTokenSecret);

				bitBucketResult.setAuthUrl(url);

			} catch (OAuthMessageSignerException e) {
				bitBucketResult.setAuthUrl("Error: " + e.getMessage());
			} catch (OAuthNotAuthorizedException e) {
				bitBucketResult.setAuthUrl("Error: " + e.getMessage());
			} catch (OAuthExpectationFailedException e) {
				bitBucketResult.setAuthUrl("Error: " + e.getMessage());
			} catch (OAuthCommunicationException e) {
				bitBucketResult.setAuthUrl("Error: " + e.getMessage());
			}
		}

		// Authentication code can be null or not, verified in the front end
		return bitBucketResult;
	}

	@Override
	public BitbucketResult redirectBitbucket(String token, String verifier,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		BitbucketResult bitBucketResult = new BitbucketResult();
		bitBucketResult.setAuthCode(verifier);

		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson == null) {
			// Create a new token
			bitBucketResult.setAccessToken(createRenewSaveAuthTokenDB(u,
					bitBucketResult, false, null, req, token));
		} else {
			// There is a token already
			Gson gson = new Gson();
			BitbucketAuthToken bitbucketAuthToken = gson.fromJson(
					accessTokenJson, BitbucketAuthToken.class);

			bitBucketResult
					.setAccessToken(bitbucketAuthToken.getAccess_token());
		}

		// Access Token can be with or without error, verified in the front end
		return bitBucketResult;
	}

	@Override
	public BitbucketRepository[] getRepositoriesBitbucket(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BitbucketResult bitBucketResult = new BitbucketResult();
		BitbucketRepository[] repositories = null;

		if (accessTokenJson != null) {
			try {
				// There is a token already
				Gson gson = new Gson();
				BitbucketAuthToken bitbucketAuthToken = gson.fromJson(
						accessTokenJson, BitbucketAuthToken.class);

				BitbucketProfile1_0 profile = getUserProfile(req);

				if (profile.getError() != null && profile.getError() != "") {
					BitbucketRepository errorEntry = new BitbucketRepository();
					errorEntry.setError("Error: " + profile.getError());
					repositories = new BitbucketRepository[] { errorEntry };
				} else {
					// create a consumer object and configure it with the access
					// token and token secret obtained from the service provider
					OAuthConsumer consumer = new DefaultOAuthConsumer(
							bitBucketResult.getClientId(),
							bitBucketResult.getClientSecret());

					consumer.setTokenWithSecret(
							bitbucketAuthToken.getAccess_token(),
							bitbucketAuthToken.getToken_secret());

					// Get repositories
					String repositoriesJson = ApiInteraction.HTTPRequest(
							bitBucketResult.getRepositoriesUrl() + "/"
									+ profile.getUser().getUsername(), "GET",
							null, null, null, null, null, false, consumer);

					// Verify if there is no error
					if (repositoriesJson.contains("Error")) {
						BitbucketRepository errorEntry = new BitbucketRepository();
						errorEntry.setError("Error: " + repositoriesJson);
						repositories = new BitbucketRepository[] { errorEntry };
					} else {
						BitbucketRepositories repos = gson.fromJson(
								repositoriesJson, BitbucketRepositories.class);
						repositories = repos.getValues();
					}
				}
			} catch (Exception e) {
				BitbucketRepository errorEntry = new BitbucketRepository();
				errorEntry.setError("Error: " + e.getMessage());
				repositories = new BitbucketRepository[] { errorEntry };
			}
		} else {
			BitbucketRepository errorEntry = new BitbucketRepository();
			errorEntry.setError("NO_AUTH_TOKEN");
			repositories = new BitbucketRepository[] { errorEntry };
		}

		return repositories;
	}

	@Override
	public BitbucketRepository[] createRepositoryBitbucket(String name,
			String description, boolean is_private, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BitbucketResult bitBucketResult = new BitbucketResult();
		BitbucketRepository[] repositories = null;

		if (accessTokenJson != null) {
			try {
				// There is a token already
				Gson gson = new Gson();
				BitbucketAuthToken bitbucketAuthToken = gson.fromJson(
						accessTokenJson, BitbucketAuthToken.class);

				BitbucketProfile1_0 profile = getUserProfile(req);

				if (profile.getError() != null && profile.getError() != "") {
					BitbucketRepository errorEntry = new BitbucketRepository();
					errorEntry.setError("Error: " + profile.getError());
					repositories = new BitbucketRepository[] { errorEntry };
				} else {
					JsonFactory jFactory = new JsonFactory();
					StringWriter writer = new StringWriter();
					/*** write to string ***/
					JsonGenerator jsonGenerator;
					try {
						jsonGenerator = jFactory.createJsonGenerator(writer);
						jsonGenerator.writeStartObject();
						jsonGenerator.writeStringField("scm", "git");
						jsonGenerator.writeStringField("name", name);
						jsonGenerator.writeStringField("description",
								description);
						jsonGenerator.writeStringField("fork_policy",
								"allow_forks");
						jsonGenerator.writeStringField("is_private",
								String.valueOf(is_private));
						jsonGenerator.writeEndObject();
						jsonGenerator.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String jsonRepository = writer.toString();

					// create a consumer object and configure it with the access
					// token and token secret obtained from the service provider
					OAuthConsumer consumer = new DefaultOAuthConsumer(
							bitBucketResult.getClientId(),
							bitBucketResult.getClientSecret());

					consumer.setTokenWithSecret(
							bitbucketAuthToken.getAccess_token(),
							bitbucketAuthToken.getToken_secret());

					// Create repository
					String repositoriesJson = ApiInteraction.HTTPRequest(
							bitBucketResult.getRepositoriesUrl() + "/"
									+ profile.getUser().getUsername() + "/"
									+ name, "POST", jsonRepository, null, null,
							null, "application/json", false, consumer);

					// Verify if there is no error
					if (repositoriesJson.contains("Error")) {
						BitbucketRepository errorEntry = new BitbucketRepository();
						errorEntry.setError("Error: " + repositoriesJson);
						repositories = new BitbucketRepository[] { errorEntry };
					} else {
						repositories = getRepositoriesBitbucket(req);
					}
				}
			} catch (Exception e) {
				BitbucketRepository errorEntry = new BitbucketRepository();
				errorEntry.setError("Error: " + e.getMessage());
				repositories = new BitbucketRepository[] { errorEntry };
			}
		} else {
			BitbucketRepository errorEntry = new BitbucketRepository();
			errorEntry.setError("NO_AUTH_TOKEN");
			repositories = new BitbucketRepository[] { errorEntry };
		}

		return repositories;
	}

	@Override
	public BitbucketEvents1_0 getRepositoryEventsBitbucket(String owner,
			String name, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BitbucketResult bitBucketResult = new BitbucketResult();
		BitbucketEvents1_0 events = new BitbucketEvents1_0();

		if (accessTokenJson != null) {
			try {
				// There is a token already
				Gson gson = new Gson();
				BitbucketAuthToken bitbucketAuthToken = gson.fromJson(
						accessTokenJson, BitbucketAuthToken.class);

				// create a consumer object and configure it with the access
				// token and token secret obtained from the service provider
				OAuthConsumer consumer = new DefaultOAuthConsumer(
						bitBucketResult.getClientId(),
						bitBucketResult.getClientSecret());

				consumer.setTokenWithSecret(
						bitbucketAuthToken.getAccess_token(),
						bitbucketAuthToken.getToken_secret());

				// Get events
				String issuesJson = ApiInteraction.HTTPRequest(
						bitBucketResult.getRepositories1_0Url() + "/" + owner
								+ "/" + name + "/events", "GET", null, null,
						null, null, null, false, consumer);

				// Verify if there is no error
				if (issuesJson.contains("Error")) {
					events.setError("Error: " + issuesJson);
				} else {
					events = gson
							.fromJson(issuesJson, BitbucketEvents1_0.class);
				}
			} catch (Exception e) {
				events.setError("Error: " + e.getMessage());
			}
		} else {
			events.setError("NO_AUTH_TOKEN");
		}

		return events;
	}

	@Override
	public BitbucketIssue1_0[] getRepositoryIssuesBitbucket(String owner,
			String name, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BitbucketResult bitBucketResult = new BitbucketResult();
		BitbucketIssue1_0[] issues = null;

		if (accessTokenJson != null) {
			try {
				// There is a token already
				Gson gson = new Gson();
				BitbucketAuthToken bitbucketAuthToken = gson.fromJson(
						accessTokenJson, BitbucketAuthToken.class);

				// create a consumer object and configure it with the access
				// token and token secret obtained from the service provider
				OAuthConsumer consumer = new DefaultOAuthConsumer(
						bitBucketResult.getClientId(),
						bitBucketResult.getClientSecret());

				consumer.setTokenWithSecret(
						bitbucketAuthToken.getAccess_token(),
						bitbucketAuthToken.getToken_secret());

				// Get issues
				String issuesJson = ApiInteraction.HTTPRequest(
						bitBucketResult.getRepositories1_0Url() + "/" + owner
								+ "/" + name + "/issues", "GET", null, null,
						null, null, null, false, consumer);

				// Verify if there is no error
				if (issuesJson.contains("Error")) {
					BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
					errorEntry.setError("Error: " + issuesJson);
					issues = new BitbucketIssue1_0[] { errorEntry };
				} else {
					BitbucketIssues1_0 iss = gson.fromJson(issuesJson,
							BitbucketIssues1_0.class);
					issues = iss.getIssues();
				}
			} catch (Exception e) {
				BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
				errorEntry.setError("Error: " + e.getMessage());
				issues = new BitbucketIssue1_0[] { errorEntry };
			}
		} else {
			BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
			errorEntry.setError("NO_AUTH_TOKEN");
			issues = new BitbucketIssue1_0[] { errorEntry };
		}

		return issues;
	}

	@Override
	public BitbucketIssue1_0[] createIssueBitbucket(String owner, String name,
			String title, String content, String responsible,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BitbucketResult bitBucketResult = new BitbucketResult();
		BitbucketIssue1_0[] issues = null;

		if (accessTokenJson != null) {
			try {
				// There is a token already
				Gson gson = new Gson();
				BitbucketAuthToken bitbucketAuthToken = gson.fromJson(
						accessTokenJson, BitbucketAuthToken.class);

				// create a consumer object and configure it with the access
				// token and token secret obtained from the service provider
				OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
						bitBucketResult.getClientId(),
						bitBucketResult.getClientSecret());

				consumer.setTokenWithSecret(
						bitbucketAuthToken.getAccess_token(),
						bitbucketAuthToken.getToken_secret());

				DefaultHttpClient httpclient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(
						bitBucketResult.getRepositories1_0Url() + "/" + owner
								+ "/" + name + "/issues");

				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("title", title));
				nvps.add(new BasicNameValuePair("content", content));
				nvps.add(new BasicNameValuePair("responsible", responsible));
				nvps.add(new BasicNameValuePair("status", "new"));
				httpPost.setEntity(new UrlEncodedFormEntity(nvps));
				consumer.sign(httpPost);
				HttpResponse response = httpclient.execute(httpPost);

				if (response.getStatusLine().getStatusCode() == 200) {
					// HttpEntity entity = response.getEntity();
					// do something useful with the response body
					// and ensure it is fully consumed
					// EntityUtils.consume(entity);
					issues = getRepositoryIssuesBitbucket(owner, name, req);
				} else {
					BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
					errorEntry.setError("Error: " + response.getStatusLine());
					issues = new BitbucketIssue1_0[] { errorEntry };
				}
			} catch (Exception e) {
				BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
				errorEntry.setError("Error: " + e.getMessage());
				issues = new BitbucketIssue1_0[] { errorEntry };
			}
		} else {
			BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
			errorEntry.setError("NO_AUTH_TOKEN");
			issues = new BitbucketIssue1_0[] { errorEntry };
		}

		return issues;
	}

	@Override
	public BitbucketIssue1_0[] modifyIssueBitbucket(String owner, String name,
			int id, String title, String content, String responsible,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BitbucketResult bitBucketResult = new BitbucketResult();
		BitbucketIssue1_0[] issues = null;

		if (accessTokenJson != null) {
			try {
				// There is a token already
				Gson gson = new Gson();
				BitbucketAuthToken bitbucketAuthToken = gson.fromJson(
						accessTokenJson, BitbucketAuthToken.class);

				// create a consumer object and configure it with the access
				// token and token secret obtained from the service provider
				OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
						bitBucketResult.getClientId(),
						bitBucketResult.getClientSecret());

				consumer.setTokenWithSecret(
						bitbucketAuthToken.getAccess_token(),
						bitbucketAuthToken.getToken_secret());

				DefaultHttpClient httpclient = new DefaultHttpClient();
				HttpPut httpPut = new HttpPut(
						bitBucketResult.getRepositories1_0Url() + "/" + owner
								+ "/" + name + "/issues/" + id);

				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("title", title));
				nvps.add(new BasicNameValuePair("content", content));
				nvps.add(new BasicNameValuePair("responsible", responsible));
				httpPut.setEntity(new UrlEncodedFormEntity(nvps));
				consumer.sign(httpPut);
				HttpResponse response = httpclient.execute(httpPut);

				if (response.getStatusLine().getStatusCode() == 200) {
					// HttpEntity entity = response.getEntity();
					// do something useful with the response body
					// and ensure it is fully consumed
					// EntityUtils.consume(entity);
					issues = getRepositoryIssuesBitbucket(owner, name, req);
				} else {
					BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
					errorEntry.setError("Error: " + response.getStatusLine());
					issues = new BitbucketIssue1_0[] { errorEntry };
				}
			} catch (Exception e) {
				BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
				errorEntry.setError("Error: " + e.getMessage());
				issues = new BitbucketIssue1_0[] { errorEntry };
			}
		} else {
			BitbucketIssue1_0 errorEntry = new BitbucketIssue1_0();
			errorEntry.setError("NO_AUTH_TOKEN");
			issues = new BitbucketIssue1_0[] { errorEntry };
		}

		return issues;
	}

	@Override
	public BitbucketProfile1_0 getUserProfile(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		BitbucketResult bitBucketResult = new BitbucketResult();

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BitbucketProfile1_0 profile = new BitbucketProfile1_0();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			BitbucketAuthToken bitbucketAuthToken = gson.fromJson(
					accessTokenJson, BitbucketAuthToken.class);

			try {
				// create a consumer object and configure it with the access
				// token and token secret obtained from the service provider
				OAuthConsumer consumer = new DefaultOAuthConsumer(
						bitBucketResult.getClientId(),
						bitBucketResult.getClientSecret());

				consumer.setTokenWithSecret(
						bitbucketAuthToken.getAccess_token(),
						bitbucketAuthToken.getToken_secret());

				// Get projects
				String profileJson = ApiInteraction.HTTPRequest(
						bitBucketResult.getUserUrl(), "GET", null, null, null,
						null, null, false, consumer);

				// Verify if there is no error
				if (profileJson.contains("Error")) {
					profile.setError("Error a: " + profileJson);
				} else {
					profile = gson.fromJson(profileJson,
							BitbucketProfile1_0.class);
				}
			} catch (Exception e) {
				profile.setError("Error d:" + e.getMessage());
			}
		} else {
			profile.setError("NO_AUTH_TOKEN");
		}

		return profile;
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/bitbucket.css"));
		ui.addJsFile(new JsFile(ui, "/bitbucket.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-bitbucket");
		msm.setName("BitBucket Integration");
		msm.setId("bitbucketMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/bitbucket.html");
		ssm.setId("bitbucketMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Repositories");
		ssm2.setHref("/repositories");
		ssm2.setTemplate("/repositories.html");
		ssm2.setId("bitbucketMainMultiSub2");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
	}
}
