package trunk.modules.bitbucket.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class BitbucketDao extends JdbcDaoSupportBase {
	public ApiUserBean getBitbucketbUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void saveBitbucketUserAuthKey(int uid, String bitbucketKey){
		invokeStoredProc(ApiUserBean.class, "update_user_bitbucket_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "bitbucketKey", bitbucketKey));
	}
}

