package trunk.modules.pivotaltracker.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class PivotalTrackerDao extends JdbcDaoSupportBase {
	public ApiUserBean getPivotalTrackerUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void savePivotalTrackerUserAuthKey(int uid, String pivotalTrackerKey){
		invokeStoredProc(ApiUserBean.class, "update_user_pivotaltracker_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "pivotalTrackerKey", pivotalTrackerKey));
	}
}

