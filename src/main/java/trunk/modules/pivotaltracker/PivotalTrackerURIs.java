package trunk.modules.pivotaltracker;

public class PivotalTrackerURIs {

	static String projectsUrl = "https://www.pivotaltracker.com/services/v5/projects";
	static String profileUrl = "https://www.pivotaltracker.com/services/v5/me";
	static String userInfoUrl = "https://www.pivotaltracker.com/services/v5/my"; 
}
