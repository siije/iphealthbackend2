package trunk.modules.pivotaltracker.maps;

public class PivotalActivity {

	String kind;
	String guid;
	int project_version;
	String message;
	String highlight;
	PivotalActivityChange[] changes;
	PivotalPrimaryResource[] primary_resources;
	PivotalProject project;
	PivotalProfile performed_by;	
	String occurred_at;
	
	public PivotalActivity(){
		
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public int getProject_version() {
		return project_version;
	}

	public void setProject_version(int project_version) {
		this.project_version = project_version;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getHighlight() {
		return highlight;
	}

	public void setHighlight(String highlight) {
		this.highlight = highlight;
	}

	public PivotalActivityChange[] getChanges() {
		return changes;
	}

	public void setChanges(PivotalActivityChange[] changes) {
		this.changes = changes;
	}

	public PivotalPrimaryResource[] getPrimary_resources() {
		return primary_resources;
	}

	public void setPrimary_resources(PivotalPrimaryResource[] primary_resources) {
		this.primary_resources = primary_resources;
	}

	public PivotalProject getProject() {
		return project;
	}

	public void setProject(PivotalProject project) {
		this.project = project;
	}

	public PivotalProfile getPerformed_by() {
		return performed_by;
	}

	public void setPerformed_by(PivotalProfile performed_by) {
		this.performed_by = performed_by;
	}

	public String getOccurred_at() {
		return occurred_at;
	}

	public void setOccurred_at(String occurred_at) {
		this.occurred_at = occurred_at;
	}
}
