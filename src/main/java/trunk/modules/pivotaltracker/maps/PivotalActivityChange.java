package trunk.modules.pivotaltracker.maps;

public class PivotalActivityChange {

	String kind;
	String change_type;
	int id;
	PivotalActivityValue original_values;
	PivotalActivityValue new_values;
	String name;
	String story_type;
	
	public PivotalActivityChange(){
		
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getChange_type() {
		return change_type;
	}

	public void setChange_type(String change_type) {
		this.change_type = change_type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PivotalActivityValue getOriginal_values() {
		return original_values;
	}

	public void setOriginal_values(PivotalActivityValue original_values) {
		this.original_values = original_values;
	}

	public PivotalActivityValue getNew_values() {
		return new_values;
	}

	public void setNew_values(PivotalActivityValue new_values) {
		this.new_values = new_values;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStory_type() {
		return story_type;
	}

	public void setStory_type(String story_type) {
		this.story_type = story_type;
	}
}
