package trunk.modules.pivotaltracker.maps;

public class PivotalNotificationData {

	String kind;
	int id;
	PivotalProject project;
	PivotalProfile performer;
	String message;
	String notification_type;
	int new_attachment_count;
	String action;
	PivotalStory story;
	String created_at;
	String updated_at;
	
	public PivotalNotificationData(){
		
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PivotalProject getProject() {
		return project;
	}

	public void setProject(PivotalProject project) {
		this.project = project;
	}

	public PivotalProfile getPerformer() {
		return performer;
	}

	public void setPerformer(PivotalProfile performer) {
		this.performer = performer;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNotification_type() {
		return notification_type;
	}

	public void setNotification_type(String notification_type) {
		this.notification_type = notification_type;
	}

	public int getNew_attachment_count() {
		return new_attachment_count;
	}

	public void setNew_attachment_count(int new_attachment_count) {
		this.new_attachment_count = new_attachment_count;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public PivotalStory getStory() {
		return story;
	}

	public void setStory(PivotalStory story) {
		this.story = story;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
}
