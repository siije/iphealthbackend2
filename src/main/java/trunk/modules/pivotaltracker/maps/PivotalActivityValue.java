package trunk.modules.pivotaltracker.maps;

public class PivotalActivityValue {

	String current_state;
	int before_id;
	int after_id;
	
	public PivotalActivityValue(){
		
	}

	public String getCurrent_state() {
		return current_state;
	}

	public void setCurrent_state(String current_state) {
		this.current_state = current_state;
	}

	public int getBefore_id() {
		return before_id;
	}

	public void setBefore_id(int before_id) {
		this.before_id = before_id;
	}

	public int getAfter_id() {
		return after_id;
	}

	public void setAfter_id(int after_id) {
		this.after_id = after_id;
	}
}
