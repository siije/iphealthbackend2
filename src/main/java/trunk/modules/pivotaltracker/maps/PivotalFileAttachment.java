package trunk.modules.pivotaltracker.maps;

public class PivotalFileAttachment {

	int id;
	String filename;
	String created_at;
	int uploader_id;
	boolean thumbnailable;
	int height;
	int width;
	int size;
	String download_url;
	String content_type;
	boolean uploaded;
	String big_url;
	String thumbnail_url;
	String kind;
	
	public PivotalFileAttachment(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getUploader_id() {
		return uploader_id;
	}

	public void setUploader_id(int uploader_id) {
		this.uploader_id = uploader_id;
	}

	public boolean isThumbnailable() {
		return thumbnailable;
	}

	public void setThumbnailable(boolean thumbnailable) {
		this.thumbnailable = thumbnailable;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getDownload_url() {
		return download_url;
	}

	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}

	public String getContent_type() {
		return content_type;
	}

	public void setContent_type(String content_type) {
		this.content_type = content_type;
	}

	public boolean isUploaded() {
		return uploaded;
	}

	public void setUploaded(boolean uploaded) {
		this.uploaded = uploaded;
	}

	public String getBig_url() {
		return big_url;
	}

	public void setBig_url(String big_url) {
		this.big_url = big_url;
	}

	public String getThumbnail_url() {
		return thumbnail_url;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}	
}
