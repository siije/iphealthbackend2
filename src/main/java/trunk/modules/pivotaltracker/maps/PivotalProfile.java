package trunk.modules.pivotaltracker.maps;

public class PivotalProfile {

	int id;
	String name;
	String initials;
	String username;
	String api_token;
	boolean has_google_identity;
	String email;
	boolean receives_in_app_notifications;
	String created_at;
	String updated_at;
	String kind;
	
	public PivotalProfile(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getApi_token() {
		return api_token;
	}

	public void setApi_token(String api_token) {
		this.api_token = api_token;
	}

	public boolean isHas_google_identity() {
		return has_google_identity;
	}

	public void setHas_google_identity(boolean has_google_identity) {
		this.has_google_identity = has_google_identity;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isReceives_in_app_notifications() {
		return receives_in_app_notifications;
	}

	public void setReceives_in_app_notifications(
			boolean receives_in_app_notifications) {
		this.receives_in_app_notifications = receives_in_app_notifications;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}
}
