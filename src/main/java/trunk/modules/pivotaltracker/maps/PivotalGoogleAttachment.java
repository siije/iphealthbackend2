package trunk.modules.pivotaltracker.maps;

public class PivotalGoogleAttachment {

	int id;
	int comment_id;
	int person_id;
	String google_kind;
	String title;
	String google_id;
	String alternate_link;
	String resource_id;
	String kind;
	
	public PivotalGoogleAttachment(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getComment_id() {
		return comment_id;
	}

	public void setComment_id(int comment_id) {
		this.comment_id = comment_id;
	}

	public int getPerson_id() {
		return person_id;
	}

	public void setPerson_id(int person_id) {
		this.person_id = person_id;
	}

	public String getGoogle_kind() {
		return google_kind;
	}

	public void setGoogle_kind(String google_kind) {
		this.google_kind = google_kind;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGoogle_id() {
		return google_id;
	}

	public void setGoogle_id(String google_id) {
		this.google_id = google_id;
	}

	public String getAlternate_link() {
		return alternate_link;
	}

	public void setAlternate_link(String alternate_link) {
		this.alternate_link = alternate_link;
	}

	public String getResource_id() {
		return resource_id;
	}

	public void setResource_id(String resource_id) {
		this.resource_id = resource_id;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}
}
