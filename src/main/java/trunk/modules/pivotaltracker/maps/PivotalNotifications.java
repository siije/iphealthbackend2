package trunk.modules.pivotaltracker.maps;

public class PivotalNotifications {

	PivotalNotificationData[] data;
	
	public PivotalNotifications(){
		
	}

	public PivotalNotificationData[] getData() {
		return data;
	}

	public void setData(PivotalNotificationData[] data) {
		this.data = data;
	}
	
	
}
