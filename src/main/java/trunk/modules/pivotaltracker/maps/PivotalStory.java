package trunk.modules.pivotaltracker.maps;

public class PivotalStory {

	String kind;
	int id;
	String created_at;
	String updated_at;
	String story_type;
	String name;
	String description;
	String current_state;
	String url;
	int requested_by_id;
	int project_id;
	float estimate;
	int[] owner_ids;	
	int[] label_ids;
	int[] task_ids;
	int[] follower_ids;
	int[] comment_ids;
	
	// Other fields
	PivotalLabel[] labels;
	PivotalTask[] tasks;
	PivotalActivity[] activities;
	PivotalComment[] comments;
	
	public PivotalStory(){
		
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getStory_type() {
		return story_type;
	}

	public void setStory_type(String story_type) {
		this.story_type = story_type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrent_state() {
		return current_state;
	}

	public void setCurrent_state(String current_state) {
		this.current_state = current_state;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getRequested_by_id() {
		return requested_by_id;
	}

	public void setRequested_by_id(int requested_by_id) {
		this.requested_by_id = requested_by_id;
	}

	public int getProject_id() {
		return project_id;
	}

	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}

	public float getEstimate() {
		return estimate;
	}

	public void setEstimate(float estimate) {
		this.estimate = estimate;
	}

	public int[] getOwner_ids() {
		return owner_ids;
	}

	public void setOwner_ids(int[] owner_ids) {
		this.owner_ids = owner_ids;
	}

	public int[] getLabel_ids() {
		return label_ids;
	}

	public void setLabel_ids(int[] label_ids) {
		this.label_ids = label_ids;
	}

	public int[] getTask_ids() {
		return task_ids;
	}

	public void setTask_ids(int[] task_ids) {
		this.task_ids = task_ids;
	}

	public int[] getFollower_ids() {
		return follower_ids;
	}

	public void setFollower_ids(int[] follower_ids) {
		this.follower_ids = follower_ids;
	}

	public int[] getComment_ids() {
		return comment_ids;
	}

	public void setComment_ids(int[] comment_ids) {
		this.comment_ids = comment_ids;
	}
	
	public PivotalLabel[] getLabels() {
		return labels;
	}

	public void setLabels(PivotalLabel[] labels) {
		this.labels = labels;
	}

	public PivotalTask[] getTasks() {
		return tasks;
	}

	public void setTasks(PivotalTask[] tasks) {
		this.tasks = tasks;
	}

	public PivotalActivity[] getActivities() {
		return activities;
	}

	public void setActivities(PivotalActivity[] activities) {
		this.activities = activities;
	}

	public PivotalComment[] getComments() {
		return comments;
	}

	public void setComments(PivotalComment[] comments) {
		this.comments = comments;
	}
}
