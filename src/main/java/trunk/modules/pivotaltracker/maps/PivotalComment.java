package trunk.modules.pivotaltracker.maps;

public class PivotalComment {

	String text;
	int person_id;
	PivotalFileAttachment[] file_attachments;
	PivotalGoogleAttachment[] google_attachments;
	String commit_identifier;
	String commit_type;
	String kind;
	int id;
	int story_id;
	//String created_at;
	//String updated_at;
	
	public PivotalComment(){
		
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getPerson_id() {
		return person_id;
	}

	public void setPerson_id(int person_id) {
		this.person_id = person_id;
	}

	public PivotalFileAttachment[] getFile_attachments() {
		return file_attachments;
	}

	public void setFile_attachments(PivotalFileAttachment[] file_attachments) {
		this.file_attachments = file_attachments;
	}

	public PivotalGoogleAttachment[] getGoogle_attachments() {
		return google_attachments;
	}

	public void setGoogle_attachments(PivotalGoogleAttachment[] google_attachments) {
		this.google_attachments = google_attachments;
	}

	public String getCommit_identifier() {
		return commit_identifier;
	}

	public void setCommit_identifier(String commit_identifier) {
		this.commit_identifier = commit_identifier;
	}

	public String getCommit_type() {
		return commit_type;
	}

	public void setCommit_type(String commit_type) {
		this.commit_type = commit_type;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStory_id() {
		return story_id;
	}

	public void setStory_id(int story_id) {
		this.story_id = story_id;
	}
}
