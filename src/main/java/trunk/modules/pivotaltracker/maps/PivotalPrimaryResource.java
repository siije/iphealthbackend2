package trunk.modules.pivotaltracker.maps;

public class PivotalPrimaryResource {

	String kind;
	int id;
	String name;
	String story_type;
	String url;
	
	public PivotalPrimaryResource(){
		
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStory_type() {
		return story_type;
	}

	public void setStory_type(String story_type) {
		this.story_type = story_type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}	
}
