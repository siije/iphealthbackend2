package trunk.modules.pivotaltracker;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.pivotaltracker.maps.PivotalActivity;
import trunk.modules.pivotaltracker.maps.PivotalLabel;
import trunk.modules.pivotaltracker.maps.PivotalNotifications;
import trunk.modules.pivotaltracker.maps.PivotalProfile;
import trunk.modules.pivotaltracker.maps.PivotalProject;
import trunk.modules.pivotaltracker.maps.PivotalStory;
import trunk.modules.pivotaltracker.maps.PivotalComment;
import trunk.modules.pivotaltracker.maps.PivotalTask;

@RequestMapping("/pivotaltracker")
@TrunkModuleFeature(id = "pivotaltracker", displayName = "Pivotal Tracker", enabled = true)
public interface IPivotalTrackerController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "Pivotal Tracker Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	String configPivotalTracker(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "Pivotal Tracker Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	String redirectPivotalTracker(
			@RequestParam(value = "apiKey", required = true) String apiKey,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "profile", displayName = "Pivotal Tracker Profile")
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@ResponseBody
	PivotalProfile profilePivotalTracker(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "projects", displayName = "Pivotal Tracker Projects")
	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	@ResponseBody
	PivotalProject[] projectsPivotalTracker(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "stories", displayName = "Pivotal Tracker Stories per Project")
	@RequestMapping(value = "/stories", method = RequestMethod.GET)
	@ResponseBody
	PivotalStory[] storiesPivotalTracker(
			@RequestParam(value = "id", required = true) int id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storiesFilterId", displayName = "Pivotal Tracker Stories per Project per Id")
	@RequestMapping(value = "/storiesFilterId", method = RequestMethod.GET)
	@ResponseBody
	PivotalStory[] storiesPivotalTrackerFilteredById(
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storiesFilterQuery", displayName = "Pivotal Tracker Stories per Project")
	@RequestMapping(value = "/storiesFilterQuery", method = RequestMethod.GET)
	@ResponseBody
	PivotalStory[] storiesPivotalTrackerFilteredByQuery(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "query", required = true) String query,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyCommentAdd", displayName = "Pivotal Tracker Add Comment for a Story")
	@RequestMapping(value = "/storyCommentAdd", method = RequestMethod.POST)
	@ResponseBody
	PivotalComment[] storyCommentAdd(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "comment", required = true) String commment,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "storyCommentUpdate", displayName = "Pivotal Tracker Update Comment for a Story")
	@RequestMapping(value = "/storyCommentUpdate", method = RequestMethod.POST)
	@ResponseBody
	PivotalComment[] storyCommentUpdate(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "commentId", required = true) int commentId,
			@RequestParam(value = "comment", required = true) String commment,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyCommentDelete", displayName = "Pivotal Tracker Delete Comment for a Story")
	@RequestMapping(value = "/storyCommentDelete", method = RequestMethod.POST)
	@ResponseBody
	PivotalComment[] storyCommentDelete(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "commentId", required = true) int commentId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyDescriptionUpdate", displayName = "Pivotal Tracker Description Update for a Story")
	@RequestMapping(value = "/storyDescriptionUpdate", method = RequestMethod.POST)
	@ResponseBody
	String storyDescriptionUpdate(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyLabelUpdate", displayName = "Pivotal Tracker Update Label for a Story")
	@RequestMapping(value = "/storyLabelUpdate", method = RequestMethod.POST)
	@ResponseBody
	PivotalLabel[] storyLabelUpdate(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "labelId", required = true) int labelId,
			@RequestParam(value = "label", required = true) String label,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyLabelAdd", displayName = "Pivotal Tracker Add Label for a Story")
	@RequestMapping(value = "/storyLabelAdd", method = RequestMethod.POST)
	@ResponseBody
	PivotalLabel[] storyLabelAdd(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "label", required = true) String label,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyLabelDelete", displayName = "Pivotal Tracker Delete Label for a Story")
	@RequestMapping(value = "/storyLabelDelete", method = RequestMethod.POST)
	@ResponseBody
	PivotalLabel[] storyLabelDelete(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "labelId", required = true) int labelId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyTaskAdd", displayName = "Pivotal Tracker Add Task for a Story")
	@RequestMapping(value = "/storyTaskAdd", method = RequestMethod.POST)
	@ResponseBody
	PivotalTask[] storyTaskAdd(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyTaskUpdate", displayName = "Pivotal Tracker Update Task for a Story")
	@RequestMapping(value = "/storyTaskUpdate", method = RequestMethod.POST)
	@ResponseBody
	PivotalTask[] storyTaskUpdate(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "taskId", required = true) int taskId,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "complete", required = true) boolean complete,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyTaskDelete", displayName = "Pivotal Tracker Delete Task for a Story")
	@RequestMapping(value = "/storyTaskDelete", method = RequestMethod.POST)
	@ResponseBody
	PivotalTask[] storyTaskDelete(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "taskId", required = true) int taskId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyTasks", displayName = "Pivotal Tracker Tasks per Story")
	@RequestMapping(value = "/storyTasks", method = RequestMethod.GET)
	@ResponseBody
	PivotalTask[] storyTasksPivotalTracker(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyComments", displayName = "Pivotal Tracker Comments per Story")
	@RequestMapping(value = "/storyComments", method = RequestMethod.GET)
	@ResponseBody
	PivotalComment[] storyCommentsPivotalTracker(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyActivities", displayName = "Pivotal Tracker Activities per Story")
	@RequestMapping(value = "/storyActivities", method = RequestMethod.GET)
	@ResponseBody
	PivotalActivity[] storyActivitiesPivotalTracker(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyLabels", displayName = "Pivotal Tracker Labels per Story")
	@RequestMapping(value = "/storyLabels", method = RequestMethod.GET)
	@ResponseBody
	PivotalLabel[] storyLabelsPivotalTracker(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "storyFullInfo", displayName = "Pivotal Tracker Add Story")
	@RequestMapping(value = "/storyFullInfo", method = RequestMethod.GET)
	@ResponseBody
	PivotalStory storyFullInfo(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "storyInfoAdd", displayName = "Pivotal Tracker Add Story")
	@RequestMapping(value = "/storyInfoAdd", method = RequestMethod.POST)
	@ResponseBody
	PivotalStory[] storyInfoAdd(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "estimate", required = true) float estimate,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "storyInfoDelete", displayName = "Pivotal Tracker Delete Story")
	@RequestMapping(value = "/storyInfoDelete", method = RequestMethod.POST)
	@ResponseBody
	PivotalStory[] storyInfoDelete(
			@RequestParam(value = "projectId", required = true) int projectId,
			@RequestParam(value = "storyId", required = true) int storyId,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "notifications", displayName = "Pivotal Tracker Notifications")
	@RequestMapping(value = "/notifications", method = RequestMethod.GET)
	@ResponseBody
	PivotalNotifications notificationsPivotalTracker(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
