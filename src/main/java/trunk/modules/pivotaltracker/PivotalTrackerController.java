package trunk.modules.pivotaltracker;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.springframework.stereotype.Controller;

import com.google.api.client.util.Value;
import com.google.appengine.labs.repackaged.com.google.common.base.Converter;
import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.api.utils.ApiInteraction;
import trunk.modules.pivotaltracker.dao.PivotalTrackerDao;
import trunk.modules.pivotaltracker.maps.PivotalActivity;
import trunk.modules.pivotaltracker.maps.PivotalLabel;
import trunk.modules.pivotaltracker.maps.PivotalNotificationData;
import trunk.modules.pivotaltracker.maps.PivotalNotifications;
import trunk.modules.pivotaltracker.maps.PivotalProfile;
import trunk.modules.pivotaltracker.maps.PivotalProject;
import trunk.modules.pivotaltracker.maps.PivotalStory;
import trunk.modules.pivotaltracker.maps.PivotalComment;
import trunk.modules.pivotaltracker.maps.PivotalTask;

@Controller
public class PivotalTrackerController extends TrunkModuleBase implements
		IPivotalTrackerController {

	private String getApiKeyDB(User u, boolean config) {
		// Get Pivotal Tacker user api key from database
		PivotalTrackerDao pDao = ApplicationContextProvider.getContext()
				.getBean(PivotalTrackerDao.class);
		ApiUserBean pivotalTrackerUser = pDao.getPivotalTrackerUserAuthKey(u
				.getId());

		String apiKey = pivotalTrackerUser.getPivotaltracker_key();

		if (config) {
			if (apiKey != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return apiKey;
		}
	}

	private String createRenewSaveAuthTokenDB(User u, String apiKey) {
		// Save if not error, and return response
		if (apiKey != null && apiKey != "") {
			PivotalTrackerDao pDao = ApplicationContextProvider.getContext()
					.getBean(PivotalTrackerDao.class);

			pDao.savePivotalTrackerUserAuthKey(u.getId(), apiKey);
			return apiKey;
		} else {
			// Save error for debug
			return "API_KEY_NOT_VALID";
		}
	}

	@Override
	public String configPivotalTracker(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		return getApiKeyDB(u, true);
	}

	@Override
	public String redirectPivotalTracker(String apiKey, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String apiKeyDB = getApiKeyDB(u, false);

		if (apiKeyDB == null) {
			// Create a new token
			apiKeyDB = createRenewSaveAuthTokenDB(u, apiKey);
		} else {
			// There is a token already
			apiKeyDB = "ALREADY_AUTH";
		}

		return apiKeyDB;
	}

	@Override
	public PivotalProfile profilePivotalTracker(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalProfile profile = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get projects
			String profileJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.profileUrl, "GET", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!profileJson.contains("Error")) {
				// Transform projects
				profile = gson.fromJson(profileJson, PivotalProfile.class);

				// Save new authentication token in class for further use
				return profile;
			} else {
				// Save error for debug
				PivotalProfile errorEntry = new PivotalProfile();
				errorEntry.setName("Error: " + profileJson + " PROJECTS");
				return errorEntry;
			}
		} else {
			PivotalProfile errorEntry = new PivotalProfile();
			errorEntry.setName("NO_AUTH_TOKEN");
			return errorEntry;
		}
	}

	@Override
	public PivotalProject[] projectsPivotalTracker(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalProject[] projects = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get projects
			String projectsJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl, "GET", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!projectsJson.contains("Error")) {
				// Transform projects
				projects = gson.fromJson(projectsJson, PivotalProject[].class);

				// Save new authentication token in class for further use
				return projects;
			} else {
				// Save error for debug
				PivotalProject errorEntry = new PivotalProject();
				errorEntry.setName("Error: " + projectsJson + " PROJECTS");
				projects = new PivotalProject[] { errorEntry };
			}
		} else {
			PivotalProject errorEntry = new PivotalProject();
			errorEntry.setName("NO_AUTH_TOKEN");
			projects = new PivotalProject[] { errorEntry };
		}

		// Projects can be with or without error, verified in the front end
		return projects;
	}

	@Override
	public PivotalStory[] storiesPivotalTracker(int id, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalStory[] stories = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			if (id == -1) {
				List<PivotalStory> tempStories = new LinkedList<PivotalStory>();

				for (PivotalProject project : projectsPivotalTracker(req)) {
					for (PivotalStory story : storiesPivotalTracker(
							project.getId(), req)) {
						tempStories.add(story);
					}
				}

				return tempStories
						.toArray(new PivotalStory[tempStories.size()]);
			}

			// Get stories per project
			String storiesProjectJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + id + "/stories",
					"GET", null, "X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!storiesProjectJson.contains("Error")) {
				// Transform stories
				stories = gson.fromJson(storiesProjectJson,
						PivotalStory[].class);

				// Save new authentication token in class for further use
				return stories;
			} else {
				// Save error for debug
				PivotalStory errorEntry = new PivotalStory();
				errorEntry.setName("Error: " + storiesProjectJson + " STORIES");
				stories = new PivotalStory[] { errorEntry };
			}
		} else {
			PivotalStory errorEntry = new PivotalStory();
			errorEntry.setName("NO_AUTH_TOKEN");
			stories = new PivotalStory[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return stories;
	}

	@Override
	public PivotalStory[] storiesPivotalTrackerFilteredById(int storyId,
			int projectId, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalStory[] stories = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get stories per project
			String storiesProjectJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId, "GET", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!storiesProjectJson.contains("Error")) {
				// Transform stories
				PivotalStory story = gson.fromJson(storiesProjectJson,
						PivotalStory.class);

				stories = new PivotalStory[] { story };

				// Save new authentication token in class for further use
				return stories;
			} else {
				// Save error for debug
				PivotalStory errorEntry = new PivotalStory();
				errorEntry.setName("Error: " + storiesProjectJson + " STORIES");
				stories = new PivotalStory[] { errorEntry };
			}
		} else {
			PivotalStory errorEntry = new PivotalStory();
			errorEntry.setName("NO_AUTH_TOKEN");
			stories = new PivotalStory[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return stories;
	}

	@Override
	public PivotalStory[] storiesPivotalTrackerFilteredByQuery(int projectId,
			String query, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalStory[] stories = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get stories per project
			String storiesProjectJson = ApiInteraction
					.HTTPRequest(
							PivotalTrackerURIs.projectsUrl + "/" + projectId
									+ "/stories?with_label="
									+ URLEncoder.encode(query), "GET", null,
							"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!storiesProjectJson.contains("Error")) {
				// Transform stories
				stories = gson.fromJson(storiesProjectJson,
						PivotalStory[].class);

				// Save new authentication token in class for further use
				return stories;
			} else {
				// Save error for debug
				PivotalStory errorEntry = new PivotalStory();
				errorEntry.setName("Error: " + storiesProjectJson + " STORIES");
				stories = new PivotalStory[] { errorEntry };
			}
		} else {
			PivotalStory errorEntry = new PivotalStory();
			errorEntry.setName("NO_AUTH_TOKEN");
			stories = new PivotalStory[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return stories;
	}

	@Override
	public PivotalComment[] storyCommentAdd(int projectId, int storyId,
			String comment, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);
		comment = URLDecoder.decode(comment);

		PivotalComment[] comments = null;
		if (apiKey != null) {
			PivotalProfile profile = profilePivotalTracker(req);

			PivotalComment newComment = new PivotalComment();
			newComment.setText(comment);
			newComment.setPerson_id(profile.getId());

			// Get stories per project
			String storiesCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/comments", "POST",
					newComment, "X-TrackerToken", apiKey, null,
					"application/json", true, null);

			// If no error
			if (!storiesCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyCommentsPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalComment errorEntry = new PivotalComment();
				errorEntry.setText("Error: " + storiesCommentJson + " COMMENT");
				comments = new PivotalComment[] { errorEntry };
			}
		} else {
			PivotalComment errorEntry = new PivotalComment();
			errorEntry.setText("NO_AUTH_TOKEN");
			comments = new PivotalComment[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return comments;
	}

	@Override
	public PivotalComment[] storyCommentUpdate(int projectId, int storyId,
			int commentId, String comment, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);
		comment = URLDecoder.decode(comment);

		PivotalComment[] comments = null;
		if (apiKey != null) {
			JsonFactory jFactory = new JsonFactory();
			StringWriter writer = new StringWriter();
			/*** write to string ***/
			JsonGenerator jsonGenerator;
			try {
				jsonGenerator = jFactory.createJsonGenerator(writer);
				jsonGenerator.writeStartObject();
				jsonGenerator.writeStringField("text", comment);
				jsonGenerator.writeEndObject();
				jsonGenerator.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String jsonComment = writer.toString();

			// Get stories per project
			String storiesCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/comments/" + commentId,
					"PUT", jsonComment, "X-TrackerToken", apiKey, null,
					"application/json", true, null);

			// If no error
			if (!storiesCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyCommentsPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalComment errorEntry = new PivotalComment();
				errorEntry.setText("Error: " + storiesCommentJson + " COMMENT");
				comments = new PivotalComment[] { errorEntry };
			}
		} else {
			PivotalComment errorEntry = new PivotalComment();
			errorEntry.setText("NO_AUTH_TOKEN");
			comments = new PivotalComment[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return comments;
	}

	@Override
	public PivotalComment[] storyCommentDelete(int projectId, int storyId,
			int commentId, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalComment[] comments = null;
		if (apiKey != null) {

			// Get stories per project
			String storiesCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/comments/" + commentId,
					"DELETE", null, "X-TrackerToken", apiKey, null, null, true, null);

			// Fix to check for errors
			if (storiesCommentJson.contains("204")
					&& storiesCommentJson.contains("OK")) {
				storiesCommentJson = "OK";
			}

			// If no error
			if (!storiesCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyCommentsPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalComment errorEntry = new PivotalComment();
				errorEntry.setText("Error: " + storiesCommentJson + " COMMENT");
				comments = new PivotalComment[] { errorEntry };
			}
		} else {
			PivotalComment errorEntry = new PivotalComment();
			errorEntry.setText("NO_AUTH_TOKEN");
			comments = new PivotalComment[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return comments;
	}

	@Override
	public String storyDescriptionUpdate(int projectId, int storyId,
			String description, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);
		description = URLDecoder.decode(description);

		if (apiKey != null) {
			JsonFactory jFactory = new JsonFactory();
			StringWriter writer = new StringWriter();
			/*** write to string ***/
			JsonGenerator jsonGenerator;
			try {
				jsonGenerator = jFactory.createJsonGenerator(writer);
				jsonGenerator.writeStartObject();
				jsonGenerator.writeStringField("description", description);
				jsonGenerator.writeEndObject();
				jsonGenerator.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String jsonDescription = writer.toString();

			// Get stories per project
			String storyCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId, "PUT", jsonDescription,
					"X-TrackerToken", apiKey, null, "application/json", true, null);

			// If no error
			if (!storyCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return description;
			} else {
				// Save error for debug
				return "Error: " + storyCommentJson + " DESCRIPTION";
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	@Override
	public PivotalLabel[] storyLabelUpdate(int projectId, int storyId,
			int labelId, String label, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);
		label = URLDecoder.decode(label);

		PivotalLabel[] labels = null;
		if (apiKey != null) {
			JsonFactory jFactory = new JsonFactory();
			StringWriter writer = new StringWriter();
			/*** write to string ***/
			JsonGenerator jsonGenerator;
			try {
				jsonGenerator = jFactory.createJsonGenerator(writer);
				jsonGenerator.writeStartObject();
				jsonGenerator.writeStringField("name", label);
				jsonGenerator.writeEndObject();
				jsonGenerator.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String jsonLabel = writer.toString();

			// Get stories per project
			String labelCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/labels/" + labelId, "PUT", jsonLabel,
					"X-TrackerToken", apiKey, null, "application/json", true, null);

			// If no error
			if (!labelCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyLabelsPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalLabel errorEntry = new PivotalLabel();
				errorEntry.setName("Error: " + labelCommentJson + " LABEL");
				labels = new PivotalLabel[] { errorEntry };
			}
		} else {
			PivotalLabel errorEntry = new PivotalLabel();
			errorEntry.setName("NO_AUTH_TOKEN");
			labels = new PivotalLabel[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return labels;
	}

	@Override
	public PivotalLabel[] storyLabelAdd(int projectId, int storyId,
			String label, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);
		label = URLDecoder.decode(label);

		PivotalLabel[] labels = null;
		if (apiKey != null) {
			JsonFactory jFactory = new JsonFactory();
			StringWriter writer = new StringWriter();
			/*** write to string ***/
			JsonGenerator jsonGenerator;
			try {
				jsonGenerator = jFactory.createJsonGenerator(writer);
				jsonGenerator.writeStartObject();
				jsonGenerator.writeStringField("name", label);
				jsonGenerator.writeEndObject();
				jsonGenerator.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String jsonLabel = writer.toString();

			// Get stories per project
			String labelCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/labels", "POST",
					jsonLabel, "X-TrackerToken", apiKey, null,
					"application/json", true, null);

			// If no error
			if (!labelCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyLabelsPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalLabel errorEntry = new PivotalLabel();
				errorEntry.setName("Error: " + labelCommentJson + " LABEL");
				labels = new PivotalLabel[] { errorEntry };
			}
		} else {
			PivotalLabel errorEntry = new PivotalLabel();
			errorEntry.setName("NO_AUTH_TOKEN");
			labels = new PivotalLabel[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return labels;
	}

	@Override
	public PivotalLabel[] storyLabelDelete(int projectId, int storyId,
			int labelId, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalLabel[] labels = null;
		if (apiKey != null) {

			// Get stories per project
			String labelCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/labels/" + labelId, "DELETE", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// Fix to check for errors
			if (labelCommentJson.contains("204")
					&& labelCommentJson.contains("OK")) {
				labelCommentJson = "OK";
			}

			// If no error
			if (!labelCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyLabelsPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalLabel errorEntry = new PivotalLabel();
				errorEntry.setName("Error: " + labelCommentJson + " LABEL");
				labels = new PivotalLabel[] { errorEntry };
			}
		} else {
			PivotalLabel errorEntry = new PivotalLabel();
			errorEntry.setName("NO_AUTH_TOKEN");
			labels = new PivotalLabel[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return labels;
	}

	@Override
	public PivotalTask[] storyTaskAdd(int projectId, int storyId,
			String description, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);
		description = URLDecoder.decode(description);

		PivotalTask[] tasks = null;
		if (apiKey != null) {
			JsonFactory jFactory = new JsonFactory();
			StringWriter writer = new StringWriter();
			/*** write to string ***/
			JsonGenerator jsonGenerator;
			try {
				jsonGenerator = jFactory.createJsonGenerator(writer);
				jsonGenerator.writeStartObject();
				jsonGenerator.writeStringField("description", description);
				jsonGenerator.writeEndObject();
				jsonGenerator.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String jsonTask = writer.toString();

			// Get stories per project
			String taskCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/tasks", "POST",
					jsonTask, "X-TrackerToken", apiKey, null,
					"application/json", true, null);

			// If no error
			if (!taskCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyTasksPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalTask errorEntry = new PivotalTask();
				errorEntry.setDescription("Error: " + taskCommentJson
						+ " LABEL");
				tasks = new PivotalTask[] { errorEntry };
			}
		} else {
			PivotalTask errorEntry = new PivotalTask();
			errorEntry.setDescription("NO_AUTH_TOKEN");
			tasks = new PivotalTask[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return tasks;
	}

	@Override
	public PivotalTask[] storyTaskUpdate(int projectId, int storyId,
			int taskId, String description, boolean complete,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalTask[] tasks = null;
		if (apiKey != null) {
			JsonFactory jFactory = new JsonFactory();
			StringWriter writer = new StringWriter();
			/*** write to string ***/
			JsonGenerator jsonGenerator;
			try {
				jsonGenerator = jFactory.createJsonGenerator(writer);
				jsonGenerator.writeStartObject();
				jsonGenerator.writeStringField("description", description);
				jsonGenerator.writeBooleanField("complete", complete);
				jsonGenerator.writeEndObject();
				jsonGenerator.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String jsonTask = writer.toString();

			// Get stories per project
			String taskCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/tasks/" + taskId,
					"PUT", jsonTask, "X-TrackerToken", apiKey, null,
					"application/json", true, null);

			// If no error
			if (!taskCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyTasksPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalTask errorEntry = new PivotalTask();
				errorEntry.setDescription("Error: " + taskCommentJson
						+ " LABEL");
				tasks = new PivotalTask[] { errorEntry };
			}
		} else {
			PivotalTask errorEntry = new PivotalTask();
			errorEntry.setDescription("NO_AUTH_TOKEN");
			tasks = new PivotalTask[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return tasks;
	}

	@Override
	public PivotalTask[] storyTaskDelete(int projectId, int storyId,
			int taskId, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalTask[] tasks = null;
		if (apiKey != null) {

			// Get stories per project
			String taskCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/tasks/" + taskId,
					"DELETE", null, "X-TrackerToken", apiKey, null, null, true, null);

			// Fix to check for errors
			if (taskCommentJson.contains("204")
					&& taskCommentJson.contains("OK")) {
				taskCommentJson = "OK";
			}

			// If no error
			if (!taskCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storyTasksPivotalTracker(projectId, storyId, req);
			} else {
				// Save error for debug
				PivotalTask errorEntry = new PivotalTask();
				errorEntry.setDescription("Error: " + taskCommentJson
						+ " LABEL");
				tasks = new PivotalTask[] { errorEntry };
			}
		} else {
			PivotalTask errorEntry = new PivotalTask();
			errorEntry.setDescription("NO_AUTH_TOKEN");
			tasks = new PivotalTask[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return tasks;
	}

	@Override
	public PivotalTask[] storyTasksPivotalTracker(int projectId, int storyId,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalTask[] tasks = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get stories per project
			String tasksProjectJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/tasks", "GET", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!tasksProjectJson.contains("Error")) {
				// Transform stories
				tasks = gson.fromJson(tasksProjectJson, PivotalTask[].class);

				// Save new authentication token in class for further use
				return tasks;
			} else {
				// Save error for debug
				PivotalTask errorEntry = new PivotalTask();
				errorEntry.setDescription("Error: " + tasksProjectJson
						+ " TASKS");
				tasks = new PivotalTask[] { errorEntry };
			}
		} else {
			PivotalTask errorEntry = new PivotalTask();
			errorEntry.setDescription("NO_AUTH_TOKEN");
			tasks = new PivotalTask[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return tasks;
	}

	@Override
	public PivotalComment[] storyCommentsPivotalTracker(int projectId,
			int storyId, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalComment[] comments = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get stories per project
			String commentsProjectJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/comments", "GET", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!commentsProjectJson.contains("Error")) {
				// Transform stories
				comments = gson.fromJson(commentsProjectJson,
						PivotalComment[].class);

				// Save new authentication token in class for further use
				return comments;
			} else {
				// Save error for debug
				PivotalComment errorEntry = new PivotalComment();
				errorEntry.setText("Error: " + commentsProjectJson
						+ " COMMENTS");
				comments = new PivotalComment[] { errorEntry };
			}
		} else {
			PivotalComment errorEntry = new PivotalComment();
			errorEntry.setText("NO_AUTH_TOKEN");
			comments = new PivotalComment[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return comments;
	}

	@Override
	public PivotalActivity[] storyActivitiesPivotalTracker(int projectId,
			int storyId, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalActivity[] activities = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get stories per project
			String activitiesProjectJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/activity", "GET", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!activitiesProjectJson.contains("Error")) {
				// Transform stories
				activities = gson.fromJson(activitiesProjectJson,
						PivotalActivity[].class);

				// Save new authentication token in class for further use
				return activities;
			} else {
				// Save error for debug
				PivotalActivity errorEntry = new PivotalActivity();
				errorEntry.setMessage("Error: " + activitiesProjectJson
						+ " ACTIVITIES");
				activities = new PivotalActivity[] { errorEntry };
			}
		} else {
			PivotalActivity errorEntry = new PivotalActivity();
			errorEntry.setMessage("NO_AUTH_TOKEN");
			activities = new PivotalActivity[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return activities;
	}

	@Override
	public PivotalLabel[] storyLabelsPivotalTracker(int projectId, int storyId,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalLabel[] labels = null;
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get stories per project
			String labelsProjectJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId + "/labels", "GET", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!labelsProjectJson.contains("Error")) {
				// Transform stories
				labels = gson.fromJson(labelsProjectJson, PivotalLabel[].class);

				// Save new authentication token in class for further use
				return labels;
			} else {
				// Save error for debug
				PivotalLabel errorEntry = new PivotalLabel();
				errorEntry.setName("Error: " + labelsProjectJson
						+ " ACTIVITIES");
				labels = new PivotalLabel[] { errorEntry };
			}
		} else {
			PivotalLabel errorEntry = new PivotalLabel();
			errorEntry.setName("NO_AUTH_TOKEN");
			labels = new PivotalLabel[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return labels;
	}

	@Override
	public PivotalStory storyFullInfo(int projectId, int storyId,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalStory story = new PivotalStory();
		if (apiKey != null) {

			story = storiesPivotalTrackerFilteredById(storyId, projectId, req)[0];
			story.setLabels(storyLabelsPivotalTracker(projectId, storyId, req));
			story.setTasks(storyTasksPivotalTracker(projectId, storyId, req));
			story.setActivities(storyActivitiesPivotalTracker(projectId,
					storyId, req));
			story.setComments(storyCommentsPivotalTracker(projectId, storyId,
					req));

		} else {
			story.setDescription("NO_AUTH_TOKEN");
		}

		// Stories can be with or without error, verified in the front end
		return story;
	}

	@Override
	public PivotalStory[] storyInfoAdd(int projectId, String name,
			float estimate, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);
		String current_state = "started";

		name = URLDecoder.decode(name);

		PivotalStory[] stories = null;
		if (apiKey != null) {
			JsonFactory jFactory = new JsonFactory();
			StringWriter writer = new StringWriter();
			/*** write to string ***/
			JsonGenerator jsonGenerator;
			try {
				jsonGenerator = jFactory.createJsonGenerator(writer);
				jsonGenerator.writeStartObject();
				jsonGenerator.writeStringField("current_state", current_state);
				jsonGenerator.writeNumberField("estimate", estimate);
				jsonGenerator.writeStringField("name", name);
				jsonGenerator.writeEndObject();
				jsonGenerator.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String jsonStory = writer.toString();

			// Get stories per project
			String storiesCommentJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories", "POST", jsonStory, "X-TrackerToken",
					apiKey, null, "application/json", true, null);

			// If no error
			if (!storiesCommentJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storiesPivotalTracker(projectId, req);
			} else {
				// Save error for debug
				PivotalStory errorEntry = new PivotalStory();
				errorEntry.setName("Error: " + storiesCommentJson + " STORY");
				stories = new PivotalStory[] { errorEntry };
			}
		} else {
			PivotalStory errorEntry = new PivotalStory();
			errorEntry.setName("NO_AUTH_TOKEN");
			stories = new PivotalStory[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return stories;
	}

	@Override
	public PivotalStory[] storyInfoDelete(int projectId, int storyId,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalStory[] stories = null;
		if (apiKey != null) {

			// Get stories per project
			String storyJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.projectsUrl + "/" + projectId
							+ "/stories/" + storyId, "DELETE", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// Fix to check for errors
			if (storyJson.contains("204") && storyJson.contains("OK")) {
				storyJson = "OK";
			}

			// If no error
			if (!storyJson.contains("Error")) {
				// Save new authentication token in class for further use
				return storiesPivotalTracker(projectId, req);
			} else {
				// Save error for debug
				PivotalStory errorEntry = new PivotalStory();
				errorEntry.setName("Error: " + storyJson + " STORY");
				stories = new PivotalStory[] { errorEntry };
			}
		} else {
			PivotalStory errorEntry = new PivotalStory();
			errorEntry.setName("NO_AUTH_TOKEN");
			stories = new PivotalStory[] { errorEntry };
		}

		// Stories can be with or without error, verified in the front end
		return stories;
	}

	@Override
	public PivotalNotifications notificationsPivotalTracker(
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Pivotal Tracker user authentication key from database
		String apiKey = getApiKeyDB(u, false);

		PivotalNotifications notifications = new PivotalNotifications();
		if (apiKey != null) {
			// Deserialize class object
			Gson gson = new Gson();

			// Get stories per project
			String notificationsProjectJson = ApiInteraction.HTTPRequest(
					PivotalTrackerURIs.userInfoUrl
							+ "/notifications?envelope=true", "GET", null,
					"X-TrackerToken", apiKey, null, null, true, null);

			// If no error
			if (!notificationsProjectJson.contains("Error")) {
				// Transform stories
				notifications = gson.fromJson(notificationsProjectJson,
						PivotalNotifications.class);

				// Save new authentication token in class for further use
				return notifications;
			} else {
				// Save error for debug
				PivotalNotificationData errorEntry = new PivotalNotificationData();
				errorEntry.setMessage("Error: " + notificationsProjectJson
						+ " STORIES");
				notifications
						.setData(new PivotalNotificationData[] { errorEntry });
			}
		} else {
			PivotalNotificationData errorEntry = new PivotalNotificationData();
			errorEntry.setMessage("NO_AUTH_TOKEN");
			notifications.setData(new PivotalNotificationData[] { errorEntry });
		}

		// Stories can be with or without error, verified in the front end
		return notifications;
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/pivotaltracker.css"));
		ui.addJsFile(new JsFile(ui, "/pivotaltracker.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-envelope");
		msm.setName("Pivotal Tracker Integration");
		msm.setId("slackMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Configuration");
		ssm.setHref("/");
		ssm.setTemplate("/pivotaltracker.html");
		ssm.setId("pivotaltrackerkMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Projects");
		ssm2.setHref("/projects");
		ssm2.setTemplate("/projects.html");
		ssm2.setId("pivotaltrackerMainMultiSub2");

		SubSideMenu ssm3 = new SubSideMenu(ui);
		ssm3.setName("Stories");
		ssm3.setHref("/stories");
		ssm3.setTemplate("/stories.html");
		ssm3.setId("pivotaltrackerMainMultiSub3");

		SubSideMenu ssm4 = new SubSideMenu(ui);
		ssm4.setName("Notifications");
		ssm4.setHref("/notifications");
		ssm4.setTemplate("/notifications.html");
		ssm4.setId("pivotaltrackerMainMultiSub4");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
		msm.addSubSideMenu(ssm3);
		msm.addSubSideMenu(ssm4);
	}
}
