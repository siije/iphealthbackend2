package trunk.modules.maths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.annotations.TrunkWorkflowEvent;
import trunk.base.annotations.TrunkWorkflowFeature;
import trunk.base.module.ITrunkModuleBase;

/*
 * Base URL for module.
 *
 * In this case the URL would be http://localhost:8080/maths/<feature id> 
 */
@RequestMapping("/maths")
/*
 * id: directly maps to module folder name under webapp/modules/<module id>
 * 
 * module id must be unique
 */
@TrunkModuleFeature(id = "maths", displayName = "Maths Module", addToDB = true, forSuperAdmin = false)
/*
 * All module base interfaces must extend ITrunkModuleBase
 */
public interface IMathsController extends ITrunkModuleBase {

	@TrunkWorkflowFeature(
			/*
			 * View associated with this feature
			 */
			view = "/workflow/addWorkflow.html",
			/*
			 * CSS files to load during feature preview
			 */
			cssFiles = {
					"/maths.css",
			},
			/*
			 * Javascript files to be loaded before initializing the view
			 */
			jsFiles = {
					"/maths.js",
			},
			/*
			 * CSS files to load when settings dialog is loaded
			 */
			settingsCssFiles = {
					"/maths.css",
			},
			/*
			 * View for settings dialog
			 */
			settings = "/workflow/settings.html",

			/*
			 * The Javascript files to load before the settings dialog is
			 * displayed
			 */
			settingsJsFiles = { "/workflow/settings.js"
			},

			/*
			 * NOTE: Ignore for now
			 */
			events = {
					@TrunkWorkflowEvent(id = "odd", displayName = "Calculated number is odd"),
					@TrunkWorkflowEvent(id = "even", displayName = "Calculated number is even")
			},
			/*
			 * NOTE:Ignore for now
			 */
			actionable = true)
	/*
	 * id: feature id. Must be the same as RequestMapping value and must be
	 * unique
	 */
	@TrunkModuleFeature(id = "add", displayName = "Add Two Numbers")
	/*
	 * Must be the same as feature id.
	 * 
	 * Second part of the web service URL
	 * 
	 * In this case, the URL would be http://localhost:8080/maths/add
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	AddResult add(

			/*
			 * RequestParam value name is the name of the parameter in the json
			 * object parameter passed in while making the request
			 * 
			 * Example: var parameter = {'num1': 1, 'num2': 2}
			 */
			@RequestParam(value = "num1", required = true) int num1,
			@RequestParam(value = "num2", required = true) int num2,

			/*
			 * HttpServletRequest and HttpServletResponse gets injected
			 * automatically
			 */
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);

	@TrunkWorkflowFeature(
			view = "/workflow/subtractWorkflow.html",
			jsFiles = {
					"/maths.js",
			},
			settings = "/workflow/settings.html",
			settingsJsFiles = { "/workflow/settings.js"
			},
			events = {
					@TrunkWorkflowEvent(id = "positive", displayName = "Calculated number is positive"),
					@TrunkWorkflowEvent(id = "negative", displayName = "Calculated number is negative")
			},
			actionable = true)
	@TrunkModuleFeature(id = "subtract", displayName = "Subtract Numbers")
	@RequestMapping(value = "/subtract", method = RequestMethod.POST)
	@ResponseBody
	AddResult subtract(
			@RequestParam(value = "num1", required = true) int num1,
			@RequestParam(value = "num2", required = true) int num2,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);
	
	// JCLOG
	@TrunkWorkflowFeature(
			view = "/workflow/multiplyWorkflow.html",
			jsFiles = {
					"/maths.js",
			},
			settings = "/workflow/settings.html",
			settingsJsFiles = { "/workflow/settings.js"
			},
			events = {
					@TrunkWorkflowEvent(id = "positive", displayName = "Calculated number is positive"),
					@TrunkWorkflowEvent(id = "negative", displayName = "Calculated number is negative")
			},
			actionable = true)
	@TrunkModuleFeature(id = "multiply", displayName = "Multiply Numbers")
	@RequestMapping(value = "/multiply", method = RequestMethod.POST)
	@ResponseBody
	AddResult multiply(
			@RequestParam(value = "num1", required = true) int num1,
			@RequestParam(value = "num2", required = true) int num2,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);


	//Danielle
	@TrunkWorkflowFeature(
			view = "/workflow/divideWorkflow.html",
			jsFiles = {
					"/maths.js",
			},
			settings = "/workflow/settings.html",
			settingsJsFiles = { "/workflow/settings.js"
			},
			events = {
					@TrunkWorkflowEvent(id = "positive", displayName = "Calculated number is positive"),
					@TrunkWorkflowEvent(id = "negative", displayName = "Calculated number is negative")
			},
			actionable = true)
	@TrunkModuleFeature(id = "divide", displayName = "divide Numbers")
	@RequestMapping(value = "/divide", method = RequestMethod.POST)
	@ResponseBody
	AddResult divide(
			@RequestParam(value = "num1", required = true) int num1,
			@RequestParam(value = "num2", required = true) int num2,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);

	
}