package trunk.modules.maths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SingleSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;

/*
 * Every module should implement base interface (IMathsController) in this case and extend TrunkModuleBase.
 * 
 * @Controller annotation has to be present as well
 */
@Controller
public class MathsController extends TrunkModuleBase implements IMathsController {

	@Override
	public AddResult add(int num1, int num2, HttpServletRequest req, HttpServletResponse res) {
		AddResult result = new AddResult(num1, num2, num1 + num2);

		/*
		 * NOTE: Ignore for now
		 */
		if (result.getNum1() == 0 && result.getNum2() == 0) {
			returnWorkflowTaskSuccess(req, res); // Default event
		} else if (result.getSum() % 2 == 0) {
			returnWorkflowTaskSuccess("even", req, res); // Even event
		} else {
			returnWorkflowTaskSuccess("odd", req, res); // Odd event
		}

		/*
		 * Return the result. This gets serialized to json
		 */
		return result;
	}

	@Override
	public AddResult subtract(int num1, int num2, HttpServletRequest req, HttpServletResponse res) {
		AddResult result = new AddResult(num1, num2, num1 - num2);

		/*
		 * NOTE: Ignore for now
		 */
		if (result.getSum() == 0) {
			returnWorkflowTaskSuccess(req, res);
		} else if (result.getSum() < 0) {
			returnWorkflowTaskSuccess("negative", req, res);
		} else {
			returnWorkflowTaskSuccess("positive", req, res);
		}

		/*
		 * Return the result. This gets serialized to json
		 */
		return result;
	}
	
	// JCLOG
	@Override
	public AddResult multiply(int num1, int num2, HttpServletRequest req, HttpServletResponse res) {
		AddResult result = new AddResult(num1, num2, num1 * num2);

		/*
		 * NOTE: Ignore for now
		 */
		if (result.getSum() == 0) {
			returnWorkflowTaskSuccess(req, res);
		} else if (result.getSum() < 0) {
			returnWorkflowTaskSuccess("negative", req, res);
		} else {
			returnWorkflowTaskSuccess("positive", req, res);
		}

		/*
		 * Return the result. This gets serialized to json
		 */
		return result;
	}	
	// Danielle
	@Override
	public AddResult divide(int num1, int num2, HttpServletRequest req, HttpServletResponse res) {
		AddResult result = new AddResult(num1, num2, num1 / num2);

		/*
		 * NOTE: Ignore for now
		 */
		if (result.getSum() == 0) {
			returnWorkflowTaskSuccess(req, res);
		} else if (result.getSum() < 0) {
			returnWorkflowTaskSuccess("negative", req, res);
		} else if (num2==0) {
			returnWorkflowTaskSuccess("error", req, res);
		} else {
			returnWorkflowTaskSuccess("positive", req, res);
		}

		/*
		 * Return the result. This gets serialized to json
		 */
		return result;
	}	

	/*
	 * Add side menu items for this module. Overridden method from
	 * TrunkModuleBase
	 */
	@Override
	public void initializeUI(ModuleUI ui) {

		/*
		 * NOTE: All file URLS are relative to webapp/modules/<module id>
		 * directory
		 */

		ui.addCssFile(new JsFile(ui, "/maths.css"));
		ui.addJsFile(new JsFile(ui, "/maths.js"));

	    //SingleSideMenu sm = new SingleSideMenu(ui);

		/*
		 * URL of the page. This is the URL after http://localhost:8989/#/
		 */
		//sm.setHref("/");
		//sm.setName("Maths Module");
		//sm.setIcon("fa fa-plus-square");
		/*
		 * View to load then this menu item is clicked
		 */
		//sm.setTemplate("/maths.html");
		//sm.setId("mathsMainSideSingle");

		/*
		 * Attach the side menu to UI
		 */
		/*ui.addSideMenu(sm);

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-plus-square");
		msm.setName("Maths Features");
		msm.setId("mathsMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Add");
		ssm.setHref("/add");
		ssm.setTemplate("/add.html");
		ssm.setId("mathsMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Subtract");
		ssm2.setHref("/subtract");
		ssm2.setTemplate("/subtract.html");
		ssm2.setId("mathsMainMultiSub2");
		
		SubSideMenu ssm3 = new SubSideMenu(ui);
		ssm3.setName("Multiply");
		ssm3.setHref("/multiply");
		ssm3.setTemplate("/multiply.html");
		ssm3.setId("mathsMainMultiSub3");

		
		SubSideMenu ssm4 = new SubSideMenu(ui);
		ssm4.setName("Divide");
		ssm4.setHref("/divide");
		ssm4.setTemplate("/divide.html");
		ssm4.setId("mathsMainMultiSub4");
		
		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
		msm.addSubSideMenu(ssm3);
		msm.addSubSideMenu(ssm4);*/
	}
}