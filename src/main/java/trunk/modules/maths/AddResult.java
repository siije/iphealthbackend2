package trunk.modules.maths;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
public class AddResult {
	private int sum;

	private int num1;
	private int num2;

	/*
	 * A default public empty constructor has to be present for json
	 * serialization to work
	 */
	public AddResult() {
	}

	public AddResult(int num1, int num2, int sum) {
		this.sum = sum;

		this.num1 = num1;
		this.num2 = num2;
	}

	public int getNum1() {
		return num1;
	}

	public void setNum1(int num1) {
		this.num1 = num1;
	}

	public int getNum2() {
		return num2;
	}

	public void setNum2(int num2) {
		this.num2 = num2;
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}
}
