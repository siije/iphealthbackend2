package trunk.modules.linkedin.bean;

public class LinkedinHttpResultBean{
	private String responseCode;
	private String responseBody;
	
	public void setResponseCode(String code){
		this.responseCode = code;
	}
	
	public String getResponseCode(){
		return responseCode;
	}
	
	public void setResponseBody(String body){
		this.responseBody = body;
	}
	
	public String getResponseBody(){
		return responseBody;
	}
}