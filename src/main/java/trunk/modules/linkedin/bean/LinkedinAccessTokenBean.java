package trunk.modules.linkedin.bean;

// Representation of the Asana authentication token
public class LinkedinAccessTokenBean {
	
	private String access_token;
	private String lifetime;
		
	public LinkedinAccessTokenBean(){
		
	}
	
	public String getAccess_token() {
		return access_token;
	}
	
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	
	public String getlifetime() {
		return lifetime;
	}
	
	public void setlifetime(String lifetime) {
		this.lifetime = lifetime;
	}
}
