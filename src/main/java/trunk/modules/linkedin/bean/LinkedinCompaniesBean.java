package trunk.modules.linkedin.bean;

public class LinkedinCompaniesBean{
	private LinkedinCompanyBean[] linkedinCompaniesBean;
	
	public void setLinkedinCompaniesBean(LinkedinCompanyBean[] linkedinCompaniesBean){
		this.linkedinCompaniesBean = linkedinCompaniesBean;
	}
	
	public LinkedinCompanyBean[] getLinkedinCompaniesBean(){
		return linkedinCompaniesBean;
	}
}