package trunk.modules.linkedin.bean;

import trunk.modules.api.utils.ApiResultBase;

public class LinkedinResultBean extends ApiResultBase {
	private String response_type = "code";
	private String state = "YXOEIPWF20453sdffef490";
	private String grant_type = "authorization_code";
	
	public LinkedinResultBean() {
		// Initialize variables for Slack API
		appName = "Verdi API";
		clientId = "7593otil4p6axp";
		clientSecret = "UGBzdX2G0zlT6pSW";
		redirectUrl = "http://localhost:8989/redirectLinkedin.html";
		baseAuthUrl = "https://www.linkedin.com/uas/oauth2/authorization";
		baseTokenUrl = "https://www.linkedin.com/uas/oauth2/accessToken";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}
	
	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		StringBuilder str = new StringBuilder();
		str.append(baseAuthUrl);
		str.append("?response_type=");
		str.append(response_type);
		str.append("&state=");
		str.append(state);
		str.append("&client_id=");	
		str.append(clientId);
		str.append("&redirect_uri=");	
		str.append(redirectUrl);
		
		authUrl = str.toString();
	}
	
	@Override
	public void generateTokenParamsUrl(boolean renew, String authorizationCode) {
		StringBuilder str = new StringBuilder();			
	
		str.append("grant_type=");
		str.append(grant_type);
		str.append("&code=");
		str.append(authorizationCode);
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);
		str.append("&client_secret=");
		str.append(clientSecret);

		tokenParamsUrl = str.toString();
	}
	
}