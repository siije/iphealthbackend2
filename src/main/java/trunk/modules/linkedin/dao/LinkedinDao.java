package trunk.modules.linkedin.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class LinkedinDao extends JdbcDaoSupportBase {
	public void saveLinkedinAccessTokenDB(int uid, String linkedin_key){
		invokeStoredProc(ApiUserBean.class, "update_user_linkedin_key",
				new StoredProcParam(Types.INTEGER, "p_uid", uid),
				new StoredProcParam(Types.VARCHAR, "p_linkedinKey", linkedin_key));
	}
	
	public ApiUserBean getLinkedinAccessTokenDB(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "p_uid", uid));		
	}
}