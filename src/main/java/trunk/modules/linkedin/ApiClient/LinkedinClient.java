package trunk.modules.linkedin.ApiClient;

import org.json.XML;
import org.json.JSONObject;
import org.json.JSONException;

import trunk.modules.api.utils.ApiInteraction;

public class LinkedinClient {
	private String BASE_URL = "https://api.linkedin.com/v1/";
	private ApiInteraction service;
	private String accessToken;
	private String returnResult;
	private String companyBrief = "(id,name,industries,website-url,logo-url)";
	private String companyDetail = "(id,name,universal-name,industries,locations,logo-url,specialties,website-url,company-type,employee-count-range,founded-year,description,num-followers)";

	public LinkedinClient(String ACCESS_TOKEN) {
		accessToken = ACCESS_TOKEN;
	};

	protected String httpRequest(String URL, String type, String content,
			String contentType) {
		String suffix = "oauth2_access_token=" + accessToken;
		return service.HTTPRequest(URL, type, content, null, null, suffix,
				contentType, false, null);
	}

	protected String get(String requestURL) {
		String response = httpRequest(requestURL, "GET", null, null);

		if ("Error".equals(response.substring(0, 5))) {
			return response;
		} else {
			// convert xml to json
			try {
				JSONObject xmlJSONObj = XML.toJSONObject(response);
				returnResult = xmlJSONObj.toString();
			} catch (JSONException e) {
				System.out.println(e.getMessage());
				returnResult = "Error: convert failed!";
			}
			return returnResult;
		}
	}

	protected String post(String requestURL, String content) {
		String response = httpRequest(requestURL, "POST", content, "text/xml");
		return response;
	}

	protected String delete(String requestURL) {
		String response = httpRequest(requestURL, "DELETE", null, null);
		return response;
	}

	protected String put(String requestURL, String content) {
		String response = httpRequest(requestURL, "PUT", content, "text/xml");
		return response;
	}

	public String getPrivateProfile(String id, String field) {
		String user_id = (id == null ? "~" : ("id=" + id));
		String fields = null;

		if ("standard".equals(field)) {
			fields = "";
		} else if ("full".equals(field)) {
			fields = "summary,"
					+ "skills:(id,skill:(name)),"
					+ "positions:(id,title,summary,start-date,end-date,is-current,company:(name,id)),"
					+ "educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes),"
					+ "job-bookmarks," + "suggestions";
		}

		return get(BASE_URL + "people/" + user_id + ":"
				+ "(id,first-name,last-name,picture-url," + fields + ")" + "?");
	}

	public String getMyProfile(String field) {
		return getPrivateProfile(null, field);
	}

	public String searchPeople(String key_word) {
		String request_url = BASE_URL + "people-search?" + key_word + "&";

		System.out.println(request_url);
		return get(request_url);
	}

	public String getConnectionsProfile() {
		return get(BASE_URL
				+ "people/~/connections:(id,first-name,last-name,picture-url)"
				+ "?");
	}

	public String getFollowedCompanies() {
		String request_url = BASE_URL + "people/~/following/companies:"
				+ companyBrief + "?";
		return get(request_url);
	}

	public String getCompanyInfo(String id) {
		String request_url = BASE_URL + "companies/" + id + ":" + companyDetail
				+ '?';
		return get(request_url);
	}

	public String getCompaniesUpdates(String id, String count, String event_type) {
		String request_url = BASE_URL + "companies/" + id + "/updates";
		String condition = "?";

		// set data amount,
		// default 10
		if (count != null) {
			condition = condition + "start=0&count=" + count + "&";
		} else {
			condition = condition + "start=0&count=10&";
		}

		// set event type
		// status-update
		// new-product
		// job-posting
		if (event_type != null) {
			condition = condition + "event-type=" + event_type + "&";
		}
		return get(request_url + condition);
	}

	public String getCompaniesUpdates(String id) {
		return getCompaniesUpdates(id, null, null);
	}

	public String followCompany(String content) {
		String request_url = BASE_URL + "people/~/following/companies" + "?";
		return post(request_url, content);
	}

	public String unfollowCompany(String id) {
		String request_url = BASE_URL + "people/~/following/companies/id=" + id
				+ "?";
		return delete(request_url);
	}

	public String getCompanyUpdateComments(String id, String key,
			String event_type) {
		String request_url = BASE_URL + "companies/" + id + "/updates/key="
				+ key + "/update-comments?event-type=" + event_type;
		return get(request_url);
	}

	public String getCompanyUpdatelikes(String id, String key, String event_type) {
		String request_url = BASE_URL + "companies/" + id + "/updates/key="
				+ key + "/likes?event-type=" + event_type;
		return get(request_url);
	}

	public String postUpdateComment(String update_key, String content) {
		String request_url = BASE_URL + "people/~/network/updates/key="
				+ update_key + "/update-comments?";
		return post(request_url, content);
	}

	public String postUpdateLike(String update_key, String content) {
		String request_url = BASE_URL + "people/~/network/updates/key="
				+ update_key + "/is-liked?";
		return put(request_url, content);
	}

	public String postUpdateShares(String content) {
		String request_url = BASE_URL + "people/~/shares?";
		return post(request_url, content);
	}

	public String getGroups() {
		String request_url = BASE_URL
				+ "people/~/group-memberships:"
				+ "(group:(id,name,small-logo-url,counts-by-category),membership-state,show-group-logo-in-profile)"
				+ "?";
		return get(request_url);
	}

	public String getSuggestedGroups() {
		String request_url = BASE_URL
				+ "people/~/suggestions/groups:(id,name,is-open-to-non-members,small-logo-url,large-logo-url)?";
		return get(request_url);
	}

	public String getGroupDiscussions(String key, String type) {
		String request_url = null;

		if (type == "lastest") {
			request_url = BASE_URL
					+ "groups/"
					+ key
					+ "/posts:(id,title,creation-timestamp,summary,creator:(first-name,last-name,headline),likes,comments,relation-to-viewer:(is-following,is-liked))"
					+ "?category=discussion&order=recency&modified-since=1302727083000&count=3"
					+ "&";
		} else {
			request_url = BASE_URL
					+ "groups/"
					+ key
					+ "/posts:(id,title,creation-timestamp,summary,creator:(first-name,last-name,headline),likes,comments,relation-to-viewer:(is-following,is-liked))"
					+ "?category=discussion&order=popularity&count=5" + "&";
		}
		return get(request_url);
	}

	/*
	 * public String getGroupDiscussionComments(){
	 * 
	 * }
	 */

	public String postDiscussionLike(String key, String content) {
		String request_url = BASE_URL + "posts/" + key
				+ "/relation-to-viewer/is-liked?";
		return put(request_url, content);
	}

	public String postDiscussionFollow(String key, String content) {
		String request_url = BASE_URL + "posts/" + key
				+ "/relation-to-viewer/is-following?";
		return put(request_url, content);
	}

	public String postDiscussionComment(String key, String content) {
		String request_url = BASE_URL + "posts/" + key + "/comments?";
		return post(request_url, content);
	}

	public String postDiscussion(String key, String content) {
		String request_url = BASE_URL + "groups/" + key + "/posts?";
		return post(request_url, content);
	}

	public String getMyConnections() {
		String request_url = BASE_URL
				+ "people/~/connections:(id,first-name,last-name,headline,location,picture-url,public-profile-url)"
				+ "?";
		return get(request_url);
	}

	public String sendMessage(String content) {
		String request_url = BASE_URL + "people/~/mailbox" + "?";
		return post(request_url, content);
	}

	public String getNetworkUpdates(String start, String count) {
		String Count = ((count != null) ? count : "10");
		String Start = ((start != null) ? start : "0");

		String request_url = BASE_URL + "people/~/network/updates?count="
				+ Count + "&start=" + Start + "&type=CONN" + "&type=JOBS"
				+ "&type=JGRP" + "&type=PICT" + "&type=PFOL" + "&type=PRFX"
				+ "&type=RECU" + "&type=PRFU" + "&type=SHAR" + "&type=VIRL"
				+ "&";

		return get(request_url);
	}

	public String getNetworkUpdates() {
		return getNetworkUpdates(null, null);
	}

	public String getNetworkStatistic() {
		String request_url = BASE_URL + "people/~/network/network-stats" + "?";
		return get(request_url);
	}

	public String getNetworkStatus() {
		String request_url = BASE_URL + "people/~/network/network-stats" + "?";
		return get(request_url);
	}

	public String postNetworkComment(String key, String content) {
		String request_url = BASE_URL + "people/~/network/updates/key=" + key
				+ "/update-comments" + "?";
		return post(request_url, content);
	}

	public String postNetworkLike(String key, String like) {
		String request_url = BASE_URL + "people/~/network/updates/key=" + key
				+ "/is-liked" + "?";
		return put(request_url, like);
	}

	public String postNetworkUpdate(String content) {
		String request_url = BASE_URL + "people/~/person-activities" + "?";
		return post(request_url, content);
	}
}