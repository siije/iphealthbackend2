package trunk.modules.linkedin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.api.utils.ApiInteraction;
import trunk.modules.linkedin.bean.LinkedinResultBean;
import trunk.modules.linkedin.bean.LinkedinHttpResultBean;
import trunk.modules.linkedin.bean.LinkedinAccessTokenBean;
import trunk.modules.linkedin.dao.LinkedinDao;
import trunk.modules.linkedin.ApiClient.LinkedinClient;;

@Controller
public class LinkedinController extends TrunkModuleBase implements ILinkedinController {
	private LinkedinResultBean linkedinResultBean = new LinkedinResultBean();
	private LinkedinClient linkedinClient;
	
	@Override
	public LinkedinHttpResultBean connLinkedin(HttpServletRequest req, HttpServletResponse resp){
		LinkedinDao linkedinOAuthDao = ApplicationContextProvider.getContext().getBean(LinkedinDao.class);
		ApiUserBean linkedinUser = linkedinOAuthDao.getLinkedinAccessTokenDB(SessionProvider.getUser(req).getId());
		
		LinkedinHttpResultBean response = new LinkedinHttpResultBean();
		
		if (linkedinUser.getLinkedin_key() != null){
			linkedinClient = new LinkedinClient(linkedinUser.getLinkedin_key());
			
			response.setResponseCode("200");
			response.setResponseBody(linkedinUser.getLinkedin_key());
		} else{
			response.setResponseCode("l-001");
			response.setResponseBody(linkedinResultBean.getAuthUrl());
		}
		
		return response;
	}
	
	@Override
	public String getOAuthURL(){
		return linkedinResultBean.getAuthUrl();
	};
	
	@Override
	public LinkedinHttpResultBean configLinkedin(String code, HttpServletRequest req,HttpServletResponse resp){
		// generate access token url
		linkedinResultBean.generateTokenParamsUrl(true, code);
		LinkedinHttpResultBean response = new LinkedinHttpResultBean();
		
		// get access token
		String accessToken = ApiInteraction.getAccessToken(linkedinResultBean);
		
		if (!accessToken.contains("Error")) {
			Gson gson = new Gson();
			LinkedinAccessTokenBean linkedinAccessToken = gson.fromJson(accessToken, LinkedinAccessTokenBean.class);
			
			LinkedinDao linkedinOAuthDao = ApplicationContextProvider.getContext().getBean(LinkedinDao.class);
			linkedinOAuthDao.saveLinkedinAccessTokenDB(SessionProvider.getUser(req).getId(), linkedinAccessToken.getAccess_token());;
			
			response.setResponseCode("200");
			response.setResponseBody("success to connect Linkedin");
			
			return response;
		} else {
			// Save error for debug
			response.setResponseCode("l-002");
			response.setResponseBody(accessToken);
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getMyProfile(String field,HttpServletRequest req,HttpServletResponse resp){
		// Connect to linkedin test
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.getMyProfile(field);

			if ("Error".equals(result.substring(0,5))){
				response.setResponseBody("Can't connect to Linkedin");
				response.setResponseCode("Connected failed");	
			}else{
				LinkedinHttpResultBean myFollowedCompanies = getMyFollowedCompanies(req, resp);
				
				// combine result
				if ("200".equals(myFollowedCompanies.getResponseCode())){
					result = result.substring(0,result.length()-1) + ',' + myFollowedCompanies.getResponseBody().substring(1);
				}
				
				response.setResponseBody(result);
				response.setResponseCode("200");	
			}
			
			//System.out.println(result);
			return response;
		// else return error
		}else{
			return response;
		}
	}

	@Override
	public LinkedinHttpResultBean getMyFollowedCompanies(HttpServletRequest req,HttpServletResponse resp){
		// Connect to linkedin test
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.getFollowedCompanies();
			
			if(result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Get followed company failed");
				response.setResponseBody("Run out of the limits");
			}else if ("Error".equals(result.substring(0,5))){
				response.setResponseBody("Can't connect to Linkedin");
				response.setResponseCode("Get followed company failed");	
			}else{
				response.setResponseBody(result);
				response.setResponseCode("200");	
			}
			
			return response;
		// else return error
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getFollowedCompaniesUpdates(HttpServletRequest req,HttpServletResponse resp){
		// Connect to linkedin test
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			// get company id first
			LinkedinHttpResultBean myFollowedCompanies = getMyFollowedCompanies(req, resp);
			
			String companiesS = myFollowedCompanies.getResponseBody();
			String[] companies =  companiesS.substring(companiesS.indexOf("[{")+1, companiesS.indexOf("}]")-1).split("},");
			String companiesUpdates = "{" + companiesS.substring(1, companiesS.length()-1) + ",\"company_updates\":[";
			
			// get company updates one by one
			// combine the result
			for (String company: companies){
				if (company.contains("id")){
					// +4 avoid string id":
					String company_id = company.substring(company.indexOf("id") + 4,company.indexOf(','));
					String result = linkedinClient.getCompaniesUpdates(company_id);
					
					if (result.contains("403") && "Error".equals(result.substring(0,5))){
						response.setResponseCode("Get company updates failed");
						response.setResponseBody("Run out of the limits");
						
						break;
					}else if("Error".equals(result.substring(0,5))){
						response.setResponseCode("Get company updates failed");
						response.setResponseBody(result);
						
						break;
					}else{
						companiesUpdates = companiesUpdates + result + ',';
					}
					
				}
			}
			
			if ("200".equals(response.getResponseCode())){
				companiesUpdates = companiesUpdates.substring(0,companiesUpdates.length()-1) + "]}";
			
				response.setResponseBody(companiesUpdates);
				response.setResponseCode("200");	
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getGroups(HttpServletRequest req, HttpServletResponse resp){
		// Connect to linkedin test
		LinkedinHttpResultBean response = connLinkedin(req,resp);
				
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.getGroups();
			
			if (result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Get groups failed");
				response.setResponseBody("Run out of the limits");
			}else if("Error".equals(result.substring(0,5))){
				response.setResponseCode("Get groups failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody(result);
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getSuggestedGroups(HttpServletRequest req,HttpServletResponse resp){
		// Connect to linkedin test
		LinkedinHttpResultBean response = connLinkedin(req,resp);
				
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.getSuggestedGroups();
			
			if (result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Get suggested group failed");
				response.setResponseBody("Run out of the limits");
			}else if("Error".equals(result.substring(0,5))){
				response.setResponseCode("Get suggested group failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody(result);
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getGroupDiscussions(String key, String type, HttpServletRequest req, HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String discussions = "[";
			String[] keys = key.split(",");
			
			for(String group_key: keys){
				String result = linkedinClient.getGroupDiscussions(group_key, type);
				
				if (result.contains("403") && "Error".equals(result.substring(0,5))){
					response.setResponseCode("Get group discussion failed");
					response.setResponseBody("Run out of the limits");
					break;
				}else if("Error".equals(result.substring(0,5))){
					response.setResponseCode("Get group discussion failed");
					response.setResponseBody(result);
					
					break;
				}else{
					discussions = discussions + result + ",";
				}
			}
			
			if ("200".equals(response.getResponseCode())){
				discussions = discussions.substring(0, discussions.length()-1) + "]";
				response.setResponseCode("200");
				response.setResponseBody(discussions);
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getCompanyDetail(String companyId, HttpServletRequest req, HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.getCompanyInfo(companyId);
			
			if(result.contains("403") && result.contains("Error")){
				response.setResponseCode("Get company detail failed");
				response.setResponseBody("Run out of the limits");
				
			}else if (result.contains("Error")){
				response.setResponseCode("Get company detail failed");
				response.setResponseBody(result);
				
			}else{
				response.setResponseCode("200");
				response.setResponseBody(result);
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean followCompany(String companyId, String type, HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = "Error, can't find company id";
			
			if (companyId != null){
				String follow =   "<company>"
								+ "<id>" + companyId + "</id>"
								+ "</company>";
				if ("follow".equals(type)){
					result = linkedinClient.followCompany(follow);
				} else if ("unfollow".equals(type)){
					result = linkedinClient.unfollowCompany(companyId);
				}
			}
			
			if(result.contains("403") && result.contains("Error")){
				response.setResponseCode("Follow company failed");
				response.setResponseBody("Run out of the limits");
				
			}else if (result.contains("Error")){
				response.setResponseCode("Follow company  failed");
				response.setResponseBody(result);
				
			}else{
				response.setResponseCode("200");
				response.setResponseBody("Follow company successful");
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean addComment(String key,String type, String content, HttpServletRequest req, HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = null;
			System.out.println(key);
			System.out.println(type);
			
			if ("company".equals(type)){
				String comment = "<update-comment><comment>" + content + "</comment></update-comment>";
				result = linkedinClient.postUpdateComment(key, comment);
			} else if("discussion".equals(type)){
				String comment = "<comment><text>" + content + "</text></comment>";
				result = linkedinClient.postDiscussionComment(key, comment);
			} else if("network".equals(type)){
				String comment = "<update-comment><comment>" + content + "</comment></update-comment> ";
				result = linkedinClient.postNetworkComment(key, comment);
			}
			
			if(result.contains("403") && result.contains("Error")){
				response.setResponseCode("Add comment failed");
				response.setResponseBody("Run out of the limits");
			}else if (result.contains("Error")){
				response.setResponseCode("Add comment failed");
				response.setResponseBody(result);
			}
			else{
				response.setResponseCode("200");
				response.setResponseBody("Added comment successful!");
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean addFollow(String key,String type,HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = null;
			
			if ("follow".equals(type)){
				String follow = "<is-following>true</is-following>";
				result = linkedinClient.postDiscussionFollow(key, follow);
			}else if("unfollow".equals(type)){
				String follow = "<is-following>false</is-following>";
				result = linkedinClient.postDiscussionFollow(key, follow);
			}else{
				result = "Error, unknow type!";
			}
			
			System.out.println(result);
			
			if(result.contains("403") && result.contains("Error")){
				response.setResponseCode("Followed failed");
				response.setResponseBody("Run out of the limits");
			}else if (result.contains("Error") && !result.contains("204")){
				response.setResponseCode("Followed failed");
				response.setResponseBody(result);
			}
			else{
				response.setResponseCode("200");
				response.setResponseBody(type + "ed successful!");
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean addLike(String key, String field ,String type, HttpServletRequest req, HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String like = null;
			String result = "Error";
			
			if ("like".equals(type)){
				like = "<is-liked>true</is-liked>";
			} else if("unlike".equals(type)){
				like = "<is-liked>false</is-liked>";
			}

			if ("company".equals(field)){
				result = linkedinClient.postUpdateLike(key, like);
			}else if("discussion".equals(field)){
				result = linkedinClient.postDiscussionLike(key, like);
			}else if("network".equals(field)){
				result = linkedinClient.postNetworkLike(key, like);
			}

			if(result.contains("403") && result.contains("Error")){
				response.setResponseCode("Liked Failed");
				response.setResponseBody("Run out of the limits");
			
			}else if (result.contains("Error") && !result.contains("204")){
				response.setResponseCode("Liked Failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody("Liked successful!");
			}
			
			return response;
		}else{
			return response;
		}
	};
	
	@Override
	public LinkedinHttpResultBean addShare(String comment, String content, String visibility, HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String share = "<share>" 
						+ comment
						+ content
						+ visibility
						+"</share>";
			String result = linkedinClient.postUpdateShares(share);
			
			if(result.contains("403") && result.contains("Error")){
				response.setResponseCode("Added share failed");
				response.setResponseBody("Run out of the limits");
			}else if (result.contains("Error")){
				response.setResponseCode("Added share failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody("Shared successful!");
			}
			
			return response;
		}else{
			return response;
		}
	};
	
	@Override
	public LinkedinHttpResultBean postDiscussion(String key, String content, HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.postDiscussion(key,content);
			
			if(result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Posted discussion failed");
				response.setResponseBody("Run out of the limits");
			}else if ("Error".equals(result.substring(0,5))){
				response.setResponseCode("Posted discussion failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody("Added discussion successful!");
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getMyConnections(HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.getMyConnections();
			
			if(result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Get connections failed");
				response.setResponseBody("Run out of the limits");
			}else if ("Error".equals(result.substring(0,5))){
				response.setResponseCode("Get connections failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody(result);
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean sendMessage(String message,HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.sendMessage(message);
			
			if(result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Send Message Error:");
				response.setResponseBody("Run out of the limits");
			}else if ("Error".equals(result.substring(0,5))){
				response.setResponseCode("Send Message Error:");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody("Send Message Successful");
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getNetworkUpdates(String start, String count, HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = "Error";
			
			if (start != null || count != null){
				result = linkedinClient.getNetworkUpdates(start, count);
			}else{
				result = linkedinClient.getNetworkUpdates();
			}
			
			if(result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Get network updates failed");
				response.setResponseBody("Run out of the limits");
			}else if ("Error".equals(result.substring(0,5))){
				response.setResponseCode("Get network updates failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody(result);
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean getNetworkStatistic(HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.getNetworkStatistic();

			if(result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Get network updates failed");
				response.setResponseBody("Run out of the limits");
			}else if ("Error".equals(result.substring(0,5))){
				response.setResponseCode("Get network updates failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody(result);
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean postNetworkUpdates(String content,HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = linkedinClient.postNetworkUpdate(content);
			
			System.out.println(result);
			
			if(result.contains("403") && result.contains(result)){
				response.setResponseCode("Post update failed");
				response.setResponseBody("Run out of the limits");
			}else if (result.contains(result)){
				response.setResponseCode("Post update failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody("Post update successful");
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public LinkedinHttpResultBean search(String type, String keywords,HttpServletRequest req,HttpServletResponse resp){
		LinkedinHttpResultBean response = connLinkedin(req,resp);
		
		// if get access token successfully, then retrieve data 
		if ("200".equals(response.getResponseCode())){
			String result = "Error";
			System.out.println(keywords);
			System.out.println(type);
			
			if ("people".equals(type)){
				result = linkedinClient.searchPeople(keywords);
				System.out.println(result);
			}
			
			System.out.println(result);
			
			if(result.contains("403") && "Error".equals(result.substring(0,5))){
				response.setResponseCode("Search people failed");
				response.setResponseBody("Run out of the limits");
			}else if ("Error".equals(result.substring(0,5))){
				response.setResponseCode("Search people failed");
				response.setResponseBody(result);
			}else{
				response.setResponseCode("200");
				response.setResponseBody(result);
			}
			
			return response;
		}else{
			return response;
		}
	}
	
	@Override
	public void initializeUI(ModuleUI ui) {
		ui.addCssFile(new JsFile(ui, "/controllers/linkedin.css"));
		ui.addJsFile(new JsFile(ui, "/controllers/linkedin.js"));
		
		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setName("Linkedin");
		msm.setIcon("fa fa-linkedin");
		msm.setId("linkedinApi");
		
		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setHref("/myLinkedin");
		ssm.setName("My Linkedin ");
		ssm.setTemplate("/LinkedinDashboard.html");
		ssm.setId("g2myLinkedin");
		
		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setHref("/reserved");
		ssm2.setName("Reserved");
		ssm2.setTemplate("/LinkedinDashboard.html");
		ssm2.setId("reserved");
		
		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
		ui.addSideMenu(msm);
	}
}