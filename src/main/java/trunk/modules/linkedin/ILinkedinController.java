package trunk.modules.linkedin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.linkedin.bean.LinkedinHttpResultBean;

@RequestMapping("/linkedin")
@TrunkModuleFeature(id = "linkedin", displayName = "Linked API", enabled = true)
public interface ILinkedinController extends ITrunkModuleBase {
	@TrunkModuleFeature(id = "connLinkedin", displayName = "config")
	@RequestMapping(value = "/connLinkedin", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean connLinkedin(
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getOAuthURL", displayName = "getOAuthURL")
	@RequestMapping(value = "/getOAuthURL", method = RequestMethod.GET)
	@ResponseBody
	String getOAuthURL();
	
	
	@TrunkModuleFeature(id = "configLinkedin", displayName = "configLinkedin")
	@RequestMapping(value = "/configLinkedin", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean configLinkedin(
			@RequestParam(value = "code", required = true) String code,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getMyProfile", displayName = "getMyProfile")
	@RequestMapping(value = "/getMyProfile", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getMyProfile(
			@RequestParam(value = "field", required = true) String field,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getMyFollowedCompanies", displayName = "getMyFollowedCompanies")
	@RequestMapping(value = "/getMyFollowedCompanies", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getMyFollowedCompanies(
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getCompanyDetail", displayName = "getCompanyDetail")
	@RequestMapping(value = "/getCompanyDetail", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getCompanyDetail(
			@RequestParam(value = "companyId", required = true) String companyId,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getFollowedCompaniesUpdates", displayName = "getFollowedCompaniesUpdates")
	@RequestMapping(value = "/getFollowedCompaniesUpdates", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getFollowedCompaniesUpdates(
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "followCompany", displayName = "followCompany")
	@RequestMapping(value = "/followCompany", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean followCompany(
			@RequestParam(value = "companyId", required = true) String companyId,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "addComment", displayName = "addComment")
	@RequestMapping(value = "/addComment", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean addComment(
			@RequestParam(value = "key", required = true) String key,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "content", required = true) String content,	
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "addLike", displayName = "addLike")
	@RequestMapping(value = "/addLike", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean addLike(
			@RequestParam(value = "key", required = true) String key,
			@RequestParam(value = "field", required = true) String field,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "addFollow", displayName = "addFollow")
	@RequestMapping(value = "/addFollow", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean addFollow(
			@RequestParam(value = "key", required = true) String key,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "addShare", displayName = "addShare")
	@RequestMapping(value = "/addShare", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean addShare(
			@RequestParam(value = "comment", required = true) String comment,
			@RequestParam(value = "content", required = true) String content,
			@RequestParam(value = "visibility", required = true) String visibility,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getGroups", displayName = "getGroups")
	@RequestMapping(value = "/getGroups", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getGroups(
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getSuggestedGroups", displayName = "getSuggestedGroups")
	@RequestMapping(value = "/getSuggestedGroups", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getSuggestedGroups(
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getGroupDiscussions", displayName = "getGroupDiscussions")
	@RequestMapping(value = "/getGroupDiscussions", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getGroupDiscussions(
			@RequestParam(value = "key", required = true) String key,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "postDiscussion", displayName = "postDiscussion")
	@RequestMapping(value = "/postDiscussion", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean postDiscussion(
			@RequestParam(value = "key", required = true) String key,
			@RequestParam(value = "content", required = true) String content,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getMyConnections", displayName = "getMyConnections")
	@RequestMapping(value = "/getMyConnections", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getMyConnections(
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "sendMessage", displayName = "sendMessage")
	@RequestMapping(value = "/sendMessage", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean sendMessage(
			@RequestParam(value = "message", required = true) String message,	
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getNetworkUpdates", displayName = "getNetworkUpdates")
	@RequestMapping(value = "/getNetworkUpdates", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getNetworkUpdates(
			@RequestParam(value = "start", required = true)String start, 
			@RequestParam(value = "count", required = true) String count,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getNetworkStatistic", displayName = "getNetworkStatistic")
	@RequestMapping(value = "/getNetworkStatistic", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean getNetworkStatistic(
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "postNetworkUpdates", displayName = "postNetworkUpdates")
	@RequestMapping(value = "/postNetworkUpdates", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean postNetworkUpdates(
			@RequestParam(value = "content", required = true) String content,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "search", displayName = "search")
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	@ResponseBody
	LinkedinHttpResultBean search(
			@RequestParam(value = "type", required = true) String type,	
			@RequestParam(value = "keywords", required = true) String keywords,	
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
}