package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Contact")
public class Contact{
	@XmlTransient
	private String ContactID;
	@XmlTransient
	private String ContactStatus;
	@XmlTransient
	private String Name;
	
	@XmlElement(name="Addresses", type=Addresses.class)
	private Addresses addresses;
	
	@XmlElement(name="Phones", type=Phones.class)
	private Phones phones;
	
	@XmlTransient
	private String UpdatedDateUTC;
	@XmlTransient
	private String IsSupplier;
	@XmlTransient
	private String IsCustomer;
	@XmlTransient
	private String HasAttachments;
	
	@XmlElement(name="ContactID")
	public void setContactID(String contactID){
		this.ContactID = contactID;
	}
	
	public String getContactID(){
		return ContactID;
	}
	
	@XmlElement(name="ContactStatus")
	public void setContactStatus(String contactStatus){
		this.ContactStatus = contactStatus;
	}
	
	public String getContactStatus(){
		return ContactStatus;
	}
	
	@XmlElement(name="Name")
	public void setName(String name){
		this.Name = name;
	}
	
	public String getName(){
		return Name;
	}	
	
	public Addresses getAddresses(){
		return addresses;
	}
	
	public Phones getPhones(){
		return phones;
	}

	@XmlElement(name="UpdatedDateUTC")
	public void setUpdatedDateUTC(String updatedDateUTC){
		this.UpdatedDateUTC = updatedDateUTC;
	}
	
	public String getUpdatedDateUTC(){
		return UpdatedDateUTC;
	}
	
	@XmlElement(name="IsSupplier")
	public void setIsSupplier(String isSupplier){
		this.IsSupplier = isSupplier;
	}
	
	public String getIsSupplier(){
		return IsSupplier;
	}
	
	@XmlElement(name="IsCustomer")
	public void setIsCustomer(String isCustomer){
		this.IsCustomer = isCustomer;
	}
	
	public String getIsCustomer(){
		return IsCustomer;
	}
	
	@XmlElement(name="HasAttachments")
	public String getHasAttachments(){
		return HasAttachments;
	}
	
	public void setHasAttachments(String hasAttachments){
		this.HasAttachments = hasAttachments;
	}

}