package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Item")
public class Item{
	@XmlTransient
	private String ItemID;
	@XmlTransient
	private String Code;
	@XmlTransient
	private String Description;
	@XmlTransient
	private String UpdatedDateUTC;

	@XmlElement(name="PurchaseDetails", type=ItemPurchaseDetail.class)
	private ItemPurchaseDetail itemPurchaseDetails;
	
	public ItemPurchaseDetail getPurchaseDetails(){
		return itemPurchaseDetails;
	}
	
	public void setItemPurchaseDetail(ItemPurchaseDetail itemPurchaseDetails){
		this.itemPurchaseDetails = itemPurchaseDetails;
	}
	
	@XmlElement(name="SalesDetails", type=ItemSalesDetail.class)
	private ItemSalesDetail itemSalesDetail;
	
	public ItemSalesDetail getItemSalesDetail(){
		return itemSalesDetail;
	}
	
	public void setItemSalesDetail(ItemSalesDetail itemSalesDetail){
		this.itemSalesDetail = itemSalesDetail;
	}
	
	@XmlElement(name="ItemID")
	public void setItemID(String ItemID){
		this.ItemID = ItemID;
	}
	
	public String getItemID(){
		return ItemID;
	}
	
	@XmlElement(name="Code")
	public void setCode(String Code){
		this.Code = Code;
	}
	
	public String getCode(){
		return Code;
	}
	
	@XmlElement(name="Description")
	public void setDescription(String Description){
		this.Description = Description;
	}
	
	public String getDescription(){
		return Description;
	}
	
	@XmlElement(name="UpdatedDateUTC")
	public void setUpdatedDateUTC(String UpdatedDateUTC){
		this.UpdatedDateUTC = UpdatedDateUTC;
	}
	
	public String getUpdatedDateUTC(){
		return UpdatedDateUTC;
	}
	
}