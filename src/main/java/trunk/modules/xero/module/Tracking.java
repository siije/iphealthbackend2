package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Tracking")
public class Tracking{
	@XmlElement(name = "TrackingCategory", type = TrackingCategory.class)
	private TrackingCategory trackingCategory;
	
	public void setTrackingCategory (TrackingCategory trackingCategory){
		this.trackingCategory = trackingCategory;
	}
	
	public TrackingCategory getTrackingCategory(){
		return trackingCategory;
	}
}