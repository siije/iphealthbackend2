package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Addresses")
public class Addresses{
	
	@XmlElement(name = "Address", type = Address.class)
	private Address[] addresses;
	
	public Addresses(){}
	
	public Address[] getAddresses(){
		return addresses;
	}
	
	public void setAddresses(Address[] addresses){
		this.addresses = addresses;
	}
}