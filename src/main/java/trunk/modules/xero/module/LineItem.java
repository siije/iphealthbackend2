package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="LineItem")
public class LineItem{
	@XmlTransient
	private String Code;
	@XmlTransient
	private String Description;
	@XmlTransient
	private String UnitAmount;
	@XmlTransient
	private String TaxType;
	@XmlTransient
	private String TaxAmount;
	@XmlTransient
	private String LineAmount;
	@XmlTransient
	private String AccountCode;
	
	@XmlElement(name = "Tracking", type = Tracking.class)
	private Tracking tracking;
	
	@XmlTransient
	private String Quantity;
	
	@XmlElement(name="Code")
	public void setCode(String code){
		this.Code = code;
	}
	
	public String getCode(){
		return Code;
	}
	
	@XmlElement(name="Description")
	public void setDescription(String description){
		this.Description = description;
	}
	
	public String getDescription(){
		return Description;
	}
	
	@XmlElement(name="UnitAmount")
	public void setUnitAmount(String unitAmount){
		this.UnitAmount = unitAmount;
	}
	
	public String getUnitAmount(){
		return UnitAmount;
	}
	
	@XmlElement(name="TaxType")
	public void setTaxType(String taxType){
		this.TaxType = taxType;
	}
	
	public String getTaxType(){
		return TaxType;
	}
	
	@XmlElement(name="TaxAmount")
	public void setTaxAmount(String taxAmount){
		this.TaxAmount = taxAmount;
	}
	
	public String getTaxAmount(){
		return TaxAmount;
	}
	
	@XmlElement(name="LineAmount")
	public void setLineAmount(String lineAmount){
		this.LineAmount = lineAmount;
	}
	
	public String getLineAmount(){
		return LineAmount;
	}
	
	@XmlElement(name="AccountCode")
	public void setAccountCode(String accountCode){
		this.AccountCode = accountCode;
	}
	
	public String getAccountCode(){
		return AccountCode;
	}
	
	public void setTracking (Tracking tracking){
		this.tracking = tracking;
	}
	
	public Tracking getTracking(){
		return tracking;
	}
	
	@XmlElement(name="Quantity")
	public void setQuantity(String quantity){
		this.Quantity = quantity;
	}
	
	public String getQuantity(){
		return Quantity;
	}
}