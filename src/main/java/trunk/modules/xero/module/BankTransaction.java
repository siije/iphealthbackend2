package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="BankTransaction")
public class BankTransaction{
	@XmlElement(name="Contact", type=Contact.class)
	private Contact contact;
	
	@XmlTransient
	private String Date;
	@XmlTransient
	private String Status;
	@XmlTransient
	private String LineAmountTypes;
	
	@XmlElement(name="LineItems", type=LineItems.class)
	private LineItems lineItems;
	
	@XmlTransient
	private String SubTotal;
	@XmlTransient
	private String TotalTax;
	@XmlTransient
	private String Total;
	@XmlTransient
	private String UpdatedDateUTC;
	@XmlTransient
	private String CurrencyCode;
	@XmlTransient
	private String BankTransactionID;
	
	@XmlElement(name="BankAccount", type=Account.class)
	private Account bankAccount;
	
	@XmlTransient
	private String Type;
	@XmlTransient
	private String Reference;
	@XmlTransient
	private String IsReconciled;
	
	@XmlTransient
	private String HasAttachments;

	public Contact getContact(){
		return contact;
	}
	
	public void setContact(Contact contact){
		this.contact = contact;
	}
	
	
	@XmlElement(name="Date")
	public void setDate(String date){
		this.Date = date;
	}
	
	public String getDate(){
		return Date;
	}
	
	@XmlElement(name="Status")
	public void setStatus(String status){
		this.Status = status;
	}
	
	public String getStatus(){
		return Status;
	}
	
	@XmlElement(name="LineAmountTypes")
	public void setLineAmountTypes(String lineAmountTypes){
		this.LineAmountTypes = lineAmountTypes;
	}
	
	public String getLineAmountTypes(){
		return LineAmountTypes;
	}
	
	public LineItems getLineItems(){
		return lineItems;
	}
	
	public void setLineItems(LineItems lineItems){
		this.lineItems = lineItems;
	}
	
	@XmlElement(name="SubTotal")
	public void setSubTotal(String subTotal){
		this.SubTotal = subTotal;
	}
	
	public String getSubTotal(){
		return SubTotal;
	}
	
	@XmlElement(name="TotalTax")
	public void setTotalTax(String totalTax){
		this.TotalTax = totalTax;
	}
	
	public String getTotalTax(){
		return TotalTax;
	}
	
	@XmlElement(name="Total")
	public void setTotal(String total){
		this.Total = total;
	}
	
	public String getTotal(){
		return Total;
	}
	
	@XmlElement(name="UpdatedDateUTC")
	public String getUpdatedDateUTC(){
		return UpdatedDateUTC;
	}
	
	public void setUpdatedDateUTC(String updatedDateUTC){
		this.UpdatedDateUTC = updatedDateUTC;
	}
	
	@XmlElement(name="CurrencyCode")
	public String getCurrencyCode(){
		return CurrencyCode;
	}
	
	public void setCurrencyCode(String currencyCode){
		this.CurrencyCode = currencyCode;
	}
	
	@XmlElement(name="BankTransactionID")
	public String getBankTransactionID(){
		return BankTransactionID;
	};
	
	public void setBankTransactionID(String BankTransactionID){
		this.BankTransactionID = BankTransactionID;
	}
	
	public void setBankAccount(Account bankAccount){
		this.bankAccount = bankAccount;
	}
	
	public Account getBankAccount(){
		return bankAccount;
	}
	
	@XmlElement(name="Type")
	public String getType(){
		return Type;
	}
	
	public void setType(String type){
		this.Type = type;
	}
	
	@XmlElement(name="Reference")
	public String getReference(){
		return Reference;
	};
	
	public void setReference(String Reference){
		this.Reference = Reference;
	}
	
	@XmlElement(name="IsReconciled")
	public String getIsReconciled(){
		return IsReconciled;
	};
	
	public void setIsReconciled(String IsReconciled){
		this.IsReconciled = IsReconciled;
	}
	
	@XmlElement(name="HasAttachments")
	public String getHasAttachments(){
		return HasAttachments;
	}
	
	public void setHasAttachments(String hasAttachments){
		this.HasAttachments = hasAttachments;
	}

}