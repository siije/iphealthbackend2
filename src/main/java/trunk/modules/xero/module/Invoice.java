package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Invoice")
public class Invoice{
	@XmlElement(name="Contact", type=Contact.class)
	private Contact contact;
	
	@XmlTransient
	private String Date;
	@XmlTransient
	private String DueDate;
	@XmlTransient
	private String Status;
	@XmlTransient
	private String LineAmountTypes;
	
	@XmlElement(name="LineItems", type=LineItems.class)
	private LineItems lineItems;
	
	@XmlTransient
	private String SubTotal;
	@XmlTransient
	private String TotalTax;
	@XmlTransient
	private String Total;
	@XmlTransient
	private String UpdatedDateUTC;
	@XmlTransient
	private String CurrencyCode;
	@XmlTransient
	private String FullyPaidOnDate;
	@XmlTransient
	private String Type;
	@XmlTransient
	private String InvoiceID;
	@XmlTransient
	private String InvoiceNumber;
	
	@XmlElement(name="Payments", type=Payments.class)
	private Payments payments;
	
	@XmlElement(name="CreditNotes", type=CreditNotes.class)
	private CreditNotes creditNotes;
	
	@XmlTransient
	private String AmountDue;
	@XmlTransient
	private String AmountPaid;
	@XmlTransient
	private String AmountCredited;
	@XmlTransient
	private String SentToContact;
	@XmlTransient
	private String HasAttachments;
	@XmlTransient
	private String BrandingThemeID;

	public Contact getContact(){
		return contact;
	}
	
	public void setContact(Contact contact){
		this.contact = contact;
	}
	
	@XmlElement(name="Date")
	public void setDate(String date){
		this.Date = date;
	}
	
	public String getDate(){
		return Date;
	}
	
	@XmlElement(name="DueDate")
	public void setDueDate(String dueDate){
		this.DueDate = dueDate;
	}
	
	public String getDueDate(){
		return DueDate;
	}
	
	@XmlElement(name="Status")
	public void setStatus(String status){
		this.Status = status;
	}
	
	public String getStatus(){
		return Status;
	}
	
	@XmlElement(name="LineAmountTypes")
	public void setLineAmountTypes(String lineAmountTypes){
		this.LineAmountTypes = lineAmountTypes;
	}
	
	public String getLineAmountTypes(){
		return LineAmountTypes;
	}
	
	public LineItems getLineItems(){
		return lineItems;
	}
	
	public void setLineItems(LineItems lineItems){
		this.lineItems = lineItems;
	}
	
	@XmlElement(name="SubTotal")
	public void setSubTotal(String subTotal){
		this.SubTotal = subTotal;
	}
	
	public String getSubTotal(){
		return SubTotal;
	}
	
	@XmlElement(name="TotalTax")
	public void setTotalTax(String totalTax){
		this.TotalTax = totalTax;
	}
	
	public String getTotalTax(){
		return TotalTax;
	}
	
	@XmlElement(name="Total")
	public void setTotal(String total){
		this.Total = total;
	}
	
	public String getTotal(){
		return Total;
	}
	
	@XmlElement(name="UpdatedDateUTC")
	public String getUpdatedDateUTC(){
		return UpdatedDateUTC;
	}
	
	public void setUpdatedDateUTC(String updatedDateUTC){
		this.UpdatedDateUTC = updatedDateUTC;
	}
	
	@XmlElement(name="CurrencyCode")
	public String getCurrencyCode(){
		return CurrencyCode;
	}
	
	public void setCurrencyCode(String currencyCode){
		this.CurrencyCode = currencyCode;
	}
	
	@XmlElement(name="FullyPaidOnDate")
	public String getFullyPaidOnDate(){
		return FullyPaidOnDate;
	};
	
	public void setFullyPaidOnDate(String fullyPaidOnDate){
		this.FullyPaidOnDate = fullyPaidOnDate;
	}
	
	@XmlElement(name="Type")
	public String getType(){
		return Type;
	}
	
	public void setType(String type){
		this.Type = type;
	}
	
	@XmlElement(name="InvoiceID")
	public String getInvoiceID(){
		return InvoiceID;
	};
	
	public void setInvoiceID(String invoiceID){
		this.InvoiceID = invoiceID;
	}
	
	@XmlElement(name="InvoiceNumber")
	public String getInvoiceNumber(){
		return InvoiceNumber;
	};
	
	public void setInvoiceNumber(String invoiceNumber){
		this.InvoiceNumber = invoiceNumber;
	}
	
	public Payments getPayments(){
		return payments;
	}
	
	public void setPayments(Payments payments){
		this.payments = payments;
	}
	
	public CreditNotes getCreditNotes(){
		return creditNotes;
	}
	
	public void setCreditNotes(CreditNotes creditNotes){
		this.creditNotes = creditNotes;
	}
	
	@XmlElement(name="AmountDue")
	public String getAmountDue(){
		return AmountDue;
	};
	
	public void setAmountDue(String amountDue){
		this.AmountDue = amountDue;
	}
	
	@XmlElement(name="AmountPaid")
	public String getAmountPaid(){
		return AmountPaid;
	};
	
	public void setAmountPaid(String amountPaid){
		this.AmountPaid = amountPaid;
	}
	
	@XmlElement(name="AmountCredited")
	public String getAmountCredited(){
		return AmountCredited;
	};
	
	public void setAmountCredited(String amountCredited){
		this.AmountCredited = amountCredited;
	}
	
	@XmlElement(name="HasAttachments")
	public String getHasAttachments(){
		return HasAttachments;
	}
	
	public void setHasAttachments(String hasAttachments){
		this.HasAttachments = hasAttachments;
	}
	
	@XmlElement(name="SentToContact")
	public String getSentToContact(){
		return SentToContact;
	}
	
	public void setSentToContact(String sentToContact){
		this.SentToContact = sentToContact;
	}
	
	@XmlElement(name="BrandingThemeID")
	public String getBrandingThemeID(){
		return BrandingThemeID;
	}
	
	public void setBrandingThemeID(String BrandingThemeID){
		this.BrandingThemeID = BrandingThemeID;
	}
}