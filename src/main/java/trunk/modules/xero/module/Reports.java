package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Reports")
public class Reports{
	@XmlElement(name = "Report", type = Report.class)
	private Report[] reports;

	public Reports(){}
	
	public Report[] getReports(){
		return reports;
	}
	
	public void setReports(Report[] reports){
		this.reports = reports;
	}
}