package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "BankTransfers")
public class BankTransfers{
	
	@XmlElement(name = "BankTransfer", type = BankTransfer.class)
	private BankTransfer[] bankTransfers;
	
	public BankTransfers(){}
	
	public BankTransfer[] getBankTransfers(){
		return bankTransfers;
	}
	
	public void setBankTransfers(BankTransfer[] bankTransfers){
		this.bankTransfers = bankTransfers;
	}
}