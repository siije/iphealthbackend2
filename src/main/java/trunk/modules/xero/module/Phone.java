package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Phone")
public class Phone{
	private String PhoneType;
	private String PhoneNumber;
	private String PhoneAreaCode;
	
	@XmlElement(name="PhoneType")
	public void setPhoneType(String phoneType){
		this.PhoneType = phoneType;
	}
	
	public String getPhoneType(){
		return PhoneType;
	}
	
	@XmlElement(name="PhoneNumber")
	public void setPhoneNumber(String phoneNumber){
		this.PhoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber(){
		return PhoneNumber;
	}
	
	@XmlElement(name="PhoneAreaCode")
	public void setAddressLine2(String phoneAreaCode){
		this.PhoneAreaCode = phoneAreaCode;
	}
	
	public String getPhoneAreaCode(){
		return PhoneAreaCode;
	}
	
}