package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Allocation")
public class Allocation{
	@XmlTransient
	private String AppliedAmount;
	@XmlTransient
	private String Date;
	
	@XmlElement(name="AppliedAmount")
	public void setAppliedAmount(String AppliedAmount){
		this.AppliedAmount = AppliedAmount;
	}
	
	public String getAppliedAmount(){
		return AppliedAmount;
	}
	
	@XmlElement(name="Date")
	public void setDate(String Date){
		this.Date = Date;
	}
	
	public String getDate(){
		return Date;
	}
}