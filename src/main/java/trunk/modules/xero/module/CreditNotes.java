package trunk.modules.xero.module;

import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CreditNotes")
public class CreditNotes{
	
	@XmlElement(name = "CreditNote", type = CreditNote.class)
	private CreditNote[] creditNotes;
	
	public CreditNotes(){}
	
	public CreditNote[] getCreditNotes(){
		return creditNotes;
	}
	
	public void setCreditNotes(CreditNote[] creditNotes){
		this.creditNotes = creditNotes;
	}
}