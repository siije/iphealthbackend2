package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="TrackingCategory")
public class TrackingCategory{
	@XmlTransient
	private String Name;
	
	@XmlTransient
	private String Option;
	
	@XmlTransient
	private String TrackingCategoryID;
	
	@XmlElement(name="Name")
	public void setName(String name){
		this.Name = name;
	}
	
	public String getName(){
		return Name;
	}
	
	@XmlElement(name="Option")
	public void setOption(String option){
		this.Option = option;
	}
	
	public String getOption(){
		return Option;
	}
	
	@XmlElement(name="TrackingCategoryID")
	public void setTrackingCategoryID(String trackingCategoryID){
		this.TrackingCategoryID = trackingCategoryID;
	}
	
	public String getTrackingCategoryID(){
		return TrackingCategoryID;
	}
}