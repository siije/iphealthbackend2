package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Contacts")
public class Contacts{
	
	@XmlElement(name = "Contact", type = Contact.class)
	private Contact[] contacts;
	
	public Contacts(){}
	
	public Contact[] getContacts(){
		return contacts;
	}
	
	public void setContacts(Contact[] contacts){
		this.contacts = contacts;
	}
}