package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="SalesDetails")
public class ItemSalesDetail{
	@XmlTransient
	private String UnitPrice;
	@XmlTransient
	private String AccountCode;
	@XmlTransient
	private String TaxType;
	
	@XmlElement(name="UnitPrice")
	public void setUnitPrice(String UnitPrice){
		this.UnitPrice = UnitPrice;
	}
	
	public String getUnitPrice(){
		return UnitPrice;
	}
	
	@XmlElement(name="AccountCode")
	public void setAccountCode(String AccountCode){
		this.AccountCode = AccountCode;
	}
	
	public String getAccountCode(){
		return AccountCode;
	}
	
	@XmlElement(name="TaxType")
	public void setTaxType(String TaxType){
		this.TaxType = TaxType;
	}
	
	public String getTaxType(){
		return TaxType;
	}
	
}