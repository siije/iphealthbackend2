package trunk.modules.xero.module.report;

import javax.xml.bind.annotation.*;
/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Attributes")
public class ReportRowCellAttributes{
	public ReportRowCellAttributes(){}
	
	@XmlElement(name = "Attribute", type = ReportRowCellAttribute.class)
	private ReportRowCellAttribute[] reportRowCellAttributes ;
	
	public ReportRowCellAttribute[] getReportRowCellAttributes(){
		return reportRowCellAttributes;
	}
	
	public void setReportRowCellAttributes(ReportRowCellAttribute[] reportRowCellAttributes){
		this.reportRowCellAttributes = reportRowCellAttributes;
	}
}