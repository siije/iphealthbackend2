package trunk.modules.xero.module.report;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Cell")
public class ReportRowCell{
	public ReportRowCell(){}
	
	@XmlElement(name = "Value")
	private String Value;
	
	public void setValue(String value){
		this.Value = value;
	}
	
	public String getValue(){
		return Value;
	}
	
	@XmlElement(name = "Attributes", type = ReportRowCellAttributes.class)
	private ReportRowCellAttributes reportRowCellAttributes;
	
	public ReportRowCellAttributes getReportRowCellAttributes(){
		return reportRowCellAttributes;
	}
	
}