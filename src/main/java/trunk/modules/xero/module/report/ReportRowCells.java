package trunk.modules.xero.module.report;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Cells")
public class ReportRowCells{
	public ReportRowCells(){}
	
	@XmlElement(name = "Cell", type = ReportRowCell.class)
	private ReportRowCell[] reportRowCells;

	public ReportRowCell[] getReportRowCells(){
		return reportRowCells;
	}
	
	public void setReportRowCells(ReportRowCell[] reportRowCells){
		this.reportRowCells = reportRowCells;
	}
}