package trunk.modules.xero.module.report;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Rows")
public class ReportRows{
	public ReportRows(){}
	
	@XmlElement(name = "Row", type = ReportRow.class)
	private ReportRow[] reportRows;
	
	public ReportRow[] getReportRows(){
		return reportRows;
	}
	
	public void setReportRows(ReportRow[] reportRows){
		this.reportRows = reportRows;
	}

}