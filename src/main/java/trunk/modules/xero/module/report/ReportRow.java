package trunk.modules.xero.module.report;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Row")
public class ReportRow{
	private String RowType;
	
	@XmlElement(name = "Rows", type = ReportRows.class)
	private ReportRows reportRows;
	
	@XmlElement(name = "Cells", type = ReportRowCells.class)
	private ReportRowCells reportRowCells;
	
	public ReportRow(){}
	
	@XmlElement(name = "RowType")
	public void setRowType(String rowType){
		this.RowType = rowType;
	}
	
	public String getRowType(){
		return RowType;
	}
	
	public ReportRows getReportRows(){
		return reportRows;
	}
	
	public void setReportRows(ReportRows reportRows){
		this.reportRows = reportRows;
	}
	
	public ReportRowCells getReportRowCells(){
		return reportRowCells;
	}
	
	public void setReportRowCells(ReportRowCells reportRowCells){
		this.reportRowCells = reportRowCells;
	}

}