package trunk.modules.xero.module.report;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Attribute")
public class ReportRowCellAttribute{
	private String Value;
	private String Id;
	
	@XmlElement(name="Value")
	public void setValue(String value){
		this.Value = value;
	}
	
	public String getValue(){
		return Value;
	}
	
	@XmlElement(name="Id")
	public void setId(String Id){
		this.Id = Id;
	}
	
	public String getId(){
		return Id;
	}
	
}