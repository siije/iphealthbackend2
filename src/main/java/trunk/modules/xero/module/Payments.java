package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Payments")
public class Payments{
	
	@XmlElement(name = "Payment", type = Payment.class)
	private Payment[] payments;
	
	public Payments(){}
	
	public Payment[] getPayments(){
		return payments;
	}
	
	public void setPayments(Payment[] payments){
		this.payments = payments;
	}
}