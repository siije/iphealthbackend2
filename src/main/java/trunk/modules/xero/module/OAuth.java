package trunk.modules.xero.module;

public class OAuth {
	private String consumerKey;
	private String consumerSecret; 
	
	public void setConsumerKey(String consumerKey){
		this.consumerKey = consumerKey;
	}
	public String getConsumerKey(){
		return consumerKey;
	}
	
	public void setConsumerSecret(String consumerSecret){
		this.consumerSecret = consumerSecret;
	}
	public String getConsumerSecret(){
		return consumerSecret;
	}
	
}