package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Allocations")
public class Allocations{
	
	@XmlElement(name = "Allocation", type = Allocation.class)
	private Allocation[] allocations;
	
	public Allocations(){}
	
	public Allocation[] getAllocations(){
		return allocations;
	}
	
	public void setAllocations(Allocation[] allocations){
		this.allocations = allocations;
	}
}