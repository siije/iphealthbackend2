package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="BankTransfer")
public class BankTransfer{
	@XmlTransient
	private String BankTransferID;
	@XmlTransient
	private String CreatedDateUTC;
	@XmlTransient
	private String Date;
	
	@XmlElement(name = "FromBankAccount", type = Account.class)
	private Account fromBankAccount;
	
	@XmlElement(name = "ToBankAccount", type = Account.class)
	private Account toBankAccount;
	
	@XmlTransient
	private String Amount;
	@XmlTransient
	private String FromBankTransactionID;
	@XmlTransient
	private String ToBankTransactionID;
	@XmlTransient
	private String CurrencyRate;
	@XmlTransient
	private String HasAttachments;
	
	@XmlElement(name = "BankTransferID")
	public void setBankTransferID(String BankTransferID){
		this.BankTransferID = BankTransferID;
	}
	public String getBankTransferID(){
		return BankTransferID;
	}
	
	@XmlElement(name = "CreatedDateUTC")
	public void setCreatedDateUTC(String CreatedDateUTC){
		this.CreatedDateUTC = CreatedDateUTC;
	}
	public String getCreatedDateUTC(){
		return CreatedDateUTC;
	}
	
	@XmlElement(name = "Date")
	public void setDate(String Date){
		this.Date = Date;
	}
	public String getDate(){
		return Date;
	}
	
	public Account getFromBankAccount(){
		return fromBankAccount;
	}
	
	public Account getToBankAccount(){
		return toBankAccount;
	}
	
	public void setFromBankAccount(Account account){
		this.fromBankAccount = account;
	}
	
	public void setToBankAccount(Account account){
		this.toBankAccount = account;
	}
	
	@XmlElement(name = "Amount")
	public void setAmount(String Amount){
		this.Amount = Amount;
	}
	public String getAmount(){
		return Amount;
	}
	
	@XmlElement(name = "FromBankTransactionID")
	public void setFromBankTransactionID(String FromBankTransactionID){
		this.FromBankTransactionID = FromBankTransactionID;
	}
	public String getFromBankTransactionID(){
		return FromBankTransactionID;
	}
	@XmlElement(name = "ToBankTransactionID")
	public void setToBankTransactionID(String ToBankTransactionID){
		this.ToBankTransactionID = ToBankTransactionID;
	}
	public String getToBankTransactionID(){
		return ToBankTransactionID;
	}
	
	@XmlElement(name = "CurrencyRate")
	public void setCurrencyRate(String CurrencyRate){
		this.CurrencyRate = CurrencyRate;
	}
	public String getCurrencyRate(){
		return CurrencyRate;
	}
	@XmlElement(name = "HasAttachments")
	public void setHasAttachments(String HasAttachments){
		this.HasAttachments = HasAttachments;
	}
	public String getHasAttachments(){
		return HasAttachments;
	}
}