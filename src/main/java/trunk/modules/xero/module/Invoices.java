package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Invoices")
public class Invoices{
	
	@XmlElement(name = "Invoice", type = Invoice.class)
	private Invoice[] invoices;
	
	public Invoices(){}
	
	public Invoice[] getInvoices(){
		return invoices;
	}
	
	public void setInvoices(Invoice[] invoices){
		this.invoices = invoices;
	}
}