package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Payment")
public class Payment{
	@XmlTransient
	private String PaymentID;
	@XmlTransient
	private String Date;
	@XmlTransient
	private float Amount;
	@XmlTransient
	private String Reference;
	@XmlTransient
	private String CurrencyRate;
	@XmlTransient
	private String PaymentType;
	@XmlTransient
	private String Status;
	@XmlTransient
	private String UpdatedDateUTC;
	
	@XmlElement(name="Account", type=Account.class)
	private Account account;
	
	@XmlElement(name="Invoice", type=Invoice.class)
	private Invoice invoice;
	
	@XmlElement(name="PaymentID")
	public void setPaymentID(String paymentID){
		this.PaymentID = paymentID;
	}
	
	public String getPaymentID(){
		return PaymentID;
	}
	
	@XmlElement(name="Date")
	public void setDate(String date){
		this.Date = date;
	}
	
	public String getDate(){
		return Date;
	}
	
	@XmlElement(name="Amount")
	public void setAmount(float amount){
		this.Amount = amount;
	}
	
	public float getAmount(){
		return Amount;
	}
	
	@XmlElement(name="Reference")
	public void setReference(String reference){
		this.Reference = reference;
	}
	
	public String getReference(){
		return Reference;
	}
	
	public Account getAccount(){
		return account;
	}
	
	public void setAccount(Account account){
		this.account = account;
	}
	
	public Invoice getInvoice(){
		return invoice;
	}
	
	public void setInvoice(Invoice invoice){
		this.invoice =  invoice;
	}
	
	@XmlElement(name="CurrencyRate")
	public void setCurrencyRate(String CurrencyRate){
		this.CurrencyRate = CurrencyRate;
	}
	
	public String getCurrencyRate(){
		return CurrencyRate;
	}
	
	@XmlElement(name="PaymentType")
	public void setPaymentType(String PaymentType){
		this.PaymentType = PaymentType;
	}
	
	public String getPaymentType(){
		return PaymentType;
	}
	
	@XmlElement(name="Status")
	public void setStatus(String Status){
		this.Status = Status;
	}
	
	public String getStatus(){
		return Status;
	}
	
	@XmlElement(name="UpdatedDateUTC")
	public void setUpdatedDateUTC(String UpdatedDateUTC){
		this.UpdatedDateUTC = UpdatedDateUTC;
	}
	
	public String getUpdatedDateUTC(){
		return UpdatedDateUTC;
	}
}