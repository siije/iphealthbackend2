package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Phones")
public class Phones{
	
	@XmlElement(name = "Phone", type = Phone.class)
	private Phone[] phones;
	
	public Phones(){}
	
	public Phone[] getPhone(){
		return phones;
	}
	
	public void setPhone(Phone[] phones){
		this.phones = phones;
	}
}