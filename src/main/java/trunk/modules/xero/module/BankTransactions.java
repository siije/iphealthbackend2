package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "BankTransactions")
public class BankTransactions{
	
	@XmlElement(name = "BankTransaction", type = BankTransaction.class)
	private BankTransaction[] bankTransactions;
	
	public BankTransactions(){}
	
	public BankTransaction[] getBankTransactions(){
		return bankTransactions;
	}
	
	public void setBankTransactions(BankTransaction[] BankTransaction){
		this.bankTransactions = BankTransaction;
	}
}