package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="CreditNote")
public class CreditNote{
	@XmlElement(name="Contact", type=Contact.class)
	private Contact contact;
	
	@XmlTransient
	private String Date;
	@XmlTransient
	private String DueDate;
	@XmlTransient
	private String Status;
	@XmlTransient
	private String LineAmountTypes;
	
	@XmlElement(name="LineItems", type=LineItems.class)
	private LineItems lineItems;
	
	@XmlTransient
	private String SubTotal;
	@XmlTransient
	private String TotalTax;
	@XmlTransient
	private String Total;
	@XmlTransient
	private String UpdatedDateUTC;
	@XmlTransient
	private String CurrencyCode;
	@XmlTransient
	private String FullyPaidOnDate;
	@XmlTransient
	private String Type;
	@XmlTransient
	private String CreditNoteID;
	@XmlTransient
	private String CreditNoteNumber;
	@XmlTransient
	private String RemainingCredit;
	@XmlTransient
	private String AppliedAmount; //for invoice class
	
	@XmlElement(name="Allocations", type=Allocations.class)
	private Allocations allocations;
	
	public void setContact(Contact contact){
		this.contact = contact;
	}
	
	public Contact getContact(){
		return contact;
	}
	
	@XmlElement(name="Date")
	public void setDate(String date){
		this.Date = date;
	}
	
	public String getDate(){
		return Date;
	}
	
	@XmlElement(name="DueDate")
	public void setDueDate(String DueDate){
		this.DueDate = DueDate;
	}
	
	public String getDueDate(){
		return DueDate;
	}
	
	@XmlElement(name="Status")
	public void setStatus(String status){
		this.Status = status;
	}
	
	public String getStatus(){
		return Status;
	}
	
	@XmlElement(name="LineAmountTypes")
	public void setLineAmountTypes(String lineAmountTypes){
		this.LineAmountTypes = lineAmountTypes;
	}
	
	public String getLineAmountTypes(){
		return LineAmountTypes;
	}
	
	public LineItems getLineItems(){
		return lineItems;
	}
	
	public void setLineItems(LineItems lineItems){
		this.lineItems = lineItems;
	}
	
	@XmlElement(name="SubTotal")
	public void setSubTotal(String subTotal){
		this.SubTotal = subTotal;
	}
	
	public String getSubTotal(){
		return SubTotal;
	}
	
	@XmlElement(name="TotalTax")
	public void setTotalTax(String totalTax){
		this.TotalTax = totalTax;
	}
	
	public String getTotalTax(){
		return TotalTax;
	}
	
	@XmlElement(name="Total")
	public void setTotal(String total){
		this.Total = total;
	}
	
	public String getTotal(){
		return Total;
	}
	
	@XmlElement(name="CreditNoteID")
	public void setCreditNoteID(String creditNoteID){
		this.CreditNoteID = creditNoteID;
	}
	
	public String getCreditNoteID(){
		return CreditNoteID;
	}
	
	@XmlElement(name="CreditNoteNumber")
	public void setCreditNoteNumber(String creditNoteNumber){
		this.CreditNoteNumber = creditNoteNumber;
	}
	
	public String getCreditNoteNumber(){
		return CreditNoteNumber;
	}
	
	@XmlElement(name="AppliedAmount")
	public void setAppliedAmount(String appliedAmount){
		this.AppliedAmount = appliedAmount;
	}
	
	public String getAppliedAmount(){
		return AppliedAmount;
	}
	
	@XmlElement(name="UpdatedDateUTC")
	public String getUpdatedDateUTC(){
		return UpdatedDateUTC;
	}
	
	public void setUpdatedDateUTC(String updatedDateUTC){
		this.UpdatedDateUTC = updatedDateUTC;
	}
	
	@XmlElement(name="CurrencyCode")
	public String getCurrencyCode(){
		return CurrencyCode;
	}
	
	public void setCurrencyCode(String currencyCode){
		this.CurrencyCode = currencyCode;
	}
	
	@XmlElement(name="FullyPaidOnDate")
	public String getFullyPaidOnDate(){
		return FullyPaidOnDate;
	};
	
	public void setFullyPaidOnDate(String fullyPaidOnDate){
		this.FullyPaidOnDate = fullyPaidOnDate;
	}
	
	@XmlElement(name="Type")
	public String getType(){
		return Type;
	}
	
	public void setType(String type){
		this.Type = type;
	}
	
	@XmlElement(name="RemainingCredit")
	public String getRemainingCredit(){
		return RemainingCredit;
	}
	
	public void setRemainingCredit(String RemainingCredit){
		this.RemainingCredit = RemainingCredit;
	}
	
	public Allocations getAllocations(){
		return allocations;
	}
	
	public void setAllocations(Allocations allocations){
		this.allocations = allocations;
	}
}