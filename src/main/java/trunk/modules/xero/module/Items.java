package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Items")
public class Items{
	@XmlElement(name = "Item", type = Item.class)
	private Item[] items;
	
	public Items(){}
	
	public Item[] getItem(){
		return items;
	}
	
	public void setItem(Item[] items){
		this.items = items;
	}
}