package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Report")
public class Report{
	private String ReportID;
	private String ReportName;
	private String ReportType;
	
	@XmlElement(name = "ReportTitles", type = ReportTitles.class)
	private ReportTitles reportTitles;
	
	private String ReportDate;
	private String UpdatedDateUTC;
	
	@XmlElement(name = "ReportID")
	public String getReportID(){
		return ReportID;
	}
	
	public void setReportID(String reportID){
		this.ReportID = reportID;
	}
	
	@XmlElement(name = "ReportName")
	public String getReportName(){
		return ReportName;
	}
	
	public void setReportName(String reportName){
		this.ReportName = reportName;
	}
	
	@XmlElement(name = "ReportType")
	public String getReportType(){
		return ReportType;
	}
	
	public void setReportType(String reportType){
		this.ReportType = reportType;
	}

	public ReportTitles getReportTitles(){
		return reportTitles;
	}
	
	@XmlElement(name = "ReportDate")
	public String getReportDate(){
		return ReportDate;
	}
	
	public void setReportDate(String reportDate){
		this.ReportDate = reportDate;
	}
	
	@XmlElement(name = "UpdatedDateUTC")
	public String getUpdatedDateUTC(){
		return UpdatedDateUTC;
	}
	
	public void setUpdatedDateUTC(String updatedDateUTC){
		this.UpdatedDateUTC = updatedDateUTC;
	}
	
}