package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Account")
public class Account{
	@XmlTransient
	private String AccountID;
	@XmlTransient
	private String Code;
	@XmlTransient
	private String Name;
	@XmlTransient
	private String Status;
	@XmlTransient
	private String Type;
	@XmlTransient
	private String TaxType;
	@XmlTransient
	private String Description;
	@XmlTransient
	private String aClass;
	@XmlTransient
	private String EnablePaymentsToAccount;
	@XmlTransient
	private String ShowInExpenseClaims;
	@XmlTransient
	private String BankAccountNumber;
	@XmlTransient
	private String CurrencyCode;
	@XmlTransient
	private String ReportingCode;
	@XmlTransient
	private String ReportingCodeName;
	@XmlTransient
	private String HasAttachments;

	@XmlElement(name="AccountID")
	public void setAccountID(String AccountID){
		this.AccountID = AccountID;
	}
	
	public String getAccountID(){
		return AccountID;
	}
	
	@XmlElement(name="Code")
	public void setCode(String Code){
		this.Code = Code;
	}
	
	public String getCode(){
		return Code;
	}
	
	@XmlElement(name="Name")
	public void setName(String Name){
		this.Name = Name;
	}
	
	public String getName(){
		return Name;
	}
	
	@XmlElement(name="Status")
	public void setStatus(String Status){
		this.Status = Status;
	}
	
	public String getStatus(){
		return Status;
	}
	
	@XmlElement(name="Type")
	public void setType(String Type){
		this.Type = Type;
	}
	
	public String getType(){
		return Type;
	}

	@XmlElement(name="TaxType")
	public void setTaxType(String TaxType){
		this.TaxType = TaxType;
	}
	
	public String getTaxType(){
		return TaxType;
	}
	
	@XmlElement(name="Description")
	public void setDescription(String Description){
		this.Description = Description;
	}
	
	public String getDescription(){
		return Description;
	}
	
	@XmlElement(name="Class")
	public void setAClass(String aClass){
		this.aClass = aClass;
	}
	
	public String getAClass(){
		return aClass;
	}
	
	@XmlElement(name="EnablePaymentsToAccount")
	public void setEnablePaymentsToAccount(String EnablePaymentsToAccount){
		this.EnablePaymentsToAccount = EnablePaymentsToAccount;
	}
	
	public String getEnablePaymentsToAccount(){
		return EnablePaymentsToAccount;
	}
	
	@XmlElement(name="ShowInExpenseClaims")
	public void setShowInExpenseClaims(String ShowInExpenseClaims){
		this.ShowInExpenseClaims = ShowInExpenseClaims;
	}
	
	public String getShowInExpenseClaims(){
		return ShowInExpenseClaims;
	}
	
	@XmlElement(name="BankAccountNumber")
	public void setBankAccountNumber(String BankAccountNumber){
		this.BankAccountNumber = BankAccountNumber;
	}

	public String getBankAccountNumber(){
		return BankAccountNumber;
	}
	@XmlElement(name="CurrencyCode")
	public void setCurrencyCode(String CurrencyCode){
		this.CurrencyCode = CurrencyCode;
	}
	
	public String getCurrencyCode(){
		return CurrencyCode;
	};
	
	@XmlElement(name="ReportingCode")
	public void setReportingCode(String ReportingCode){
		this.ReportingCode = ReportingCode;
	}
	
	public String getReportingCode(){
		return ReportingCode;
	}
	
	@XmlElement(name="ReportingCodeName")
	public void setReportingCodeName(String ReportingCodeName){
		this.ReportingCodeName = ReportingCodeName;
	}
	
	public String getReportingCodeName(){
		return ReportingCodeName;
	}
	
	@XmlElement(name="HasAttachments")
	public void setHasAttachments(String HasAttachments){
		this.HasAttachments = HasAttachments;
	}
	
	public String getHasAttachments(){
		return HasAttachments;
	}
}