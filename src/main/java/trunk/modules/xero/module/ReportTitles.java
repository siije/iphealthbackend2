package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ReportTitles")
public class ReportTitles{
	private String[] ReportTitle;
	
	public ReportTitles(){}
	
	@XmlElement(name = "ReportTitle")
	public void setReportTitle(String[] reportTitle){
		this.ReportTitle = reportTitle;
	}
	
	public String[] getReportTitle(String reportTitle){
		return ReportTitle;
	}
}