package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Accounts")
public class Accounts{
	
	@XmlElement(name = "Account", type = Account.class)
	private Account[] accounts;
	
	public Accounts(){}
	
	public Account[] getAccounts(){
		return accounts;
	}
	
	public void setAccounts(Account[] accounts){
		this.accounts = accounts;
	}
}