package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "LineItems")
public class LineItems{
	
	@XmlElement(name = "LineItem", type = LineItem.class)
	private LineItem[] lineItems;
	
	public LineItems(){}
	
	public LineItem[] getLineItem(){
		return lineItems;
	}
	
	public void setLineItem(LineItem[] lineItems){
		this.lineItems = lineItems;
	}
}