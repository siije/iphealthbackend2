package trunk.modules.xero.module;

import javax.xml.bind.annotation.*;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Address")
public class Address{
	private String AddressType;
	private String AddressLine1;
	private String AddressLine2;
	private String City;
	private String PostalCode;
	
	
	@XmlElement(name="AddressType")
	public void setAddressType(String addressType){
		this.AddressType = addressType;
	}
	
	public String getAddressType(){
		return AddressType;
	}
	
	@XmlElement(name="AddressLine1")
	public void setAddressLine1(String addressLine1){
		this.AddressLine1 = addressLine1;
	}
	
	public String getAddressLine1(){
		return AddressLine1;
	}
	
	@XmlElement(name="AddressLine2")
	public void setAddressLine2(String addressLine2){
		this.AddressLine2 = addressLine2;
	}
	
	public String getAddressLine2(){
		return AddressLine2;
	}
	
	@XmlElement(name="City")
	public void setCity(String city){
		this.City = city;
	}
	
	public String getCity(){
		return City;
	}
	
	@XmlElement(name="PostalCode")
	public void setPostalCode(String postalCode){
		this.PostalCode = postalCode;
	}
	
	public String getPostalCode(){
		return PostalCode;
	}
}