package trunk.modules.xero.beans;

import javax.xml.bind.annotation.*;

import trunk.modules.xero.module.Contacts;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Response")
public class XeroContactsBean{
	@XmlElement(name = "Id")
	private String Id;

	@XmlElement(name = "Status")
	private String Status;
	
	@XmlElement(name = "ProviderName")
	private String ProviderName;
	
	@XmlElement(name = "DateTimeUTC")
	private String DateTimeUTC;
	
	@XmlElement(name = "Contacts", type = Contacts.class)
	private Contacts contacts;
	
	public XeroContactsBean(){}
	
	public Contacts getAccounts(){
		return contacts;
	}
	
}