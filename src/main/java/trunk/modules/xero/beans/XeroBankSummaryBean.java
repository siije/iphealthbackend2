package trunk.modules.xero.beans;

public class XeroBankSummaryBean{
	private String bankAccount;
	private String bankAccountId;
	private float openBalance;
	private float cashReceived;
	private float cashSpent;
	private float closeBalance;
	
	public void setBankAccount(String bankAccount){
		this.bankAccount = bankAccount;
	}
	
	public String getBankAccount(){
		return bankAccount;
	}
	
	public void setBankAccountId(String bankAccountId){
		this.bankAccountId = bankAccountId;
	}
	
	public String getBankAccountId(){
		return bankAccountId;
	}
	
	public void setOpenBalance(float balance){
		this.openBalance = balance;
	}
	
	public float getOpenBalance(){
		return openBalance;
	}
	
	public void setCashReceived(float cash){
		this.cashReceived = cash;
	}
	
	public float getCashReceived(){
		return cashReceived;
	}
	
	public void setCashSpent(float cash){
		this.cashSpent = cash;
	}
	
	public float getCashSpent(){
		return cashSpent;
	}
	
	public void setCloseBalance(float balance){
		this.closeBalance = balance;
	}
	
	public float getCloseBalance(){
		return closeBalance;
	}
}