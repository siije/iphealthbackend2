package trunk.modules.xero.beans;

import java.util.List;

import javax.xml.bind.annotation.*;

import trunk.modules.xero.module.Invoices;
import trunk.modules.xero.module.Invoice;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Response")
public class XeroInvoicesBean{
	@XmlElement(name = "Id")
	private String Id;

	@XmlElement(name = "Status")
	private String Status;
	
	@XmlElement(name = "ProviderName")
	private String ProviderName;
	
	@XmlElement(name = "DateTimeUTC")
	private String DateTimeUTC;
	
	@XmlElement(name = "Invoices", type = Invoices.class)
	private Invoices invoices;
	
	public XeroInvoicesBean(){}
	
	public Invoices getInvoices(){
		return invoices;
	}
	
	public void setInvoices(Invoices invoices){
		this.invoices = invoices;
	}
	
}