package trunk.modules.xero.beans;

import trunk.modules.xero.module.OAuth;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
public class XeroOAuthBean {
	private String consumerKey = "5GWM0XK6DAKNLZJBOEWTZJKTNNVUGI";
	private String consumerSecret = "VP8U1K2IJZOUMEFIE7XU2Q79SGFBWN";
	private String keyFileURL;
	private String keyFileName;
	
	public XeroOAuthBean(){
		String workingDir = System.getProperty("user.dir");
		keyFileURL = workingDir + "/WEB-INF";
		keyFileName = "privatekey.pem";
	}
	
	
	public void setConsumerKey(String consumerKey){
		this.consumerKey = consumerKey;
	}
	public String getConsumerKey(){
		return consumerKey;
	}
	
	public void setConsumerSecret(String consumerSecret){
		this.consumerSecret = consumerSecret;
	}
	public String getConsumerSecret(){
		return consumerSecret;
	}
	
	public void setKeyFileURL(String keyFileURL){
		this.keyFileURL = keyFileURL;
	}
	
	public String getKeyFileURL(){
		return keyFileURL;
	}
	
	public void setKeyFileName(String keyFileName){
		this.keyFileName = keyFileName;
	}
	
	public String getKeyFileName(){
		return keyFileName;
	}
	
	public String getKeyFile(){
		return keyFileURL + '/' + keyFileName;
	}
	
}