package trunk.modules.xero.beans;

public class XeroHttpResultBean{
	private String responseCode;
	private String responseBody;
	
	public void setResponseCode(String code){
		this.responseCode = code;
	}
	
	public String getResponseCode(){
		return responseCode;
	}
	
	public void setResponseBody(String body){
		this.responseBody = body;
	}
	
	public String getResponseBody(){
		return responseBody;
	}
}