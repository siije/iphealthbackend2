package trunk.modules.xero.beans;

import javax.xml.bind.annotation.*;

import trunk.modules.xero.module.Payments;
import trunk.modules.xero.module.Reports;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Response")
public class XeroReportsBean{
	@XmlElement(name = "Id")
	private String Id;

	@XmlElement(name = "Status")
	private String Status;
	
	@XmlElement(name = "ProviderName")
	private String ProviderName;
	
	@XmlElement(name = "DateTimeUTC")
	private String DateTimeUTC;
	
	@XmlElement(name = "Payments", type = Payments.class)
	private Payments payments;
	
	public XeroReportsBean(){}
	
	public Payments getPayments(){
		return payments;
	}
	
}