package trunk.modules.xero.beans;

import javax.xml.bind.annotation.*;

import trunk.modules.xero.module.Items;

/*
 * Gets serialized to json before returned by the web service.
 * 
 * Java bean style getters and setters has to be present
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Response")
public class XeroItemsBean{
	@XmlElement(name = "Id")
	private String Id;

	@XmlElement(name = "Status")
	private String Status;
	
	@XmlElement(name = "ProviderName")
	private String ProviderName;
	
	@XmlElement(name = "DateTimeUTC")
	private String DateTimeUTC;
	
	@XmlElement(name = "Items", type = Items.class)
	private Items items;
	
	public XeroItemsBean(){}
	
	public Items getAccounts(){
		return items;
	}
	
}