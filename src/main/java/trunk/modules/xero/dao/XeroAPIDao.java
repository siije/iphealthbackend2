package trunk.modules.xero.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class XeroAPIDao extends JdbcDaoSupportBase {
	public void saveXeroPrivateAuthKey(int uid, String xero_key, byte[] xero_privateKey){
		invokeStoredProc(ApiUserBean.class, "update_user_xero_auth",
				new StoredProcParam(Types.INTEGER, "pUid", uid),
				new StoredProcParam(Types.VARCHAR, "XeroKey", xero_key),
				new StoredProcParam(Types.BLOB, "xeroPrivateKey", xero_privateKey));
	}
	
	public ApiUserBean getXeroUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_userXero_by_id",
				new StoredProcParam(Types.INTEGER, "p_uid", uid));		
	}
}