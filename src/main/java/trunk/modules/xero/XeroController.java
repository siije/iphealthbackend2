package trunk.modules.xero;

import java.io.Reader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;

import javax.xml.bind.DatatypeConverter;

import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;

import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.scribe.exceptions.OAuthConnectionException;
import org.scribe.model.Response;

import trunk.base.SessionProvider;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.xero.xeroApi.*;
import trunk.modules.xero.beans.*;
import trunk.modules.xero.module.*;
import trunk.modules.xero.dao.XeroAPIDao;

@Controller
public class XeroController extends TrunkModuleBase implements IXeroController {
	private XeroOAuthBean OAuth = new XeroOAuthBean();
	private XeroAPI xeroClient;
	Gson gson = new Gson();

	public <T> T stringToJson(String data, Class<T> clazz) throws IllegalAccessException, InstantiationException{
		Gson gson = new Gson();
		Object object = clazz.newInstance();

		object = gson.fromJson(data, clazz);
		return (T) object;
	}
	
	/*
	 * This method implement nothing in the interface 
	 * It's used to connect to Xero
	 */
	@Override
	public XeroHttpResultBean connXero(HttpServletRequest req){
		XeroAPIDao xeroOAuthDao = ApplicationContextProvider.getContext().getBean(XeroAPIDao.class);
		ApiUserBean xeroUser = xeroOAuthDao.getXeroUserAuthKey(SessionProvider.getUser(req).getId());
		XeroHttpResultBean response = new XeroHttpResultBean();
		
		String key = xeroUser.getXero_key().split("/")[0];
		String secret = xeroUser.getXero_key().split("/")[1];
		
		byte[] privatekey = xeroUser.getXero_privateKey();
		
		if (key.isEmpty() || secret.isEmpty() || privatekey.toString().isEmpty()){
			response.setResponseCode("001");
			response.setResponseBody("Error, can't get correct auth information!");
			return response; 
		}
		
		// Connection builder
		try{
			Reader pemReader = new InputStreamReader(new ByteArrayInputStream(privatekey));
			xeroClient = new XeroAPI(pemReader, key, secret);	
			
			response.setResponseCode("200");
			response.setResponseBody("Connection well!");
			
		}catch(OAuthConnectionException e){
			System.err.println("An OAuthConnectionException was caught!");
			e.printStackTrace();
			
			response.setResponseCode("400");
			response.setResponseBody(e.getMessage());
		}	

		return response;
	}
	
	
	@Override 
	public XeroHttpResultBean connConfig(MultipartFile file,String key, String secret, HttpServletRequest req){
		XeroHttpResultBean result = new XeroHttpResultBean();
		XeroAPIDao connDB = ApplicationContextProvider.getContext().getBean(XeroAPIDao.class);
		
		int maxFileSize = 1024*1024*1;
		
		if (file.isEmpty()){
			result.setResponseBody("Error, It's a empty file!");
			result.setResponseCode("004");
			
			return result ;
		}else if (file.getSize() > maxFileSize){
			result.setResponseBody("Error, this file is too big!");
			result.setResponseCode("005");
			
			return result ;
		}else{
			try{
				int pUid = SessionProvider.getUser(req).getId();
				String XeroKey = key + '/' + secret;
				byte[] xeroPrivateKey = file.getBytes();
				
				connDB.saveXeroPrivateAuthKey(pUid, XeroKey, xeroPrivateKey);
			}catch(IOException e){
				result.setResponseBody(e.getMessage());
				result.setResponseCode("003");
			}
			
			return result;
		}
	}
	
	@Override
	public XeroHttpResultBean postAttachment(MultipartFile file, String endpoint,String id,HttpServletRequest req){
		XeroHttpResultBean result = new XeroHttpResultBean();
	
		int maxFileSize = 1024*1024*21;
		
		if (file.isEmpty()){
			result.setResponseBody("Error, It's a empty file!");
			result.setResponseCode("004");
			
			return result ;
		}else if (file.getSize() > maxFileSize){
			result.setResponseBody("Error, this file is too big!");
			result.setResponseCode("005");
			
			return result ;
		}else{
			System.out.println(file.getOriginalFilename());
			System.out.println(endpoint);
			System.out.println(id);
			
			return result;
		}
	}
	
	@Override
	public XeroHttpResultBean addPayment(String invoiceID, String accountID, float amount, String paidDate, String reference, HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		Payments myPayments = new Payments();
		Payment[] myPayment = new Payment[1];
		
		//Set object myPayment with passed data
		Invoice myInvoice = new Invoice();
		myInvoice.setInvoiceID(invoiceID);
		Account myAccount = new Account();
		myAccount.setAccountID(accountID);

		myPayment[0] = new Payment();
		myPayment[0].setAccount(myAccount);
		myPayment[0].setInvoice(myInvoice);
		myPayment[0].setAmount(amount);
		myPayment[0].setDate(paidDate);
		myPayment[0].setReference(reference);
		
		myPayments.setPayments(myPayment);
		
		//Send request
		Response response = xeroClient.addPaymentToInvoice(myPayments);
		
		//Setting result
		XeroHttpResultBean httpResult = new XeroHttpResultBean();
		httpResult.setResponseBody(response.getBody());
		httpResult.setResponseCode(Integer.toString(response.getCode()));
		
		return httpResult;
	}
	
	@Override
	public XeroPaymentsBean getAllPayments(HttpServletRequest req){
		/*
		 * Checking whether connected to Xero
		 * If not, call connXero();
		 */
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroPaymentsBean payments = xeroClient.getAllPayments(XeroPaymentsBean.class);
		return payments;
	}
	
	@Override
	public XeroPaymentsBean getSinglePayments(String paymentID,HttpServletRequest req){
		/*
		 * Checking whether connected to Xero
		 * If not, call connXero();
		 */
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroPaymentsBean thePayment = xeroClient.getSinglePayments(paymentID, XeroPaymentsBean.class);
		return thePayment;
	}
	
	@Override
	public XeroAccountsBean getAccounts(HttpServletRequest req){
		/*
		 * Checking whether connected to Xero
		 * If not, call connXero();
		 */
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroAccountsBean accounts = xeroClient.getAccounts(XeroAccountsBean.class);
		return accounts;
	}
	
	@Override
	public XeroHttpResultBean addInvoice(String invoice, String contact, String lineItems, HttpServletResponse resp){
		// Convert Json to specific object
		Invoice myInvoice = gson.fromJson(invoice, Invoice.class);
		Contact myContact = gson.fromJson(contact,Contact.class);
		myInvoice.setContact(myContact);

		String[] jsons = !"null".equals(lineItems) ? lineItems.substring(1, lineItems.length()-2).split("},") : null;
		
		if (jsons != null){
			LineItem[] myLineItems = new LineItem[jsons.length];
			int index = 0;

			for(String json : jsons){
				json = json +"}";
				
				myLineItems[index] = gson.fromJson(json, LineItem.class);
				index++;
			}
			
			// Set Data
			LineItems myLineItem = new LineItems();
			myLineItem.setLineItem(myLineItems);
			
			myInvoice.setLineItems(myLineItem);
		}

		//Send request
		Response response = xeroClient.addInvoice(myInvoice);
		
		//Setting http result
		XeroHttpResultBean httpResult = new XeroHttpResultBean();
		httpResult.setResponseBody(response.getBody());
		httpResult.setResponseCode(Integer.toString(response.getCode()));
		
		
		return httpResult;
	}
	
	@Override
	public XeroInvoicesBean getInvoices(HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroInvoicesBean invoices = xeroClient.getInvoices(XeroInvoicesBean.class);
		return invoices;
	}
	
	@Override
	public XeroHttpResultBean updateStatusOfInvoice(String invoiceID, String newStatus, HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		Invoice myInvoice = new Invoice();
		
		myInvoice.setInvoiceID(invoiceID);
		myInvoice.setStatus(newStatus);
		
		//Send request
		Response response = xeroClient.updateStatusOfInvoice(myInvoice);
		
		//Setting http result
		XeroHttpResultBean httpResult = new XeroHttpResultBean();
		httpResult.setResponseBody(response.getBody());
		httpResult.setResponseCode(Integer.toString(response.getCode()));
		
		return httpResult;
	}
	
	@Override
	public XeroHttpResultBean updateStatusOfCreditNote(String creditNoteID, String newStatus, HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		CreditNote myCreditNote = new CreditNote();
		
		myCreditNote.setCreditNoteID(creditNoteID);;
		myCreditNote.setStatus(newStatus);
		
		//Send request
		Response response = xeroClient.updateStatusOfCreditNote(myCreditNote);
		
		//Setting http result
		XeroHttpResultBean httpResult = new XeroHttpResultBean();
		httpResult.setResponseBody(response.getBody());
		httpResult.setResponseCode(Integer.toString(response.getCode()));
		
		return httpResult;
	}
	
	@Override
	public XeroInvoicesBean getSingleInvoice(String invoiceID, HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		//More information than get all invoices (include line items)
		XeroInvoicesBean singleInvoice = xeroClient.getSingleInvoice(invoiceID, XeroInvoicesBean.class);
		return singleInvoice;
	}
	
	@Override
	public XeroBankTransBean getAllBankTrans(HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroBankTransBean allBankTrans = xeroClient.getAllBankTrans(XeroBankTransBean.class);
		return allBankTrans;
	}
	
	@Override
	public XeroHttpResultBean addBankTransaction(String bankTransaction, String contact, String bankAccount,String lineItems){
		// Convert Json to specific object
		BankTransaction myBankTran = gson.fromJson(bankTransaction, BankTransaction.class);
		Contact myContact = gson.fromJson(contact,Contact.class);
		Account myAccount = gson.fromJson(bankAccount, Account.class);
		myBankTran.setContact(myContact);
		myBankTran.setBankAccount(myAccount);

		String[] jsons = !"null".equals(lineItems) ? lineItems.substring(1, lineItems.length()-2).split("},") : null;
		
		if (jsons != null){
			LineItem[] mylineItems = new LineItem[jsons.length];
			int index = 0;

			for(String json : jsons){
				json = json +"}";
				
				mylineItems[index] = gson.fromJson(json, LineItem.class);
				index++;
			}
			
			// Set Data
			LineItems myLineItem = new LineItems();
			myLineItem.setLineItem(mylineItems);
			
			myBankTran.setLineItems(myLineItem);
		}

		//Send request
		Response response = xeroClient.addBankTransaction(myBankTran);
		
		//Setting http result
		XeroHttpResultBean httpResult = new XeroHttpResultBean();
		httpResult.setResponseBody(response.getBody());
		httpResult.setResponseCode(Integer.toString(response.getCode()));
		
		return httpResult;
	}
	
	@Override
	public XeroHttpResultBean addBankTransfer(String bankTransfer, String fromBankAccount,String toBankAccount){
		// Convert Json to specific object
			BankTransfer myBankTransfer = gson.fromJson(bankTransfer, BankTransfer.class);
			Account FromBankAccount = gson.fromJson(fromBankAccount, Account.class);
			Account ToBankAccount = gson.fromJson(toBankAccount, Account.class);
			
			myBankTransfer.setFromBankAccount(FromBankAccount);
			myBankTransfer.setToBankAccount(ToBankAccount);

			//Send request
			Response response = xeroClient.addBankTransfer(myBankTransfer);
			
			//Setting http result
			XeroHttpResultBean httpResult = new XeroHttpResultBean();
			httpResult.setResponseBody(response.getBody());
			httpResult.setResponseCode(Integer.toString(response.getCode()));
			
			return httpResult;
	}
	
	@Override
	public XeroBankTransBean getSingleBankTran(String bankTransID, HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroBankTransBean theBankTran = xeroClient.getSingleBankTrans(bankTransID, XeroBankTransBean.class);
		return theBankTran;
	}
	
	@Override
	public XeroBankTransfersBean getAllTransfers(HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroBankTransfersBean allTransfers = xeroClient.getAllTransfers(XeroBankTransfersBean.class);
		return allTransfers;
	}
	
	@Override
	public XeroBankTransfersBean getSingleTransfer(String transferID, HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroBankTransfersBean theTransfer = xeroClient.getSingleTransfer(transferID, XeroBankTransfersBean.class);
		return theTransfer;
	}
	
	
	@Override
	public XeroReportsBean getBankSummary(HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroReportsBean bankSummary = xeroClient.getBankSummary(XeroReportsBean.class);
		//System.out.println("type" + bankSummary.getReports().getReports()[0].getReportType());
		
		return bankSummary;
	}
	
	@Override
	public XeroContactsBean getAllContacts(HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroContactsBean myContacts = xeroClient.getAllContacts(XeroContactsBean.class);
		return myContacts;
	}
	
	@Override
	public XeroItemsBean getAllItems(HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroItemsBean myItems = xeroClient.getAllItems(XeroItemsBean.class);
		return myItems;
	}
	
	@Override
	public XeroCreditNotesBean getAllCreditNotes(HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroCreditNotesBean myCreditNotes = xeroClient.getAllCreditNotes(XeroCreditNotesBean.class);
		return myCreditNotes;
	}
	
	@Override
	public XeroHttpResultBean addCreditNote(String creditNote,String contact,String lineItems){
		// Convert Json to specific object
		CreditNote myCreditNote = gson.fromJson(creditNote, CreditNote.class);
		Contact myContact = gson.fromJson(contact,Contact.class);
		myCreditNote.setContact(myContact);

		String[] jsons = !"null".equals(lineItems) ? lineItems.substring(1, lineItems.length()-2).split("},") : null;
		
		if (jsons != null){
			LineItem[] mylineItems = new LineItem[jsons.length];
			int index = 0;

			for(String json : jsons){
				json = json +"}";
				
				mylineItems[index] = gson.fromJson(json, LineItem.class);
				index++;
			}
			
			// Set Data
			LineItems myLineItem = new LineItems();
			myLineItem.setLineItem(mylineItems);
			
			myCreditNote.setLineItems(myLineItem);
		}

		//Send request
		Response response = xeroClient.addCreditNote(myCreditNote);
		
		//Setting http result
		XeroHttpResultBean httpResult = new XeroHttpResultBean();
		httpResult.setResponseBody(response.getBody());
		httpResult.setResponseCode(Integer.toString(response.getCode()));
		
		
		return httpResult;
		
	}
	@Override
	public XeroCreditNotesBean getSingleCreditNote(String creditNoteID,HttpServletRequest req){
		if(xeroClient == null){
			XeroHttpResultBean connResult = this.connXero(req);
		}
		
		XeroCreditNotesBean myCreditNotes = xeroClient.getSingleCreditNotes(creditNoteID, XeroCreditNotesBean.class);
		return myCreditNotes;
	}
	
	@Override
	public void initializeUI(ModuleUI ui) {
		ui.addCssFile(new JsFile(ui, "/controllers/xero.css"));
		ui.addJsFile(new JsFile(ui, "/controllers/xero.js"));
		
		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setName("Xero");
		msm.setIcon("fa fa-envelope");
		msm.setId("xeroApi");
		
		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setHref("/myxero");
		ssm.setName("My Xero ");
		ssm.setTemplate("/XeroDashboard.html");
		ssm.setId("g2myXero");
		
		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setHref("/xeroconnect");
		ssm2.setName("Xero Connect");
		ssm2.setTemplate("/XeroDashboard");
		ssm2.setId("g2publicXero");
		
		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
		ui.addSideMenu(msm);
	}
}
