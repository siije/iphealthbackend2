package trunk.modules.xero;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import  org.springframework.web.multipart.MultipartFile;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.xero.beans.*;

@RequestMapping("/xero")
@TrunkModuleFeature(id = "xero", displayName = "My Xero", enabled = true)
public interface IXeroController extends ITrunkModuleBase {
	@TrunkModuleFeature(id = "connXero", displayName = "connXero")
	@RequestMapping(value = "/connXero", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean connXero(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "connConfig", displayName = "connConfig")
	@RequestMapping(value = "/connConfig", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean connConfig(
			@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam(value = "key", required = true) String key,
			@RequestParam(value = "secret", required = true) String secret,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "postAttachment", displayName = "postAttachment")
	@RequestMapping(value = "/postAttachment", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean postAttachment(
			@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam(value = "endpoint", required = true) String endpoint,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getAccounts", displayName = "getAccounts")
	@RequestMapping(value = "/getAccounts", method = RequestMethod.POST)
	@ResponseBody
	XeroAccountsBean getAccounts(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getBankSummary", displayName = "getBankSummary")
	@RequestMapping(value = "/getBankSummary", method = RequestMethod.POST)
	@ResponseBody
	XeroReportsBean getBankSummary(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getSingleBankTran", displayName = "getSingleBankTran")
	@RequestMapping(value = "/getSingleBankTran", method = RequestMethod.POST)
	@ResponseBody
	XeroBankTransBean getSingleBankTran(
			@RequestParam(value = "bankTransID", required = true) String bankTransID,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getAllBankTrans", displayName = "getAllBankTrans")
	@RequestMapping(value = "/getAllBankTrans", method = RequestMethod.POST)
	@ResponseBody
	XeroBankTransBean getAllBankTrans(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "addBankTransaction", displayName = "addBankTransaction")
	@RequestMapping(value = "/addBankTransaction", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean addBankTransaction(
		@RequestParam(value = "bankTransaction", required = true) String bankTransaction,
		@RequestParam(value = "contact", required = true) String contact, 
		@RequestParam(value = "bankAccount", required = true) String bankAccount, 
		@RequestParam(value = "lineItems", required = true) String lineItems);
	
	@TrunkModuleFeature(id = "addBankTransfer", displayName = "addBankTransfer")
	@RequestMapping(value = "/addBankTransfer", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean addBankTransfer(
		@RequestParam(value = "bankTransfer", required = true) String bankTransfer,
		@RequestParam(value = "fromBankAccount", required = true) String fromBankAccount, 
		@RequestParam(value = "toBankAccount", required = true) String toBankAccount);
	
	@TrunkModuleFeature(id = "getSingleTransfer", displayName = "getSingleTransfer")
	@RequestMapping(value = "/getSingleTransfer", method = RequestMethod.POST)
	@ResponseBody
	XeroBankTransfersBean getSingleTransfer(
			@RequestParam(value = "transferID", required = true) String transferID,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getAllTransfers", displayName = "getAllTransfers")
	@RequestMapping(value = "getAllTransfers", method = RequestMethod.POST)
	@ResponseBody
	XeroBankTransfersBean getAllTransfers(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "addInvoice", displayName = "addInvoice")
	@RequestMapping(value = "/addInvoice", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean addInvoice(
			@RequestParam(value = "invoice", required = true) String invoice,
			@RequestParam(value = "contact", required = true) String contact, 
			@RequestParam(value = "lineItems", required = true) String lineItems, 
			@RequestParam(value = "resp", required = true) HttpServletResponse resp);
	
	@TrunkModuleFeature(id = "getInvoices", displayName = "getInvoices")
	@RequestMapping(value = "/getInvoices", method = RequestMethod.POST)
	@ResponseBody
	XeroInvoicesBean getInvoices(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getSingleInvoice", displayName = "getSingleInvoice")
	@RequestMapping(value = "/getSingleInvoice", method = RequestMethod.POST)
	@ResponseBody
	XeroInvoicesBean getSingleInvoice(
			@RequestParam(value = "invoiceID", required = true) String invoiceID,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "addPayment", displayName = "addPayment")
	@RequestMapping(value = "/addPayment", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean addPayment(
			@RequestParam(value = "invoiceID", required = true) String invoiceID,
			@RequestParam(value = "accountID", required = true) String accountID,
			@RequestParam(value = "amount", required = true) float amount,
			@RequestParam(value = "paidDate", required = true) String paidDate,
			@RequestParam(value = "reference", required = true) String reference,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getAllPayments", displayName = "getAllPayments")
	@RequestMapping(value = "/getAllPayments", method = RequestMethod.POST)
	@ResponseBody
	XeroPaymentsBean getAllPayments(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getSinglePayments", displayName = "getSinglePayments")
	@RequestMapping(value = "/getSinglePayments", method = RequestMethod.POST)
	@ResponseBody
	XeroPaymentsBean getSinglePayments(
			@RequestParam(value = "paymentID", required = true) String paymentID,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "updateStatusOfInvoice", displayName = "updateStatusOfInvoice")
	@RequestMapping(value = "/updateStatusOfInvoice", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean updateStatusOfInvoice(
			@RequestParam(value = "invoiceID", required = true) String invoiceID,
			@RequestParam(value = "newStatus", required = true) String newStatus,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "updateStatusOfCreditNote", displayName = "updateStatusOfCreditNote")
	@RequestMapping(value = "/updateStatusOfCreditNote", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean updateStatusOfCreditNote(
			@RequestParam(value = "creditNoteID", required = true) String creditNoteID,
			@RequestParam(value = "newStatus", required = true) String newStatus,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getAllContacts", displayName = "getAllContacts")
	@RequestMapping(value = "/getAllContacts", method = RequestMethod.POST)
	@ResponseBody
	XeroContactsBean getAllContacts(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getAllItems", displayName = "getAllItems")
	@RequestMapping(value = "/getAllItems", method = RequestMethod.POST)
	@ResponseBody
	XeroItemsBean getAllItems(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "getAllCreditNotes", displayName = "getAllCreditNotes")
	@RequestMapping(value = "/getAllCreditNotes", method = RequestMethod.POST)
	@ResponseBody
	XeroCreditNotesBean getAllCreditNotes(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "addCreditNote", displayName = "addCreditNote")
	@RequestMapping(value = "/addCreditNote", method = RequestMethod.POST)
	@ResponseBody
	XeroHttpResultBean addCreditNote(
		@RequestParam(value = "creditNote", required = true) String creditNote,
		@RequestParam(value = "contact", required = true) String contact, 
		@RequestParam(value = "lineItems", required = true) String lineItems);
	
	@TrunkModuleFeature(id = "getSingleCreditNote", displayName = "getSingleCreditNote")
	@RequestMapping(value = "/getSingleCreditNote", method = RequestMethod.POST)
	@ResponseBody
	XeroCreditNotesBean getSingleCreditNote(
		@RequestParam(value = "creditNoteID", required = true) String creditNoteID,
		@RequestParam(value = "req", required = true) HttpServletRequest req);
}
