package trunk.modules.xero.xeroApi;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.services.RSASha1SignatureService;
import org.scribe.services.SignatureService;

import java.io.Reader;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;

/**
 * Xero API for scribe.
 * 
 */
public class XeroOAuthService extends DefaultApi10a {
	
	private static final String AUTHORIZATION_URL = "https://api.xero.com/oauth/Authorize?oauth_token=%s";
	private static RSASha1SignatureService signatureService;
	
	public XeroOAuthService(){} // Blank constructor
	
	public XeroOAuthService(Reader reader) { // constructor for file
		
	    Security.addProvider(new BouncyCastleProvider());
	    
	    try (PEMParser pemParser = new PEMParser(reader)) {
	      PEMKeyPair pair = (PEMKeyPair) pemParser.readObject();
	      byte[] encodedPrivateKey = pair.getPrivateKeyInfo().getEncoded();
	      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	      PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
	      PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
	      signatureService = new RSASha1SignatureService(privateKey);
	      
	    } catch(IOException e) {
	      throw new IllegalStateException(e);
	      
	    } catch(NoSuchAlgorithmException | InvalidKeySpecException e) {
	      throw new IllegalArgumentException(e);
	    }
	    
	}
	/*
	public XeroOAuthService(byte[] obj) {
		
	    Security.addProvider(new BouncyCastleProvider());
	    
	    try (PEMParser pemParser = new PEMParser(reader)) {
	      PEMKeyPair pair = (PEMKeyPair) pemParser.readObject();
	      byte[] encodedPrivateKey = pair.getPrivateKeyInfo().getEncoded();
	      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	      PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
	      PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
	      signatureService = new RSASha1SignatureService(privateKey);
	      
	    } catch(IOException e) {
	      throw new IllegalStateException(e);
	      
	    } catch(NoSuchAlgorithmException | InvalidKeySpecException e) {
	      throw new IllegalArgumentException(e);
	    }
	    
	}
	*/
	@Override
	public String getAccessTokenEndpoint() {
		return "https://api.xero.com/oauth/AccessToken";
	}

	@Override
	public String getRequestTokenEndpoint() {
		return "https://api.xero.com/oauth/RequestToken";
	}

	@Override
	public Verb getAccessTokenVerb() {
		return Verb.GET;
	}

	@Override
	public Verb getRequestTokenVerb() {
		return Verb.GET;
	}

	@Override
	public String getAuthorizationUrl(Token requestToken) {
		return String.format(AUTHORIZATION_URL, requestToken.getToken());
	}
	
	@Override
	  public SignatureService getSignatureService() {
	    return signatureService; 
	  }

}