package trunk.modules.xero.xeroApi;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.Marshaller;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import com.connectifier.xeroclient.models.ApiException; // remove in future
import trunk.modules.xero.module.Payments;
import trunk.modules.xero.module.Invoice;
import trunk.modules.xero.module.CreditNote;
import trunk.modules.xero.module.BankTransaction;
import trunk.modules.xero.module.BankTransfer;

public class XeroAPI {

  protected static final String BASE_URL = "https://api.xero.com/api.xro/2.0/";
  protected static final DateFormat utcFormatter;
  
  static {
    utcFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    utcFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  protected final OAuthService service;
  protected final Token token;

  protected static final Pattern MESSAGE_PATTERN = Pattern.compile("<Message>(.*)</Message>");

  // Xero Private API
  public XeroAPI(Reader pemReader, String consumerKey, String consumerSecret) {
	  service = new ServiceBuilder()
        .provider(new XeroOAuthService(pemReader))
        .apiKey(consumerKey)
        .apiSecret(consumerSecret)
        .build();
	  
	  token = new Token(consumerKey, consumerSecret);
  }
  
  protected XeroApiException newApiException(Response response) {
	  ApiException exception = null;
	  try {
		  exception = unmarshallObject(response, ApiException.class);
	  } catch (Exception e) {}
	  
	  // Jibx doesn't support xsi:type, so we pull out errors this somewhat-hacky way 
	  Matcher matcher = MESSAGE_PATTERN.matcher(response.getBody());
	  StringBuilder messages = new StringBuilder();
	  while (matcher.find()) {
		  if (messages.length() > 0) {
			  messages.append(", ");
		  }
		  messages.append(matcher.group(1));
	  }
    
	  if (exception == null) {
		  if (messages.length() > 0) {
			  return new XeroApiException(response.getCode(), messages.toString());
		  }
		  
		  return new XeroApiException(response.getCode());
	  }
	  
	  return new XeroApiException(response.getCode(), "Error number " + exception.getErrorNumber() + ". " + messages);     
  }
  
  protected <T> T get_object(String endPoint, Class<T> clazz) {
	    return get_object(endPoint, null, null, clazz);
  }
  
  protected <T> T get_object(String endPoint, Date modifiedAfter, Map<String,String> params, Class<T> clazz) {
	    OAuthRequest request = new OAuthRequest(Verb.GET, BASE_URL + endPoint);
	    
	    if (modifiedAfter != null) {
	    	request.addHeader("If-Modified-Since", utcFormatter.format(modifiedAfter));
	    }
	    
	    if (params != null) {
	    	for (Map.Entry<String,String> param : params.entrySet()) {
	    		request.addQuerystringParameter(param.getKey(), param.getValue());
	    	}
	    }
	    
	    service.signRequest(token, request);
	    Response response = request.send();
	    
	    if (response.getCode() != 200) {
	    	throw newApiException(response);
	    }
	    
	    return unmarshallObject(response, clazz);
  }
  
  protected <T> Response put(String endPoint, Object object, Class<T> clazz) {
	    OAuthRequest request = new OAuthRequest(Verb.PUT, BASE_URL + endPoint);
	    String contents = marshallRequest(object, clazz);
	    
	    System.out.println(contents); // only for test
	    
	    request.addPayload(contents);
	    
	    service.signRequest(token, request);
	    Response response = request.send();
	    
	    if (response.getCode() != 200) { throw newApiException(response); }
	    
	    return response;
  }
  
  //For xml test
  protected <T> String put_forTest(String endPoint, Object object, Class<T> clazz) {
	    OAuthRequest request = new OAuthRequest(Verb.PUT, BASE_URL + endPoint);
	    String contents = marshallRequest(object, clazz);
	    
	    System.out.println(contents); // only for test
	    
	    request.addPayload(contents);
	    
	    service.signRequest(token, request);
	    return "ok";
}
  
  protected <T> Response post(String endPoint, Object object, Class<T> clazz) {
	    OAuthRequest request = new OAuthRequest(Verb.POST, BASE_URL + endPoint);
	    
	    String contents = marshallRequest(object, clazz);
	    System.out.println(contents);
	    
	    /*
	     * different part from put method: 
	     * when put: use "request.addPayload(contents)";
	     */
	    request.addBodyParameter("xml", contents);
	    
	    service.signRequest(token, request);
	    Response response = request.send();
	   
	    if (response.getCode() != 200) { throw newApiException(response); }
	    
	    return response;
}
  
  private <T> String marshallRequest(Object object, Class<T> clazz){
	  try {
		  JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
		  Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		  //jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
		  jaxbMarshaller.setProperty( Marshaller.JAXB_FRAGMENT, true );
		  
		  StringWriter w = new StringWriter();
		  jaxbMarshaller.marshal(object, w);
		  
		  return w.toString();
		  
	  } catch (JAXBException e) {
	      throw new IllegalStateException("Error marshalling request object " + object.getClass(), e);
	  }
  }
  
  @SuppressWarnings("unchecked")
  private <T> T unmarshallObject(Response response, Class<T> clazz) {
	  String responseBody = response.getBody();

	  try {
		  JAXBContext jc = JAXBContext.newInstance(clazz);
		  Unmarshaller unmarshaller = jc.createUnmarshaller();
		  return (T) unmarshaller.unmarshal(new StringReader(responseBody));
      
	  } catch (JAXBException e) {
		  throw new IllegalStateException("Error unmarshalling response: " + responseBody, e);
	  }
  }

  protected void addToMapIfNotNull(Map<String,String> map, String key, Object value) {
	  if (value != null) {
		  map.put(key, value.toString());
	  }
  }
  
  protected <T> T singleResult(List<T> list) {
	  if (list.isEmpty()) {
		  return null;
	  }
	  
	  if (list.size() > 1) {
		  throw new IllegalStateException("Got multiple results for query");
	  }
	  return list.get(0);
  }
  
  public <T> T getAccounts(Class<T> clazz) {
	  return get_object("Accounts", clazz);
  }
  
  public <T> T getReport(String ReportName, Class<T> clazz ){
	  return get_object("reports/"+ReportName, clazz);
  }
  
  public <T> T getBankSummary(Class<T> clazz){
	  return getReport("BankSummary", clazz);
  }
  
  public <T> T getAllBankTrans(Class<T> clazz){
	  return get_object("banktransactions", clazz);
  }
  
  public Response addBankTransaction(BankTransaction bankTransaction){
	  return put("banktransactions", bankTransaction ,BankTransaction.class);
  }
  
  public <T> T getSingleBankTrans(String bankTransID, Class<T> clazz){
	  return get_object("banktransactions/" + bankTransID, clazz);
  }
  
  public <T> T getAllTransfers(Class<T> clazz){
	  return get_object("banktransfers", clazz);
  }
  
  public Response addBankTransfer(BankTransfer bankTransfer){
	  return put("banktransfers", bankTransfer ,BankTransfer.class);
  }
  
  
  public <T> T getSingleTransfer(String transferID, Class<T> clazz){
	  return get_object("banktransfers/" + transferID, clazz);
  }
  
  public <T> T getInvoices(Class<T> clazz){
	  return get_object("invoices", clazz);
  }
  
  public <T> T getSingleInvoice(String invoiceId, Class<T> clazz){
	  return get_object("invoices/" + invoiceId, clazz);
  }
  
  public Response addInvoice(Invoice invoices){
	  return put("invoices", invoices ,Invoice.class);
  }
  
  public Response addPaymentToInvoice(Payments payments){
	  return put("payments", payments, Payments.class);
  }
  
  public <T> T getAllPayments(Class<T> clazz){
	  return get_object("payments", clazz);
  }
  
  public <T> T getSinglePayments(String paymentID, Class<T> clazz){
	  return get_object("payments/" + paymentID, clazz);
  }
  
  public Response updateStatusOfInvoice(Invoice invoice){
	  return post("invoices/" + invoice.getInvoiceID(), invoice, Invoice.class);
  }
  
  public <T> T getAllContacts(Class<T> clazz){
	  return get_object("contacts", clazz);
  }
  
  public <T> T getAllItems(Class<T> clazz){
	  return get_object("items", clazz);
  }
  
  public <T> T getAllCreditNotes(Class<T> clazz){
	  return get_object("creditnotes", clazz);
  }
  
  public <T> T getSingleCreditNotes(String creditNoteID, Class<T> clazz){
	  return get_object("creditnotes/" + creditNoteID, clazz);
  }
  
  public Response addCreditNote(CreditNote creditNote){
	  return put("creditnotes", creditNote, CreditNote.class);
  }
  
  public Response updateStatusOfCreditNote(CreditNote creditNote){
	  return post("creditnotes/" + creditNote.getCreditNoteID(), creditNote, CreditNote.class);
  }
}