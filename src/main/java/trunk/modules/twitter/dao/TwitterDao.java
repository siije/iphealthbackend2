package trunk.modules.twitter.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class TwitterDao extends JdbcDaoSupportBase {
	public ApiUserBean getTwitterUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void saveTwitterUserAuthKey(int uid, String twitterKey){
		invokeStoredProc(ApiUserBean.class, "update_user_twitter_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "twitterKey", twitterKey));
	}
}

