package trunk.modules.twitter;

import trunk.modules.api.utils.ApiResultBase;

// 3 step connection
public class TwitterResult extends ApiResultBase {
		
	private TwitterResult twitter;
	
	
	public TwitterResult getTwitter() {
		return twitter;
	}

	public void setTwitter(TwitterResult twitter) {
		this.twitter = twitter;
	}

	public TwitterResult() {
		// Initialize variables for Twitter API
		appName = "iphealth";
		clientId = "iw9n9IHQz3m2pZN86ptZu2xdS";
		clientSecret = "0A3RdqtX016vMD7I8Oa9sYInUhd49HkDRUipTFr94oBSeUj4bT";
		redirectUrl = "http://localhost:8989/redirectTwitter.html";
		baseAuthUrl = "https://api.twitter.com/oauth/authorize";
		baseTokenUrl = "https://api.twitter.com/oauth/access_token";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		authUrl = null;
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {
		tokenParamsUrl = null;
	}
}
