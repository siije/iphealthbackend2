package trunk.modules.twitter;

import java.net.URLDecoder;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.RegEx;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.twitter.dao.TwitterDao;
import trunk.modules.twitter.maps.AccessTokenMap;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

@Controller
public class TwitterController extends TrunkModuleBase implements
		ITwitterController {

	private Twitter getClient(TwitterResult twitterResult,
			HttpServletRequest req) {
		if (req.getSession().getAttribute("twitter") == null) {
			Twitter twitter = TwitterFactory.getSingleton();

			try {
				twitter.setOAuthConsumer(twitterResult.getClientId(),
						twitterResult.getClientSecret());
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}

			req.getSession().setAttribute("twitter", twitter);
			return twitter;
		} else {
			return (Twitter) req.getSession().getAttribute("twitter");
		}
	}

	private String getAuthTokenDB(User u, boolean config) {
		// Get Twitter user authentication key from database
		TwitterDao tDao = ApplicationContextProvider.getContext().getBean(
				TwitterDao.class);
		ApiUserBean twitterUser = tDao.getTwitterUserAuthKey(u.getId());

		String accessTokenJson = twitterUser.getTwitter_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u,
			TwitterResult twitterResult, String verifier, HttpServletRequest req) {

		try {
			RequestToken requestToken = (RequestToken) req.getSession()
					.getAttribute("requestToken");

			// Get authentication token
			AccessToken accessToken = getClient(twitterResult, req)
					.getOAuthAccessToken(requestToken, verifier);

			req.getSession().removeAttribute("requestToken");

			// Save if not error, and return response
			Gson gson = new Gson();
			String accessTokenJson = gson.toJson(accessToken);

			TwitterDao tDao = ApplicationContextProvider.getContext().getBean(
					TwitterDao.class);

			tDao.saveTwitterUserAuthKey(u.getId(), accessTokenJson);
			return accessTokenJson;
		} catch (TwitterException e) {
			e.printStackTrace();
			// Save error for debug
			return "Error: " + e.getMessage();
		}
	}

	@Override
	public TwitterResult configTwitter(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		TwitterResult twitterResult = new TwitterResult();
		String authCode = getAuthTokenDB(u, true);

		twitterResult.setAuthCode(authCode);

		// 3 step flow in this case
		if (authCode == null) {
			try {
				RequestToken requestToken = getClient(twitterResult, req)
						.getOAuthRequestToken(twitterResult.getRedirectUrl());

				// Set new auth url based on 3 step flow
				req.getSession().setAttribute("requestToken", requestToken);
				twitterResult.setAuthUrl(requestToken.getAuthenticationURL());

			} catch (TwitterException e) {
				e.printStackTrace();
				twitterResult.setAuthCode("Error:" + e.getErrorMessage());
			}
		}

		// Authentication code can be null or not, verified in the front end
		return twitterResult;
	}

	@Override
	public TwitterResult redirectTwitter(String token, String verifier,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Token already granted
		TwitterResult twitterResult = new TwitterResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson == null) {
			// Create a new token
			accessTokenJson = createRenewSaveAuthTokenDB(u, twitterResult,
					verifier, req);

			twitterResult.setAccessToken(accessTokenJson);

		} else {
			twitterResult.setAccessToken(accessTokenJson);
		}

		// Access Token can be with or without error, verified in the front end
		return twitterResult;
	}

	@Override
	public List<Status> getTweets(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Token already granted
		TwitterResult twitterResult = new TwitterResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			try {
				Gson gson = new Gson();
				AccessTokenMap accessToken = gson.fromJson(accessTokenJson,
						AccessTokenMap.class);

				Twitter twitter = getClient(twitterResult, req);
				twitter.setOAuthAccessToken(accessToken);

				List<Status> statuses = twitter.getHomeTimeline();
				return statuses;
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public String postTweet(String status, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Token already granted
		TwitterResult twitterResult = new TwitterResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		// Decode status
		status = URLDecoder.decode(status);

		if (accessTokenJson != null) {
			try {
				Gson gson = new Gson();
				AccessTokenMap accessToken = gson.fromJson(accessTokenJson,
						AccessTokenMap.class);

				Twitter twitter = getClient(twitterResult, req);
				twitter.setOAuthAccessToken(accessToken);

				twitter.updateStatus(status);
				return "OK";
			} catch (TwitterException e) {
				e.printStackTrace();
				return "Error: " + e.getMessage();
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}
	
	@Override
	public List<Status> queryTweets(String query, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Token already granted
		TwitterResult twitterResult = new TwitterResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		// Decode query
		query = URLDecoder.decode(query);
		
		Pattern pattern = Pattern.compile("(#[a-zA-Z0-9]+)(, #[a-zA-Z0-9]+,?)*");
		Matcher matcher = pattern.matcher(query);
		
		if(!matcher.matches()){
			return null;
		}
			
		
		StringBuilder processedQuery = new StringBuilder();

		String[] tempQuery = query.split(",");

		int index = 1;
		for (String hashTag : tempQuery) {
			String trimedHashTag = hashTag.trim();

			if (!trimedHashTag.contains("#")) {
				trimedHashTag = "#" + trimedHashTag;
			}

			processedQuery.append(trimedHashTag);

			if (index == tempQuery.length) {
				processedQuery.append(" OR ");
			}

			index++;
		}

		if (accessTokenJson != null) {
			try {
				Gson gson = new Gson();
				AccessTokenMap accessToken = gson.fromJson(accessTokenJson,
						AccessTokenMap.class);

				Twitter twitter = getClient(twitterResult, req);
				twitter.setOAuthAccessToken(accessToken);

				Query queryT = new Query(processedQuery.toString());
				QueryResult result = twitter.search(queryT);
				List<Status> statuses = result.getTweets();
				return statuses;
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/twitter.css"));
		ui.addJsFile(new JsFile(ui, "/twitter.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-twitter");
		msm.setName("Twitter Integration");
		msm.setId("twitterMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/twitter.html");
		ssm.setId("twitterMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Tweets");
		ssm2.setHref("/tweets");
		ssm2.setTemplate("/tweets.html");
		ssm2.setId("twitterMainMultiSub2");

		SubSideMenu ssm3 = new SubSideMenu(ui);
		ssm3.setName("Query Tweets");
		ssm3.setHref("/queryTweets");
		ssm3.setTemplate("/queryTweets.html");
		ssm3.setId("twitterMainMultiSub3");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
		msm.addSubSideMenu(ssm3);
	}
}
