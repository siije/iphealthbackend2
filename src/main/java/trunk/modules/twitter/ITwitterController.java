package trunk.modules.twitter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import twitter4j.Status;

@RequestMapping("/twitter")
@TrunkModuleFeature(id = "twitter", displayName = "Twitter", enabled = true)
public interface ITwitterController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "Twitter Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	TwitterResult configTwitter(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "Twitter Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	TwitterResult redirectTwitter(
			@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "verifier", required = true) String verifier,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "tweets", displayName = "Twitter Tweets")
	@RequestMapping(value = "/tweets", method = RequestMethod.GET)
	@ResponseBody
	List<Status> getTweets(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "tweet", displayName = "Twitter Post Tweet")
	@RequestMapping(value = "/tweet", method = RequestMethod.POST)
	@ResponseBody
	String postTweet(
			@RequestParam(value = "status", required = true) String status,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
		
	@TrunkModuleFeature(id = "tweetsQuery", displayName = "Twitter Query Tweets")
	@RequestMapping(value = "/tweetsQuery", method = RequestMethod.POST)
	@ResponseBody
	List<Status> queryTweets(
			@RequestParam(value = "query", required = true) String query,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
