package trunk.modules.box;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.box.maps.BoxItemMap;

@RequestMapping("/box")
@TrunkModuleFeature(id = "box", displayName = "Box", enabled = true)
public interface IBoxController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "Box Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	BoxResult configBox(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "Box Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	BoxResult redirectBox(
			@RequestParam(value = "authCode", required = true) String code,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "files", displayName = "Box Get Files")
	@RequestMapping(value = "/files", method = RequestMethod.GET)
	@ResponseBody
	List<BoxItemMap> getFilesBox(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "parentFolder", displayName = "Box Get Parent Folder")
	@RequestMapping(value = "/parentFolder", method = RequestMethod.GET)
	@ResponseBody
	BoxItemMap parentFolderBox(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "createFolder", displayName = "Box Create Folder")
	@RequestMapping(value = "/createFolder", method = RequestMethod.POST)
	@ResponseBody
	List<BoxItemMap> createFolderBox(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "renameFolder", displayName = "Box Rename Folder")
	@RequestMapping(value = "/renameFolder", method = RequestMethod.POST)
	@ResponseBody
	List<BoxItemMap> renameFolderBox(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "sharedFolderLink", displayName = "Box Shared Folder Link")
	@RequestMapping(value = "/sharedFolderLink", method = RequestMethod.POST)
	@ResponseBody
	String sharedFolderLinkBox(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "canDownload", required = true) boolean canDownload,
			@RequestParam(value = "canPreview", required = true) boolean canPreview,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "file", displayName = "Box Download File")
	@RequestMapping(value = "/file", method = RequestMethod.GET)
	@ResponseBody
	void downloadFileBox(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);

	@TrunkModuleFeature(id = "file", displayName = "Box Save File")
	@RequestMapping(value = "/file", method = RequestMethod.POST)
	@ResponseBody
	String saveFileBox(
			@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "deleteFileFolder", displayName = "Box Delete File or Folder")
	@RequestMapping(value = "/deleteFileFolder", method = RequestMethod.POST)
	@ResponseBody
	String deleteFileFolderBox(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "isFile", required = true) boolean isFile,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
