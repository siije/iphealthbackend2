package trunk.modules.box.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class BoxDao extends JdbcDaoSupportBase {
	public ApiUserBean getGithubUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void saveBoxUserAuthKey(int uid, String boxKey){
		invokeStoredProc(ApiUserBean.class, "update_user_box_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "boxKey", boxKey));
	}
}

