package trunk.modules.box.maps;

public class BoxItemMap {

	String id;
	String name;
	String parentId;
	String parentName;
	String description;
	String sharedLink;
	boolean is_file;

	String error = "";

	public BoxItemMap() {

	}

	public BoxItemMap(String id, String name, String parentId,
			String parentName, String description, String sharedLink, boolean is_file) {
		this.id = id;
		this.name = name;
		this.parentId = parentId;
		this.parentName = parentName;
		this.description = description;
		this.sharedLink = sharedLink;
		this.is_file = is_file;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public boolean getIs_file() {
		return is_file;
	}

	public void setIs_file(boolean is_file) {
		this.is_file = is_file;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getSharedLink() {
		return sharedLink;
	}

	public void setSharedLink(String sharedLink) {
		this.sharedLink = sharedLink;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
