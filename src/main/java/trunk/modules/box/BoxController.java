package trunk.modules.box;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxSharedLink;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem.Info;
import com.box.sdk.BoxSharedLink.Access;
import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.api.utils.ApiInteraction;
import trunk.modules.box.dao.BoxDao;
import trunk.modules.box.maps.BoxAuthToken;
import trunk.modules.box.maps.BoxItemMap;

@Controller
public class BoxController extends TrunkModuleBase implements IBoxController {

	private BoxAPIConnection getClient(BoxResult result, String accessToken,
			String refreshToken) {
		// OAuth2 token authentication
		BoxAPIConnection client = new BoxAPIConnection(result.getClientId(),
				result.getClientSecret(), accessToken, refreshToken);

		client.setAutoRefresh(false);

		return client;
	}

	private String getAuthTokenDB(User u, boolean config) {
		// Get Box user authentication key from database
		BoxDao bDao = ApplicationContextProvider.getContext().getBean(
				BoxDao.class);
		ApiUserBean boxUser = bDao.getGithubUserAuthKey(u.getId());

		String accessTokenJson = boxUser.getBox_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u, BoxResult boxResult,
			boolean renew, String refreshToken) {

		// Generate parameters url
		boxResult.generateTokenParamsUrl(renew, refreshToken);

		// Get authentication token
		String accessTokenJson = ApiInteraction.getAccessToken(boxResult);

		// Save if not error, and return response
		if (!accessTokenJson.contains("Error")) {
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			BoxDao bDao = ApplicationContextProvider.getContext().getBean(
					BoxDao.class);

			bDao.saveBoxUserAuthKey(u.getId(), accessTokenJson);
			return boxAuthToken.getAccess_token();
		} else {
			// Save error for debug
			return accessTokenJson;
		}
	}

	@Override
	public BoxResult configBox(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		BoxResult boxResult = new BoxResult();
		boxResult.setAuthCode(getAuthTokenDB(u, true));

		// Authentication code can be null or not, verified in the front end
		return boxResult;
	}

	@Override
	public BoxResult redirectBox(String authCode, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		BoxResult boxResult = new BoxResult();
		boxResult.setAuthCode(authCode);

		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson == null) {
			// Create a new token
			boxResult.setAccessToken(createRenewSaveAuthTokenDB(u, boxResult,
					false, null));
		} else {
			// There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			boxResult.setAccessToken(boxAuthToken.getAccess_token());
		}

		// Access Token can be with or without error, verified in the front end
		return boxResult;
	}

	@Override
	public List<BoxItemMap> getFilesBox(String id, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BoxResult boxResult = new BoxResult();
		List<BoxItemMap> files = new LinkedList<BoxItemMap>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			try {
				BoxFolder folder = null;

				if (id.equals("0")) {
					folder = BoxFolder.getRootFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()));
				} else {
					folder = new BoxFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()), id);
				}

				Info currentFolderInfo = folder.getInfo();
				for (Info itemInfo : folder) {
					BoxSharedLink sharedLink = itemInfo.getSharedLink();
					String sharedLinkUrl = null;
					
					if (sharedLink != null)
						sharedLinkUrl = sharedLink.getURL();

					if (itemInfo instanceof BoxFile.Info) {
						files.add(new BoxItemMap(itemInfo.getID(), itemInfo
								.getName(), folder.getID(), currentFolderInfo
								.getName(), itemInfo.getDescription(),
								sharedLinkUrl, true));
					} else if (itemInfo instanceof BoxFolder.Info) {
						files.add(new BoxItemMap(itemInfo.getID(), itemInfo
								.getName(), folder.getID(), currentFolderInfo
								.getName(), itemInfo.getDescription(),
								sharedLinkUrl, false));
					}
				}
			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken(u, boxResult, boxAuthToken);
					return getFilesBox(id, req);
				}

				BoxItemMap errorEntry = new BoxItemMap();
				errorEntry.setError("Error: " + e.getMessage());
				files.add(errorEntry);
			}
		} else {
			BoxItemMap errorEntry = new BoxItemMap();
			errorEntry.setError("NO_AUTH_TOKEN");
			files.add(errorEntry);
		}

		return files;
	}

	@Override
	public BoxItemMap parentFolderBox(String id, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BoxResult boxResult = new BoxResult();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			try {
				BoxFolder folder = null;

				folder = new BoxFolder(getClient(boxResult,
						boxAuthToken.getAccess_token(),
						boxAuthToken.getRefresh_token()), id);

				Info currentFolderInfo = folder.getInfo();
				Info currentParentFolderInfo = currentFolderInfo.getParent();

				return new BoxItemMap(currentParentFolderInfo.getID(),
						currentParentFolderInfo.getName(), "", "", "", null,
						false);
			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken(u, boxResult, boxAuthToken);
					return parentFolderBox(id, req);
				}

				System.out.println("Error: " + e.getMessage());
				return null;
			}
		} else {
			System.out.println("NO_AUTH_TOKEN");
			return null;
		}
	}

	@Override
	public List<BoxItemMap> createFolderBox(String id, String name,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BoxResult boxResult = new BoxResult();
		List<BoxItemMap> files = new LinkedList<BoxItemMap>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			try {
				BoxFolder folder = null;

				if (id.equals("0")) {
					folder = BoxFolder.getRootFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()));
				} else {
					folder = new BoxFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()), id);
				}

				if (folder != null) {
					folder.createFolder(name);
					return getFilesBox(id, req);
				}

			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken(u, boxResult, boxAuthToken);
					return createFolderBox(id, name, req);
				}

				BoxItemMap errorEntry = new BoxItemMap();
				errorEntry.setError("Error: " + e.getMessage());
				files.add(errorEntry);
			}
		} else {
			BoxItemMap errorEntry = new BoxItemMap();
			errorEntry.setError("NO_AUTH_TOKEN");
			files.add(errorEntry);
		}

		return files;
	}

	@Override
	public List<BoxItemMap> renameFolderBox(String id, String name,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BoxResult boxResult = new BoxResult();
		List<BoxItemMap> files = new LinkedList<BoxItemMap>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			try {
				BoxFolder folder = null;

				if (id.equals("0")) {
					return files;
				} else {
					folder = new BoxFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()), id);
				}

				if (folder != null) {
					Info currentFolderInfo = folder.getInfo();
					Info currentParentFolderInfo = currentFolderInfo
							.getParent();

					folder.rename(name);
					return getFilesBox(currentParentFolderInfo.getID(), req);
				}

			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken(u, boxResult, boxAuthToken);
					return renameFolderBox(id, name, req);
				}

				BoxItemMap errorEntry = new BoxItemMap();
				errorEntry.setError("Error: " + e.getMessage());
				files.add(errorEntry);
			}
		} else {
			BoxItemMap errorEntry = new BoxItemMap();
			errorEntry.setError("NO_AUTH_TOKEN");
			files.add(errorEntry);
		}

		return files;
	}

	@Override
	public String sharedFolderLinkBox(String id, boolean canDownload,
			boolean canPreview, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		BoxResult boxResult = new BoxResult();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			try {
				BoxFolder folder = null;

				if (id.equals("0")) {
					folder = BoxFolder.getRootFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()));
				} else {
					folder = new BoxFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()), id);
				}

				if (folder != null) {
					BoxSharedLink.Permissions permissions = new BoxSharedLink.Permissions();
					permissions.setCanDownload(canDownload);
					permissions.setCanDownload(canPreview);

					folder.createSharedLink(Access.OPEN, null, permissions);
					return "OK";
				}

			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken(u, boxResult, boxAuthToken);
					return sharedFolderLinkBox(id, canDownload, canPreview, req);
				}

				return "Error: " + e.getMessage();
			}
		}

		return "NO_AUTH_TOKEN";
	}

	@Override
	public void downloadFileBox(String id, HttpServletRequest req,
			HttpServletResponse res) {
		User u = SessionProvider.getUser(req);

		// Decode path url
		BoxResult boxResult = new BoxResult();

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			try {
				BoxFile file = new BoxFile(getClient(boxResult,
						boxAuthToken.getAccess_token(),
						boxAuthToken.getRefresh_token()), id);

				file.download(res.getOutputStream());
				res.setHeader("ContentType", "application/octet-stream");
				res.flushBuffer();
			} catch (IOException e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken(u, boxResult, boxAuthToken);
					downloadFileBox(id, req, res);
				}

				e.printStackTrace();
				System.out.println("Error: " + e.getMessage());
			}
		}
	}

	@Override
	public String saveFileBox(MultipartFile file, String id,
			HttpServletRequest req) {

		User u = SessionProvider.getUser(req);

		BoxResult boxResult = new BoxResult();

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) { // There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			try {

				String fileName = file.getOriginalFilename();

				BoxFolder folder = null;
				if (id.equals("0")) {
					folder = BoxFolder.getRootFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()));
				} else {
					folder = new BoxFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()), id);
				}

				folder.uploadFile(file.getInputStream(), fileName);

				return "OK";
			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken(u, boxResult, boxAuthToken);
					return saveFileBox(file, id, req);
				}

				return "Error: " + e.getMessage();
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	@Override
	public String deleteFileFolderBox(String id, boolean isFile,
			HttpServletRequest req) {

		User u = SessionProvider.getUser(req);

		BoxResult boxResult = new BoxResult();

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) { // There is a token already
			Gson gson = new Gson();
			BoxAuthToken boxAuthToken = gson.fromJson(accessTokenJson,
					BoxAuthToken.class);

			try {
				if (isFile) {
					BoxFile file = new BoxFile(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()), id);
					file.delete();
				} else {
					BoxFolder folder = new BoxFolder(getClient(boxResult,
							boxAuthToken.getAccess_token(),
							boxAuthToken.getRefresh_token()), id);
					folder.delete(true);
				}

				return "OK";
			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken(u, boxResult, boxAuthToken);
					return deleteFileFolderBox(id, isFile, req);
				}

				return "Error: " + e.getMessage() + id;
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/box.css"));
		ui.addJsFile(new JsFile(ui, "/box.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-envelope");
		msm.setName("Box Integration");
		msm.setId("boxMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/box.html");
		ssm.setId("boxMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Files");
		ssm2.setHref("/files");
		ssm2.setTemplate("/files.html");
		ssm2.setId("boxMainMultiSub2");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
	}

	public void RenewAccessToken(User u, BoxResult boxResult,
			BoxAuthToken boxAuthToken) {
		BoxAPIConnection boxClient = getClient(boxResult,
				boxAuthToken.getAccess_token(), boxAuthToken.getRefresh_token());

		boxClient.refresh();
		boxAuthToken.setAccess_token(boxClient.getAccessToken());
		boxAuthToken.setExpires_in(boxClient.getExpires());
		boxAuthToken.setRefresh_token(boxClient.getRefreshToken());

		BoxDao bDao = ApplicationContextProvider.getContext().getBean(
				BoxDao.class);

		Gson gson = new Gson();
		String accessTokenJson = gson.toJson(boxAuthToken);
		bDao.saveBoxUserAuthKey(u.getId(), accessTokenJson);
	}
}
