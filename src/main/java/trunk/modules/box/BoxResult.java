package trunk.modules.box;

import trunk.modules.api.utils.ApiResultBase;

public class BoxResult extends ApiResultBase {
	
	private String repositoryNotificationsUrl = "https://api.github.com/repos"; 
	
	public BoxResult() {
		// Initialize variables for Box API
		appName = "iphealth";
		clientId = "39k1rnox3366rs5pfaxf4orh1me9a04d";
		clientSecret = "hlV6HjwRuOo50KFDe37r6T9yAjT9GPjZ";
		redirectUrl = "http://localhost:8989/redirectBox.html";
		baseAuthUrl = "https://app.box.com/api/oauth2/authorize";
		baseTokenUrl = "https://app.box.com/api/oauth2/token";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		StringBuilder str = new StringBuilder();
		str.append(baseAuthUrl);
		str.append("?response_type=code");
		//str.append("&app_name=");
		//str.append(appName);
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);

		authUrl = str.toString();
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {
		StringBuilder str = new StringBuilder();
		
		if(renew){
			str.append("grant_type=refresh_token");
			str.append("&refresh_token=");
			str.append(refreshToken);
		}
		else
		{
			str.append("grant_type=authorization_code");
			str.append("&code=");
			str.append(authCode);
		}
		
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);
		str.append("&client_secret=");
		str.append(clientSecret);

		tokenParamsUrl = str.toString();
	}

	public String getRepositoryNotificationsUrl() {
		return repositoryNotificationsUrl;
	}

	public void setRepositoryNotificationsUrl(String repositoryNotificationsUrl) {
		this.repositoryNotificationsUrl = repositoryNotificationsUrl;
	}
}
