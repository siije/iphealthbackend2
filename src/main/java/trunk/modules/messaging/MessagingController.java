package trunk.modules.messaging;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.SingleSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.messaging.beans.MessageBean;
import trunk.modules.messaging.dao.MessageDao;

@Controller
public class MessagingController extends TrunkModuleBase implements IMessagingController {

	@Override
	public MessageBean sendMessage(String message, HttpServletRequest req) {

		/*
		 * The logged in user that invoked this request.
		 * 
		 * Integer branchId = u.getBranch_id();
		 * 
		 * Integer uid = u.getId();
		 * 
		 * Branch id is the unique identifier of the branch that the user
		 * belongs to. Use this id to separate per branch
		 */
		User u = SessionProvider.getUser(req);

		/*
		 * Gets the instance of the MessageDao.java from the spring container
		 */
		MessageDao mDao = ApplicationContextProvider.getContext().getBean(MessageDao.class);

		/*
		 * Invokes database request using the data access object and returns the
		 * result
		 * 
		 * NOTE: The java list will get serialized to a json object
		 * 
		 * u.getId() is the unique user id (integer)
		 */
		return mDao.sendMessage(message, u.getId());
	}

	@Override
	public MessageBean getSingleMessage(String id, HttpServletRequest req) {
		List<MessageBean> all = listMessages(req);

		MessageBean mb = new MessageBean();
		mb.setId(0);
		mb.setMessage("");

		for (MessageBean messageBean : all) {
			if (messageBean.getId() == Integer.parseInt(id)) {
				mb = messageBean;
			}
		}

		return mb;
	}

	@Override
	public List<MessageBean> listMessages(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);
		MessageDao mDao = ApplicationContextProvider.getContext().getBean(MessageDao.class);

		return mDao.listMessages(u.getId());
	}

	@Override
	public List<MessageBean> deleteMessages(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);
		MessageDao mDao = ApplicationContextProvider.getContext().getBean(MessageDao.class);
		
		return mDao.deleteMessages(u.getId());
		//return "all messages deleted";
	}

	@Override
	public String updateMessage(String id) {
		return "restart";
	}

	@Override
	public void initializeUI(ModuleUI ui) {
		ui.addJsFile(new JsFile(ui, "/messaging.js"));

		/*SingleSideMenu msm = new SingleSideMenu(ui);
		msm.setName("Messaging");
		msm.setIcon("fa fa-envelope");
		msm.setId("messaginSideMenuId");
		msm.setHref("/messaging");
		msm.setTemplate("/messaging.html");

		ui.addSideMenu(msm);*/
	}
}
