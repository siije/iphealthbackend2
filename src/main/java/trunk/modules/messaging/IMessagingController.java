package trunk.modules.messaging;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.annotations.TrunkWorkflowFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.messaging.beans.MessageBean;

@RequestMapping("/messaging")
@TrunkModuleFeature(id = "messaging", displayName = "Messaging", enabled = true)
public interface IMessagingController extends ITrunkModuleBase {

	@TrunkWorkflowFeature(
			view = "/workflow/viewSingleMessage.html",
			jsFiles = {
					"/workflow/viewSingleMessage.js",
			},
			settings = "/workflow/settings.html",
			settingsJsFiles = { "/workflow/settings.js"
			},
			actionable = false)
	@TrunkModuleFeature(id = "getSingleMessage", displayName = "Show a message")
	@RequestMapping(value = "/getSingleMessage", method = RequestMethod.POST)
	@ResponseBody
	MessageBean getSingleMessage(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "send", displayName = "Send a message")
	@RequestMapping(value = "/send", method = RequestMethod.POST)
	@ResponseBody
	MessageBean sendMessage(
			@RequestParam(value = "message", required = true) String message,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "list", displayName = "List all messages")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	List<MessageBean> listMessages(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "delete", displayName = "Delete all messages", enabled = true)
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	List<MessageBean> deleteMessages(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "update", displayName = "Update a message")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	String updateMessage(
			@RequestParam(value = "id", required = true) String id);
}
