package trunk.modules.dropbox;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWriteMode;
import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.api.utils.ApiInteraction;
import trunk.modules.dropbox.dao.DropboxDao;
import trunk.modules.dropbox.maps.AppengineHttpRequestor;
import trunk.modules.dropbox.maps.DbxEntryMap;
import trunk.modules.dropbox.maps.DropboxAuthToken;

@Controller
public class DropboxController extends TrunkModuleBase implements
		IDropboxController {

	private DbxClient getClient(String accessToken) {
		DbxRequestConfig config = new DbxRequestConfig("iphealth", Locale
				.getDefault().toString(), AppengineHttpRequestor.Instance);

		DbxClient client = new DbxClient(config, accessToken);
		return client;
	}

	private String getAuthTokenDB(User u, boolean config) {
		// Get Dropbox user authentication key from database
		DropboxDao dDao = ApplicationContextProvider.getContext().getBean(
				DropboxDao.class);
		ApiUserBean dropboxUser = dDao.getDropBoxUserAuthKey(u.getId());

		String accessTokenJson = dropboxUser.getDropbox_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u,
			DropboxResult dropboxResult, boolean renew, String refreshToken) {

		// Generate parameters url
		dropboxResult.generateTokenParamsUrl(renew, refreshToken);

		// Get authentication token
		String accessTokenJson = ApiInteraction.getAccessToken(dropboxResult);

		// Save if not error, and return response
		if (!accessTokenJson.contains("Error")) {
			Gson gson = new Gson();
			DropboxAuthToken dropboxAuthToken = gson.fromJson(accessTokenJson,
					DropboxAuthToken.class);

			DropboxDao aDao = ApplicationContextProvider.getContext().getBean(
					DropboxDao.class);

			aDao.saveDropboxUserAuthKey(u.getId(), accessTokenJson);
			return dropboxAuthToken.getAccess_token();
		} else {
			// Save error for debug
			return accessTokenJson;
		}
	}

	@Override
	public DropboxResult configDropbox(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		DropboxResult dropboxResult = new DropboxResult();
		dropboxResult.setAuthCode(getAuthTokenDB(u, true));

		// Authentication code can be null or not, verified in the front end
		return dropboxResult;
	}

	@Override
	public DropboxResult redirectDropbox(String authCode, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		DropboxResult dropboxResult = new DropboxResult();
		dropboxResult.setAuthCode(authCode);

		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson == null) {
			// Create a new token
			dropboxResult.setAccessToken(createRenewSaveAuthTokenDB(u,
					dropboxResult, false, null));
		} else {
			// There is a token already
			Gson gson = new Gson();
			DropboxAuthToken dropboxAuthToken = gson.fromJson(accessTokenJson,
					DropboxAuthToken.class);

			dropboxResult.setAccessToken(dropboxAuthToken.getAccess_token());
		}

		// Access Token can be with or without error, verified in the front end
		return dropboxResult;
	}

	@Override
	public List<DbxEntryMap> getFilesDropbox(String path, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Decode path url
		path = URLDecoder.decode(path);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<DbxEntryMap> files = new LinkedList<DbxEntryMap>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			DropboxAuthToken dropboxAuthToken = gson.fromJson(accessTokenJson,
					DropboxAuthToken.class);

			DbxEntry.WithChildren listing;
			try {
				listing = getClient(dropboxAuthToken.getAccess_token())
						.getMetadataWithChildren(path);

				if (listing != null) {
					for (DbxEntry entry : listing.children) {
						files.add(new DbxEntryMap(entry.name, entry.path, entry
								.isFile()));
					}
				}
			} catch (DbxException e) {
				DbxEntryMap errorEntry = new DbxEntryMap();
				errorEntry.setError("Error:" + e.getMessage());
				files.add(errorEntry);
			}
		} else {
			DbxEntryMap errorEntry = new DbxEntryMap();
			errorEntry.setError("NO_AUTH_TOKEN");
			files.add(errorEntry);
		}

		return files;
	}

	@Override
	public List<DbxEntryMap> createFolderDropbox(String path, String name,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Decode path url
		path = URLDecoder.decode(path);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<DbxEntryMap> files = new LinkedList<DbxEntryMap>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			DropboxAuthToken dropboxAuthToken = gson.fromJson(accessTokenJson,
					DropboxAuthToken.class);

			try {
				String folderPath = path + "/" + name;
				getClient(dropboxAuthToken.getAccess_token()).createFolder(folderPath);
		
				return getFilesDropbox(path, req);
			} catch (DbxException e) {
				DbxEntryMap errorEntry = new DbxEntryMap();
				errorEntry.setError("Error:" + e.getMessage());
				files.add(errorEntry);
			}
		} else {
			DbxEntryMap errorEntry = new DbxEntryMap();
			errorEntry.setError("NO_AUTH_TOKEN");
			files.add(errorEntry);
		}

		return files;
	}

	@Override
	public void downloadFileDropbox(String path, HttpServletRequest req,
			HttpServletResponse res) {
		User u = SessionProvider.getUser(req);

		// Decode path url
		path = URLDecoder.decode(path);

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			DropboxAuthToken dropboxAuthToken = gson.fromJson(accessTokenJson,
					DropboxAuthToken.class);

			try {
				getClient(dropboxAuthToken.getAccess_token()).getFile(path,
						null, res.getOutputStream());

				res.setHeader("ContentType", "application/octet-stream");
				res.flushBuffer();
			} catch (DbxException | IOException e) {
				e.printStackTrace();
				System.out.println("Error: " + e.getMessage());
			}
		}
	}

	@Override
	public String saveFileDropbox(MultipartFile file, String path,
			HttpServletRequest req) {

		User u = SessionProvider.getUser(req);

		// Decode path url
		path = URLDecoder.decode(path);

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) { // There is a token already
			Gson gson = new Gson();
			DropboxAuthToken dropboxAuthToken = gson.fromJson(accessTokenJson,
					DropboxAuthToken.class);

			try {

				String fileName = file.getOriginalFilename();

				long length = file.getSize();
				String destinationPath = path + "/" + fileName;
				getClient(dropboxAuthToken.getAccess_token()).uploadFile(
						destinationPath, DbxWriteMode.add(), length,
						file.getInputStream());

				return "OK";
			} catch (Exception e) {
				return "Error: " + e.getMessage();
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	@Override
	public String deleteFileDropbox(String path, HttpServletRequest req) {

		User u = SessionProvider.getUser(req);

		// Decode path url
		path = URLDecoder.decode(path);

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) { // There is a token already
			Gson gson = new Gson();
			DropboxAuthToken dropboxAuthToken = gson.fromJson(accessTokenJson,
					DropboxAuthToken.class);

			try {
				getClient(dropboxAuthToken.getAccess_token()).delete(path);
				return "OK";
			} catch (Exception e) {
				return "Error: " + e.getMessage() + path;
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/dropbox.css"));
		ui.addJsFile(new JsFile(ui, "/dropbox.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-dropbox");
		msm.setName("Dropbox Integration");
		msm.setId("dropboxMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/dropbox.html");
		ssm.setId("dropboxMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Files");
		ssm2.setHref("/files");
		ssm2.setTemplate("/files.html");
		ssm2.setId("dropboxMainMultiSub2");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
	}
}
