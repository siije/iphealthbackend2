package trunk.modules.dropbox;

import trunk.modules.api.utils.ApiResultBase;

public class DropboxResult extends ApiResultBase {
	
	public DropboxResult() {
		// Initialize variables for Dropbox API
		appName = "iphealth";
		clientId = "x3d4kvgf9xe43hq";
		clientSecret = "9y14n4qvrq9ppi3";
		redirectUrl = "http://localhost:8989/redirectDropbox.html";
		baseAuthUrl = "https://www.dropbox.com/1/oauth2/authorize";
		baseTokenUrl = "https://api.dropbox.com/1/oauth2/token";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		StringBuilder str = new StringBuilder();
		str.append(baseAuthUrl);
		str.append("?response_type=code");
		//str.append("&app_name=");
		//str.append(appName);
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);

		authUrl = str.toString();
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {
		StringBuilder str = new StringBuilder();
		
		if(renew)		{
			str.append("grant_type=refresh_token");
			str.append("&refresh_token=");
			str.append(refreshToken);
		}
		else
		{
			str.append("grant_type=authorization_code");
			str.append("&code=");
			str.append(authCode);
		}
		
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);
		str.append("&client_secret=");
		str.append(clientSecret);

		tokenParamsUrl = str.toString();
	}
}
