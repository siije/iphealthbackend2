package trunk.modules.dropbox.maps;

public class DbxEntryMap {

	String name;
	String path;
	boolean is_file;
	String error = "";
	
	public DbxEntryMap()
	{
		
	}

	public DbxEntryMap(String name, String path, boolean is_file) {	
		this.name = name;
		this.path = path;
		this.is_file = is_file;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean getIs_file() {
		return is_file;
	}

	public void setIs_file(boolean is_file) {
		this.is_file = is_file;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}	
}
