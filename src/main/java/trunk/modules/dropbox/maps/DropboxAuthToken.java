package trunk.modules.dropbox.maps;

// Representation of the Dropbox authentication token
public class DropboxAuthToken {
	
	String access_token;
	String token_type;
	String uid;
	
	public DropboxAuthToken(){
		
	}
	
	public String getAccess_token() {
		return access_token;
	}
	
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	
	public String getToken_type() {
		return token_type;
	}
	
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
