package trunk.modules.dropbox.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class DropboxDao extends JdbcDaoSupportBase {
	public ApiUserBean getDropBoxUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void saveDropboxUserAuthKey(int uid, String dropboxKey){
		invokeStoredProc(ApiUserBean.class, "update_user_dropbox_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "dropboxKey", dropboxKey));
	}
}

