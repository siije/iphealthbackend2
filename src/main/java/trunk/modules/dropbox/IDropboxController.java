package trunk.modules.dropbox;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.dropbox.maps.DbxEntryMap;

@RequestMapping("/dropbox")
@TrunkModuleFeature(id = "dropbox", displayName = "Dropbox", enabled = true)
public interface IDropboxController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "Dropbox Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	DropboxResult configDropbox(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "Dropbox Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	DropboxResult redirectDropbox(
			@RequestParam(value = "authCode", required = true) String code,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "files", displayName = "Dropbox Get Files")
	@RequestMapping(value = "/files", method = RequestMethod.GET)
	@ResponseBody
	List<DbxEntryMap> getFilesDropbox(
			@RequestParam(value = "path", required = true) String path,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "createFolder", displayName = "Dropbox Create Folder")
	@RequestMapping(value = "/createFolder", method = RequestMethod.POST)
	@ResponseBody
	List<DbxEntryMap> createFolderDropbox(
			@RequestParam(value = "path", required = true) String path,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "file", displayName = "Dropbox Download File")
	@RequestMapping(value = "/file", method = RequestMethod.GET)
	@ResponseBody
	void downloadFileDropbox(
			@RequestParam(value = "path", required = true) String path,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);

	@TrunkModuleFeature(id = "file", displayName = "Dropbox Save File")
	@RequestMapping(value = "/file", method = RequestMethod.POST)
	@ResponseBody
	String saveFileDropbox(
			@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam(value = "path", required = true) String path,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "deleteFileFolder", displayName = "Dropbox Delete File Folder")
	@RequestMapping(value = "/deleteFileFolder", method = RequestMethod.POST)
	@ResponseBody
	String deleteFileDropbox(
			@RequestParam(value = "path", required = true) String path,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
