package trunk.modules.slack;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.slack.maps.SlackChannelHistory;
import trunk.modules.slack.maps.SlackChannels;
import trunk.modules.slack.maps.SlackFiles;
import trunk.modules.slack.maps.SlackFilesData;
import trunk.modules.slack.maps.SlackFilesQuery;
import trunk.modules.slack.maps.SlackMembers;
import trunk.modules.slack.maps.SlackMessagesQuery;
import twitter4j.Status;

@RequestMapping("/slack")
@TrunkModuleFeature(id = "slack", displayName = "Slack", enabled = true)
public interface ISlackController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "Slack Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	SlackResult configSlack(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "Slack Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	SlackResult redirectSlack(
			@RequestParam(value = "authCode", required = true) String authCode,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "channels", displayName = "Slack Channels")
	@RequestMapping(value = "/channels", method = RequestMethod.GET)
	@ResponseBody
	SlackChannels channelsSlack(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "channel", displayName = "Slack Channel")
	@RequestMapping(value = "/channel", method = RequestMethod.GET)
	@ResponseBody
	SlackChannelHistory channelSlackHistory(
			@RequestParam(value = "guid", required = true) String guid,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "channel", displayName = "Slack Post Comment to Channel")
	@RequestMapping(value = "/channel", method = RequestMethod.POST)
	@ResponseBody
	String postCommentToChannel(
			@RequestParam(value = "guid", required = true) String guid,
			@RequestParam(value = "comment", required = true) String comment,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "teamMembers", displayName = "Slack Team members")
	@RequestMapping(value = "/teamMembers", method = RequestMethod.GET)
	@ResponseBody
	SlackMembers teamMembersSlack(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "files", displayName = "Slack Files")
	@RequestMapping(value = "/files", method = RequestMethod.GET)
	@ResponseBody
	SlackFiles filesSlack(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "file", displayName = "Slack Save File")
	@RequestMapping(value = "/file", method = RequestMethod.POST)
	@ResponseBody
	String saveFileSlack(
			@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam(value = "guid", required = true) String guid,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "messagesQuery", displayName = "Slack Messages Query")
	@RequestMapping(value = "/messagesQuery", method = RequestMethod.GET)
	@ResponseBody
	SlackMessagesQuery queryMessages(
			@RequestParam(value = "query", required = true) String query,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "filesQuery", displayName = "Slack Files Query")
	@RequestMapping(value = "/filesQuery", method = RequestMethod.GET)
	@ResponseBody
	SlackFilesQuery queryFiles(
			@RequestParam(value = "query", required = true) String query,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
