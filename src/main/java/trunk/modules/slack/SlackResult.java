package trunk.modules.slack;

import trunk.modules.api.utils.ApiResultBase;

public class SlackResult extends ApiResultBase {
	
	private String uniqueId = "iphealth001100";
	private String channelsUrl = "https://slack.com/api/channels.list";
	private String channelHistoryUrl = "https://slack.com/api/channels.history";
	private String teamMembersUrl = "https://slack.com/api/users.list";
	private String userUrl = "https://slack.com/api/users.info";
	private String filesUrl = "https://slack.com/api/files.list";
	private String searchMessagesUrl = "https://slack.com/api/search.messages";
	private String searchFilesUrl = "https://slack.com/api/search.files";
	private String postCommentUrl = "https://slack.com/api/chat.postMessage";
	private String uploadFileUrl = "https://slack.com/api/files.upload";
	private String identifyUrl = "https://slack.com/api/auth.test";

	public SlackResult() {
		// Initialize variables for Slack API
		appName = "iphealthslack";
		clientId = "3097355031.3334106420";
		clientSecret = "714182fe4f0ef83aa64dc6f331e48926";
		redirectUrl = "http://localhost:8989/redirectSlack.html";
		baseAuthUrl = "https://slack.com/oauth/authorize";
		baseTokenUrl = "https://slack.com/api/oauth.access";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		StringBuilder str = new StringBuilder();
		str.append(baseAuthUrl);
		str.append("?client_id=");
		str.append(clientId);
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&scope=read,post,identify");	
		str.append("&state=");
		str.append(uniqueId);
		
		authUrl = str.toString();
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {
		StringBuilder str = new StringBuilder();			
	
		str.append("client_id=");
		str.append(clientId);
		str.append("&client_secret=");
		str.append(clientSecret);
		str.append("&code=");
		str.append(authCode);
		str.append("&redirect_uri=");
		str.append(redirectUrl);

		tokenParamsUrl = str.toString();
	}
	
	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getChannelsUrl() {
		return channelsUrl;
	}

	public void setChannelsUrl(String channelsUrl) {
		this.channelsUrl = channelsUrl;
	}
	
	public String getChannelHistoryUrl() {
		return channelHistoryUrl;
	}

	public void setChannelHistoryUrl(String channelHistoryUrl) {
		this.channelHistoryUrl = channelHistoryUrl;
	}

	public String getTeamMembersUrl() {
		return teamMembersUrl;
	}

	public void setTeamMembersUrl(String teamMembersUrl) {
		this.teamMembersUrl = teamMembersUrl;
	}
	
	public String getUserUrl() {
		return userUrl;
	}

	public void setUserUrl(String userUrl) {
		this.userUrl = userUrl;
	}

	public String getFilesUrl() {
		return filesUrl;
	}

	public void setFilesUrl(String filesUrl) {
		this.filesUrl = filesUrl;
	}

	public String getSearchMessagesUrl() {
		return searchMessagesUrl;
	}

	public void setSearchMessagesUrl(String searchMessagesUrl) {
		this.searchMessagesUrl = searchMessagesUrl;
	}

	public String getSearchFilesUrl() {
		return searchFilesUrl;
	}

	public void setSearchFilesUrl(String searchFilesUrl) {
		this.searchFilesUrl = searchFilesUrl;
	}

	public String getPostCommentUrl() {
		return postCommentUrl;
	}

	public void setPostCommentUrl(String postCommentUrl) {
		this.postCommentUrl = postCommentUrl;
	}

	public String getUploadFileUrl() {
		return uploadFileUrl;
	}

	public void setUploadFileUrl(String uploadFileUrl) {
		this.uploadFileUrl = uploadFileUrl;
	}

	public String getIdentifyUrl() {
		return identifyUrl;
	}

	public void setIdentifyUrl(String identifyUrl) {
		this.identifyUrl = identifyUrl;
	}
}
