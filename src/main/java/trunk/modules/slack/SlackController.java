package trunk.modules.slack;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import com.dropbox.core.DbxWriteMode;
import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.api.utils.ApiInteraction;
import trunk.modules.dropbox.maps.DropboxAuthToken;
import trunk.modules.slack.dao.SlackDao;
import trunk.modules.slack.maps.SlackAuthToken;
import trunk.modules.slack.maps.SlackChannelHistory;
import trunk.modules.slack.maps.SlackChannelHistoryData;
import trunk.modules.slack.maps.SlackChannels;
import trunk.modules.slack.maps.SlackFiles;
import trunk.modules.slack.maps.SlackFilesData;
import trunk.modules.slack.maps.SlackFilesQuery;
import trunk.modules.slack.maps.SlackIdentify;
import trunk.modules.slack.maps.SlackMembers;
import trunk.modules.slack.maps.SlackMembersData;
import trunk.modules.slack.maps.SlackMessagesQuery;
import trunk.modules.slack.maps.SlackMessagesQueryMatch;
import trunk.modules.slack.maps.SlackUser;
import trunk.modules.twitter.TwitterResult;
import trunk.modules.twitter.maps.AccessTokenMap;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

@Controller
public class SlackController extends TrunkModuleBase implements
		ISlackController {

	private String getAuthTokenDB(User u, boolean config) {
		// Get Slack user authentication key from database
		SlackDao sDao = ApplicationContextProvider.getContext().getBean(
				SlackDao.class);
		ApiUserBean slackUser = sDao.getSlackUserAuthKey(u.getId());

		String accessTokenJson = slackUser.getSlack_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u, SlackResult slackResult,
			boolean renew, String refreshToken) {
		// Generate parameters url
		slackResult.generateTokenParamsUrl(renew, refreshToken);

		// Get authentication token
		String accessTokenJson = ApiInteraction.getAccessToken(slackResult);

		// Save if not error, and return response
		if (!accessTokenJson.contains("Error")) {
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			SlackDao sDao = ApplicationContextProvider.getContext().getBean(
					SlackDao.class);

			sDao.saveSlackUserAuthKey(u.getId(), accessTokenJson);
			return slackAuthToken.getAccess_token();
		} else {
			// Save error for debug
			return accessTokenJson;
		}
	}

	@Override
	public SlackResult configSlack(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		SlackResult slackResult = new SlackResult();
		slackResult.setAuthCode(getAuthTokenDB(u, true));

		// Authentication code can be null or not, verified in the front end
		return slackResult;
	}

	@Override
	public SlackResult redirectSlack(String authCode, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		SlackResult slackResult = new SlackResult();
		slackResult.setAuthCode(authCode);

		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson == null) {
			// Create a new token
			slackResult.setAccessToken(createRenewSaveAuthTokenDB(u,
					slackResult, false, null));
		} else {
			// There is a token already
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			slackResult.setAccessToken(slackAuthToken.getAccess_token());
		}

		// Access Token can be with or without error, verified in the front end
		return slackResult;
	}

	@Override
	public SlackChannels channelsSlack(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		SlackChannels channels = new SlackChannels();
		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get channels
			String channelsJson = ApiInteraction.HTTPRequest(
					slackResult.getChannelsUrl(), "GET", null, null, null,
					"?token=" + slackAuthToken.getAccess_token(), null, false, null);

			// Verify if token is not expired
			if (channelsJson.contains("false")
					&& channelsJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get channels again
					channelsJson = ApiInteraction.HTTPRequest(
							slackResult.getChannelsUrl(), "GET", null, null,
							null, "?token=" + slackAuthToken.getAccess_token(),
							null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					channels.setOk(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (channels.getOk() != null) {
				return channels;
			}

			// If there is no error, get channels
			if (!channelsJson.contains("Error")) {
				channels = gson.fromJson(channelsJson, SlackChannels.class);

				channels.setOk(null);
			} else {
				channels.setOk(channelsJson + " CHANNELS");
			}
		} else {
			channels.setOk("NO_AUTH_TOKEN");
		}

		// Channels can be with or without error, verified in the front end
		return channels;
	}

	@Override
	public SlackChannelHistory channelSlackHistory(String guid,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		SlackChannelHistory channelHistory = new SlackChannelHistory();
		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get channel history
			String channelHistoryJson = ApiInteraction.HTTPRequest(
					slackResult.getChannelHistoryUrl(), "GET", null, null,
					null, "?token=" + slackAuthToken.getAccess_token()
							+ "&channel=" + guid, null, false, null);

			// Verify if token is not expired
			if (channelHistoryJson.contains("false")
					&& channelHistoryJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get channel history again
					channelHistoryJson = ApiInteraction.HTTPRequest(
							slackResult.getChannelHistoryUrl(), "GET", null,
							null, null,
							"?token=" + slackAuthToken.getAccess_token()
									+ "&channel=" + guid, null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					channelHistory.setOk(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (channelHistory.getOk() != null) {
				return channelHistory;
			}

			// If there is no error, get channel history
			if (!channelHistoryJson.contains("Error")) {
				channelHistory = gson.fromJson(channelHistoryJson,
						SlackChannelHistory.class);

				// Retrieve all members of the team, so we can get the
				// information
				SlackMembers teamMembers = teamMembersSlack(req);

				HashMap<String, SlackUser> teamMembersDict = new LinkedHashMap<String, SlackUser>();

				for (SlackMembersData memberData : teamMembers.getMembers()) {
					SlackUser newUser = new SlackUser();

					newUser.setOk(null);
					newUser.setUser(memberData);

					teamMembersDict.put(memberData.getId(), newUser);
				}

				// Get user info for each message
				for (SlackChannelHistoryData message : channelHistory
						.getMessages()) {
					message.setUserInfo(teamMembersDict.get(message.getUser()));
				}

				channelHistory.setOk(null);
			} else {
				channelHistory.setOk(channelHistoryJson + " CHANNEL HISTORY");
			}
		} else {
			channelHistory.setOk("NO_AUTH_TOKEN");
		}

		// Channel History can be with or without error, verified in the front
		// end
		return channelHistory;
	}

	@Override
	public String postCommentToChannel(String guid, String comment,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		// Encode comment
		comment = URLEncoder.encode(comment);

		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get identify user
			SlackIdentify userIdentify = identifyUserSlack(req);

			// Post comment
			String postCommentToChannelJson = ApiInteraction.HTTPRequest(
					slackResult.getPostCommentUrl(), "GET", null, null, null,
					"?token=" + slackAuthToken.getAccess_token() + "&channel="
							+ guid + "&text=" + comment + "&username="
							+ userIdentify.getUser(), null, false, null);

			// Verify if token is not expired
			if (postCommentToChannelJson.contains("false")
					&& postCommentToChannelJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Post comment again
					postCommentToChannelJson = ApiInteraction.HTTPRequest(
							slackResult.getPostCommentUrl(), "GET", null, null,
							null, "?token=" + slackAuthToken.getAccess_token()
									+ "&channel=" + guid + "&text=" + comment,
							null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					return accessTokenJson + " RENEW TOKEN";
				}
			}

			// If there is no error return ok
			if (!postCommentToChannelJson.contains("Error")) {
				return "OK";
			} else {
				return postCommentToChannelJson + " CHANNEL HISTORY";
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	// @Override
	public SlackIdentify identifyUserSlack(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		SlackIdentify user = new SlackIdentify();
		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get user identify slack
			String userIdentifyJson = ApiInteraction.HTTPRequest(
					slackResult.getIdentifyUrl(), "GET", null, null, null,
					"?token=" + slackAuthToken.getAccess_token(), null, false, null);

			// Verify if token is not expired
			if (userIdentifyJson.contains("false")
					&& userIdentifyJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get user identify slack again
					userIdentifyJson = ApiInteraction.HTTPRequest(
							slackResult.getIdentifyUrl(), "GET", null, null,
							null, "?token=" + slackAuthToken.getAccess_token(),
							null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					user.setOk(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (user.getOk() != null) {
				return user;
			}

			// If there is no error, get user identify
			if (!userIdentifyJson.contains("Error")) {
				user = gson.fromJson(userIdentifyJson, SlackIdentify.class);

				user.setOk(null);
			} else {
				user.setOk(userIdentifyJson + " USER");
			}
		} else {
			user.setOk("NO_AUTH_TOKEN");
		}

		// User can be with or without error, verified in the front end
		return user;
	}

	// @Override
	public SlackUser getUserSlack(String guid, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		SlackUser user = new SlackUser();
		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get user slack
			String userJson = ApiInteraction.HTTPRequest(
					slackResult.getUserUrl(), "GET", null, null, null,
					"?token=" + slackAuthToken.getAccess_token() + "&user="
							+ guid, null, false, null);

			// Verify if token is not expired
			if (userJson.contains("false")
					&& userJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get user slack again
					userJson = ApiInteraction.HTTPRequest(
							slackResult.getUserUrl(), "GET", null, null, null,
							"?token=" + slackAuthToken.getAccess_token()
									+ "&user=" + guid, null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					user.setOk(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (user.getOk() != null) {
				return user;
			}

			// If there is no error, get user slack
			if (!userJson.contains("Error")) {
				user = gson.fromJson(userJson, SlackUser.class);

				user.setOk(null);
			} else {
				user.setOk(userJson + " USER");
			}
		} else {
			user.setOk("NO_AUTH_TOKEN");
		}

		// User can be with or without error, verified in the front end
		return user;
	}

	@Override
	public SlackMembers teamMembersSlack(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		SlackMembers members = new SlackMembers();
		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get team members
			String teamMembersJson = ApiInteraction.HTTPRequest(
					slackResult.getTeamMembersUrl(), "GET", null, null, null,
					"?token=" + slackAuthToken.getAccess_token(), null, false, null);

			// Verify if token is not expired
			if (teamMembersJson.contains("false")
					&& teamMembersJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get team members again
					teamMembersJson = ApiInteraction.HTTPRequest(
							slackResult.getTeamMembersUrl(), "GET", null, null,
							null, "?token=" + slackAuthToken.getAccess_token(),
							null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					members.setOk(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (members.getOk() != null) {
				return members;
			}

			// If there is no error, get members
			if (!teamMembersJson.contains("Error")) {
				members = gson.fromJson(teamMembersJson, SlackMembers.class);

				members.setOk(null);
			} else {
				members.setOk(teamMembersJson + " TEAM MEMBERS");
			}
		} else {
			members.setOk("NO_AUTH_TOKEN");
		}

		// Members can be with or without error, verified in the front end
		return members;
	}

	@Override
	public SlackFiles filesSlack(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		SlackFiles files = new SlackFiles();
		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get files
			String filesJson = ApiInteraction.HTTPRequest(
					slackResult.getFilesUrl(), "GET", null, null, null,
					"?token=" + slackAuthToken.getAccess_token(), null, false, null);

			// Verify if token is not expired
			if (filesJson.contains("false")
					&& filesJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get files again
					filesJson = ApiInteraction.HTTPRequest(
							slackResult.getFilesUrl(), "GET", null, null, null,
							"?token=" + slackAuthToken.getAccess_token(), null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					files.setOk(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (files.getOk() != null) {
				return files;
			}

			// If there is no error, get files
			if (!filesJson.contains("Error")) {
				files = gson.fromJson(filesJson, SlackFiles.class);

				files.setOk(null);
			} else {
				files.setOk(filesJson + " FILES");
			}
		} else {
			files.setOk("NO_AUTH_TOKEN");
		}

		// Files can be with or without error, verified in the front end
		return files;
	}

	@Override
	public String saveFileSlack(MultipartFile file, String guid,
			HttpServletRequest req) {

		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			try {

				String fileName = file.getOriginalFilename();

				// Post file
				String postFileToChannelJson = ApiInteraction.HTTPRequestPostFile(
						slackResult.getUploadFileUrl(), file, null, null,
						"?token=" + slackAuthToken.getAccess_token() + 
						"&channels=" + guid +
						"&filename=" + fileName, "content");

				// Verify if token is not expired
				if (postFileToChannelJson.contains("false")
						&& postFileToChannelJson.contains("Not Authorized")) {
					// Renew token
					accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
							false, null);

					// If no error
					if (!accessTokenJson.contains("Error")) {
						// Post file again
						postFileToChannelJson = ApiInteraction.HTTPRequestPostFile(
								slackResult.getUploadFileUrl(), file, null, null,
								"?token=" + slackAuthToken.getAccess_token() + 
								"&channels=" + guid +
								"&filename=" + fileName, "file");

						// Save new authentication token in class for further use
						slackAuthToken.setAccess_token(accessTokenJson);
					} else {
						// Save error for debug
						return accessTokenJson + " RENEW TOKEN";
					}
				}

				// If there is no error return ok
				if (!postFileToChannelJson.contains("Error")) {
					return "OK";
				} else {
					return postFileToChannelJson + " SAVE FILE";
				}
			} catch (Exception e) {
				return "Error: " + e.getMessage();
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	@Override
	public SlackMessagesQuery queryMessages(String query, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		// Decode query
		query = URLEncoder.encode(query);

		SlackMessagesQuery messagesQuery = new SlackMessagesQuery();
		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get query messages
			String messagesQueryJson = ApiInteraction.HTTPRequest(
					slackResult.getSearchMessagesUrl(), "GET", null, null,
					null, "?token=" + slackAuthToken.getAccess_token()
							+ "&query=" + query, null, false, null);

			// Verify if token is not expired
			if (messagesQueryJson.contains("false")
					&& messagesQueryJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get query messages again
					messagesQueryJson = ApiInteraction.HTTPRequest(
							slackResult.getSearchMessagesUrl(), "GET", null,
							null, null,
							"?token=" + slackAuthToken.getAccess_token()
									+ "&query=" + query, null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					messagesQuery.setOk(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (messagesQuery.getOk() != null) {
				return messagesQuery;
			}

			// If there is no error, get query messages
			if (!messagesQueryJson.contains("Error")) {
				messagesQuery = gson.fromJson(messagesQueryJson,
						SlackMessagesQuery.class);

				// Retrieve all members of the team, so we can get the
				// information
				SlackMembers teamMembers = teamMembersSlack(req);

				HashMap<String, SlackUser> teamMembersDict = new LinkedHashMap<String, SlackUser>();

				for (SlackMembersData memberData : teamMembers.getMembers()) {
					SlackUser newUser = new SlackUser();

					newUser.setOk(null);
					newUser.setUser(memberData);

					teamMembersDict.put(memberData.getId(), newUser);
				}

				// Get user info for each message
				for (SlackMessagesQueryMatch message : messagesQuery
						.getMessages().getMatches()) {
					message.setUserInfo(teamMembersDict.get(message.getUser()));
				}

				messagesQuery.setOk(null);
			} else {
				messagesQuery.setOk(messagesQueryJson + " MESSAGES QUERY");
			}
		} else {
			messagesQuery.setOk("NO_AUTH_TOKEN");
		}

		// Messages query can be with or without error, verified in the front
		// end
		return messagesQuery;
	}

	@Override
	public SlackFilesQuery queryFiles(String query, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Slack user authentication key from database
		SlackResult slackResult = new SlackResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		// Decode query
		query = URLEncoder.encode(query);

		SlackFilesQuery filesQuery = new SlackFilesQuery();
		if (accessTokenJson != null) {
			// Deserialize class into SlackAuthToken object
			Gson gson = new Gson();
			SlackAuthToken slackAuthToken = gson.fromJson(accessTokenJson,
					SlackAuthToken.class);

			// Get query files
			String filesQueryJson = ApiInteraction.HTTPRequest(
					slackResult.getSearchFilesUrl(), "GET", null, null, null,
					"?token=" + slackAuthToken.getAccess_token() + "&query="
							+ query, null, false, null);

			// Verify if token is not expired
			if (filesQueryJson.contains("false")
					&& filesQueryJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, slackResult,
						false, null);

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get query files again
					filesQueryJson = ApiInteraction.HTTPRequest(
							slackResult.getSearchFilesUrl(), "GET", null, null,
							null, "?token=" + slackAuthToken.getAccess_token()
									+ "&query=" + query, null, false, null);

					// Save new authentication token in class for further use
					slackAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					filesQuery.setOk(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (filesQuery.getOk() != null) {
				return filesQuery;
			}

			// If there is no error, get query files
			if (!filesQueryJson.contains("Error")) {
				filesQuery = gson.fromJson(filesQueryJson,
						SlackFilesQuery.class);

				filesQuery.setOk(null);
			} else {
				filesQuery.setOk(filesQueryJson + " MESSAGES QUERY");
			}
		} else {
			filesQuery.setOk("NO_AUTH_TOKEN");
		}

		// Files query can be with or without error, verified in the front end
		return filesQuery;
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/slack.css"));
		ui.addJsFile(new JsFile(ui, "/slack.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-envelope");
		msm.setName("Slack Integration");
		msm.setId("slackMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/slack.html");
		ssm.setId("slackMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Channels");
		ssm2.setHref("/channels");
		ssm2.setTemplate("/channels.html");
		ssm2.setId("slackMainMultiSub2");

		SubSideMenu ssm3 = new SubSideMenu(ui);
		ssm3.setName("Team Members");
		ssm3.setHref("/teamMembers");
		ssm3.setTemplate("/teamMembers.html");
		ssm3.setId("slackMainMultiSub3");

		SubSideMenu ssm4 = new SubSideMenu(ui);
		ssm4.setName("Files");
		ssm4.setHref("/files");
		ssm4.setTemplate("/files.html");
		ssm4.setId("slackMainMultiSub4");

		SubSideMenu ssm5 = new SubSideMenu(ui);
		ssm5.setName("Query Messages");
		ssm5.setHref("/queryMessages");
		ssm5.setTemplate("/queryMessages.html");
		ssm5.setId("slackMainMultiSub5");

		SubSideMenu ssm6 = new SubSideMenu(ui);
		ssm6.setName("Query Files");
		ssm6.setHref("/queryFiles");
		ssm6.setTemplate("/queryFiles.html");
		ssm6.setId("slackMainMultiSub6");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
		msm.addSubSideMenu(ssm3);
		msm.addSubSideMenu(ssm4);
		msm.addSubSideMenu(ssm5);
		msm.addSubSideMenu(ssm6);
	}
}
