package trunk.modules.slack.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class SlackDao extends JdbcDaoSupportBase {
	public ApiUserBean getSlackUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void saveSlackUserAuthKey(int uid, String slackKey){
		invokeStoredProc(ApiUserBean.class, "update_user_slack_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "slackKey", slackKey));
	}
}

