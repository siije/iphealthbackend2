package trunk.modules.slack.maps;

public class SlackChannelsData {

	String id;
	String name;
	long created;
	boolean is_archived;
	boolean is_member;
	int num_members;
	SlackGenericData topic;
	SlackGenericData purpose;
	
	public SlackChannelsData(){
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public boolean isIs_archived() {
		return is_archived;
	}

	public void setIs_archived(boolean is_archived) {
		this.is_archived = is_archived;
	}

	public boolean isIs_member() {
		return is_member;
	}

	public void setIs_member(boolean is_member) {
		this.is_member = is_member;
	}

	public int getNum_members() {
		return num_members;
	}

	public void setNum_members(int num_members) {
		this.num_members = num_members;
	}

	public SlackGenericData getTopic() {
		return topic;
	}

	public void setTopic(SlackGenericData topic) {
		this.topic = topic;
	}

	public SlackGenericData getPurpose() {
		return purpose;
	}

	public void setPurpose(SlackGenericData purpose) {
		this.purpose = purpose;
	}
}
