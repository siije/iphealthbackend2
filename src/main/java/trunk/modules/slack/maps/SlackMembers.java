package trunk.modules.slack.maps;

public class SlackMembers {

	String ok;
	SlackMembersData[] members;
	
	public SlackMembers(){
		
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public SlackMembersData[] getMembers() {
		return members;
	}

	public void setMembers(SlackMembersData[] members) {
		this.members = members;
	}
}
