package trunk.modules.slack.maps;

public class SlackChannelHistoryDataAttachment {

	String fallback;
	String text;
	String pretext;
	String color;
	SlackChannelHistoryDataAttachmentField[] fields;
	
	public SlackChannelHistoryDataAttachment(){
		
	}

	public String getFallback() {
		return fallback;
	}

	public void setFallback(String fallback) {
		this.fallback = fallback;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPretext() {
		return pretext;
	}

	public void setPretext(String pretext) {
		this.pretext = pretext;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public SlackChannelHistoryDataAttachmentField[] getFields() {
		return fields;
	}

	public void setFields(SlackChannelHistoryDataAttachmentField[] fields) {
		this.fields = fields;
	}	
}
