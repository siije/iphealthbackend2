package trunk.modules.slack.maps;

public class SlackFiles {

	String ok;
	SlackFilesData[] files;
	SlackPagingData paging;
	
	public SlackFiles(){
		
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public SlackFilesData[] getFiles() {
		return files;
	}

	public void setFiles(SlackFilesData[] files) {
		this.files = files;
	}

	public SlackPagingData getPaging() {
		return paging;
	}

	public void setPaging(SlackPagingData paging) {
		this.paging = paging;
	}	
}
