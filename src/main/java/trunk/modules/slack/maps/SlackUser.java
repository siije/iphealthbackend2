package trunk.modules.slack.maps;

public class SlackUser {

	String ok;
	SlackMembersData user;
	
	public SlackUser(){
		
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public SlackMembersData getUser() {
		return user;
	}

	public void setUser(SlackMembersData user) {
		this.user = user;
	}
}
