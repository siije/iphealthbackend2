package trunk.modules.slack.maps;

public class SlackMessagesQueryMatch {

	String type;
	SlackChannelsData channel;
	String user;
	String username;
	String ts;
	String text;
	String permalink;
	SlackMessagesQueryPrevNextMatch previous_2; 
	SlackMessagesQueryPrevNextMatch previous;
	SlackMessagesQueryPrevNextMatch next;
	SlackMessagesQueryPrevNextMatch next_2;
	SlackUser userInfo;
	
	public SlackMessagesQueryMatch(){
		
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SlackChannelsData getChannel() {
		return channel;
	}

	public void setChannel(SlackChannelsData channel) {
		this.channel = channel;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public SlackMessagesQueryPrevNextMatch getPrevious_2() {
		return previous_2;
	}

	public void setPrevious_2(SlackMessagesQueryPrevNextMatch previous_2) {
		this.previous_2 = previous_2;
	}

	public SlackMessagesQueryPrevNextMatch getPrevious() {
		return previous;
	}

	public void setPrevious(SlackMessagesQueryPrevNextMatch previous) {
		this.previous = previous;
	}

	public SlackMessagesQueryPrevNextMatch getNext() {
		return next;
	}

	public void setNext(SlackMessagesQueryPrevNextMatch next) {
		this.next = next;
	}

	public SlackMessagesQueryPrevNextMatch getNext_2() {
		return next_2;
	}

	public void setNext_2(SlackMessagesQueryPrevNextMatch next_2) {
		this.next_2 = next_2;
	}

	public SlackUser getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(SlackUser userInfo) {
		this.userInfo = userInfo;
	}
}
