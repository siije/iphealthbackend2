package trunk.modules.slack.maps;

public class SlackChannelHistory {

	String ok;
	String latest;
	SlackChannelHistoryData[] messages;
	
	public SlackChannelHistory(){
		
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public String getLatest() {
		return latest;
	}

	public void setLatest(String latest) {
		this.latest = latest;
	}

	public SlackChannelHistoryData[] getMessages() {
		return messages;
	}

	public void setMessages(SlackChannelHistoryData[] messages) {
		this.messages = messages;
	}
}
