package trunk.modules.slack.maps;

public class SlackFilesData {

	String id;
	String created;
	String timestamp;
	
	String name;
	String title;
	String mimetype;
	String filetype;
	String pretty_type;
	String user;
	
	String mode;
	boolean editable;
	boolean is_external;
	String external_type;
	
	int size;
	
	String url;
	String url_download;
	String url_private;
	String url_private_download;
	
	String thumb_64;
	String thumb_80;
	String thumb_360;
	String thumb_360_gif;
	String thumb_360_w;
	String thumb_360_h;
	
	String permalink;
	String edit_link;
	String preview;
	String preview_highlight;
	int lines;
	int lines_more;
	
	boolean is_public;
	boolean public_url_shared;
	String[] channels;
	String[] groups;
	//String initial_comment;
	int num_stars;
	boolean is_starred;
	
	public SlackFilesData(){
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMimetype() {
		return mimetype;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

	public String getPretty_type() {
		return pretty_type;
	}

	public void setPretty_type(String pretty_type) {
		this.pretty_type = pretty_type;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isIs_external() {
		return is_external;
	}

	public void setIs_external(boolean is_external) {
		this.is_external = is_external;
	}

	public String getExternal_type() {
		return external_type;
	}

	public void setExternal_type(String external_type) {
		this.external_type = external_type;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl_download() {
		return url_download;
	}

	public void setUrl_download(String url_download) {
		this.url_download = url_download;
	}

	public String getUrl_private() {
		return url_private;
	}

	public void setUrl_private(String url_private) {
		this.url_private = url_private;
	}

	public String getUrl_private_download() {
		return url_private_download;
	}

	public void setUrl_private_download(String url_private_download) {
		this.url_private_download = url_private_download;
	}

	public String getThumb_64() {
		return thumb_64;
	}

	public void setThumb_64(String thumb_64) {
		this.thumb_64 = thumb_64;
	}

	public String getThumb_80() {
		return thumb_80;
	}

	public void setThumb_80(String thumb_80) {
		this.thumb_80 = thumb_80;
	}

	public String getThumb_360() {
		return thumb_360;
	}

	public void setThumb_360(String thumb_360) {
		this.thumb_360 = thumb_360;
	}

	public String getThumb_360_gif() {
		return thumb_360_gif;
	}

	public void setThumb_360_gif(String thumb_360_gif) {
		this.thumb_360_gif = thumb_360_gif;
	}

	public String getThumb_360_w() {
		return thumb_360_w;
	}

	public void setThumb_360_w(String thumb_360_w) {
		this.thumb_360_w = thumb_360_w;
	}

	public String getThumb_360_h() {
		return thumb_360_h;
	}

	public void setThumb_360_h(String thumb_360_h) {
		this.thumb_360_h = thumb_360_h;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getEdit_link() {
		return edit_link;
	}

	public void setEdit_link(String edit_link) {
		this.edit_link = edit_link;
	}

	public String getPreview() {
		return preview;
	}

	public void setPreview(String preview) {
		this.preview = preview;
	}

	public String getPreview_highlight() {
		return preview_highlight;
	}

	public void setPreview_highlight(String preview_highlight) {
		this.preview_highlight = preview_highlight;
	}

	public int getLines() {
		return lines;
	}

	public void setLines(int lines) {
		this.lines = lines;
	}

	public int getLines_more() {
		return lines_more;
	}

	public void setLines_more(int lines_more) {
		this.lines_more = lines_more;
	}

	public boolean isIs_public() {
		return is_public;
	}

	public void setIs_public(boolean is_public) {
		this.is_public = is_public;
	}

	public boolean isPublic_url_shared() {
		return public_url_shared;
	}

	public void setPublic_url_shared(boolean public_url_shared) {
		this.public_url_shared = public_url_shared;
	}

	public String[] getChannels() {
		return channels;
	}

	public void setChannels(String[] channels) {
		this.channels = channels;
	}

	public String[] getGroups() {
		return groups;
	}

	public void setGroups(String[] groups) {
		this.groups = groups;
	}

	public int getNum_stars() {
		return num_stars;
	}

	public void setNum_stars(int num_stars) {
		this.num_stars = num_stars;
	}

	public boolean isIs_starred() {
		return is_starred;
	}

	public void setIs_starred(boolean is_starred) {
		this.is_starred = is_starred;
	}
}
