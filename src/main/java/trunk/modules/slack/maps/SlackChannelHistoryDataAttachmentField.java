package trunk.modules.slack.maps;

public class SlackChannelHistoryDataAttachmentField {

	String title;
	String value;
	//boolean short;
	
	public SlackChannelHistoryDataAttachmentField(){
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
