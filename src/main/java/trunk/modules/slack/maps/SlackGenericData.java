package trunk.modules.slack.maps;

public class SlackGenericData {
	
	String value;
	String creator;
	long last_set;
	
	public SlackGenericData(){
		
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public long getLast_set() {
		return last_set;
	}

	public void setLast_set(long last_set) {
		this.last_set = last_set;
	}
}
