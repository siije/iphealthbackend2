package trunk.modules.slack.maps;

// Representation of the Asana authentication token
public class SlackAuthToken {
	
	String access_token;
	String scope;
		
	public SlackAuthToken(){
		
	}
	
	public String getAccess_token() {
		return access_token;
	}
	
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	
	public String getScope() {
		return scope;
	}
	
	public void setScope(String scope) {
		this.scope = scope;
	}
}
