package trunk.modules.slack.maps;

public class SlackMessagesQuery {

	String ok;
	String query;
	SlackMessagesQueryData messages;
	
	public SlackMessagesQuery(){
		
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public SlackMessagesQueryData getMessages() {
		return messages;
	}

	public void setMessages(SlackMessagesQueryData messages) {
		this.messages = messages;
	}
}
