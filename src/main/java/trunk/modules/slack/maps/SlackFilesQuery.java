package trunk.modules.slack.maps;

public class SlackFilesQuery {

	String ok;
	String query;
	SlackFilesQueryData files;
	
	public SlackFilesQuery(){
		
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public SlackFilesQueryData getFiles() {
		return files;
	}

	public void setMessages(SlackFilesQueryData files) {
		this.files = files;
	}
}
