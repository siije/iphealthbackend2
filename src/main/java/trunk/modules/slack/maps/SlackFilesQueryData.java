package trunk.modules.slack.maps;

public class SlackFilesQueryData {

	int total;
	SlackPagingData paging;
	SlackFilesData[] matches;
	
	public SlackFilesQueryData(){
		
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public SlackPagingData getPaging() {
		return paging;
	}

	public void setPaging(SlackPagingData paging) {
		this.paging = paging;
	}

	public SlackFilesData[] getMatches() {
		return matches;
	}

	public void setMatches(SlackFilesData[] matches) {
		this.matches = matches;
	}
}
