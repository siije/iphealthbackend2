package trunk.modules.slack.maps;

public class SlackMembersData {
	
	String id;
	String name;
	boolean deleted;
	String color;
	SlackProfileData profile;	
	boolean is_admin;
	boolean is_owner;
	boolean has_files;
	
	public SlackMembersData(){
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public SlackProfileData getProfile() {
		return profile;
	}

	public void setProfile(SlackProfileData profile) {
		this.profile = profile;
	}

	public boolean isIs_admin() {
		return is_admin;
	}

	public void setIs_admin(boolean is_admin) {
		this.is_admin = is_admin;
	}

	public boolean isIs_owner() {
		return is_owner;
	}

	public void setIs_owner(boolean is_owner) {
		this.is_owner = is_owner;
	}

	public boolean isHas_files() {
		return has_files;
	}

	public void setHas_files(boolean has_files) {
		this.has_files = has_files;
	}
}
