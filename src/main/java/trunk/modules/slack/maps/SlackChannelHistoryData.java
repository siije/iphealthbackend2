package trunk.modules.slack.maps;

public class SlackChannelHistoryData {

	String type;
	String subtype;
	String channel;
	String user;
	String text;
	String ts;
	SlackChannelHistoryDataAttachment[] attachments;
	SlackUser userInfo;
	
	public SlackChannelHistoryData(){
		
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTs() {
		return ts;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public SlackChannelHistoryDataAttachment[] getAttachments() {
		return attachments;
	}

	public void setAttachments(SlackChannelHistoryDataAttachment[] attachments) {
		this.attachments = attachments;
	}

	public SlackUser getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(SlackUser userInfo) {
		this.userInfo = userInfo;
	}	
}
