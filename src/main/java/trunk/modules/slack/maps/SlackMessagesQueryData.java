package trunk.modules.slack.maps;

public class SlackMessagesQueryData {

	int total;
	SlackPagingData paging;
	SlackMessagesQueryMatch[] matches;
	
	public SlackMessagesQueryData(){
		
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public SlackPagingData getPaging() {
		return paging;
	}

	public void setPaging(SlackPagingData paging) {
		this.paging = paging;
	}

	public SlackMessagesQueryMatch[] getMatches() {
		return matches;
	}

	public void setMatches(SlackMessagesQueryMatch[] matches) {
		this.matches = matches;
	}
}
