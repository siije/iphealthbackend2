package trunk.modules.slack.maps;

public class SlackChannels {
	
	String ok;
	SlackChannelsData[] channels;
		
	public SlackChannels(){
		
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}	
	
	public SlackChannelsData[] getChannels() {
		return channels;
	}

	public void setChannels(SlackChannelsData[] channels) {
		this.channels = channels;
	}
}
