package trunk.modules.asana.maps;

public class AsanaTask {
	
	AsanaTaskData data;
	String error;
	AsanaTaskStories stories;
	
	public AsanaTask(){
		
	}

	public AsanaTaskData getData() {
		return data;
	}

	public void setData(AsanaTaskData data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public AsanaTaskStories getStories() {
		return stories;
	}

	public void setStories(AsanaTaskStories stories) {
		this.stories = stories;
	}	
}
