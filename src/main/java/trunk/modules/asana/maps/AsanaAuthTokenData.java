package trunk.modules.asana.maps;

// Representation of the Asana authentication token - data part
public class AsanaAuthTokenData {
	
	long id;
	String name;
	String email;
	
	public AsanaAuthTokenData(){
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
