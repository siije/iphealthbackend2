package trunk.modules.asana.maps;


public class AsanaTaskDataUpdate {

	String name;
	String notes;
	
	public AsanaTaskDataUpdate(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
