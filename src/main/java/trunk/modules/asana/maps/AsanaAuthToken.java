package trunk.modules.asana.maps;

// Representation of the Asana authentication token
public class AsanaAuthToken {
	
	String access_token;
	String token_type;
	int expires_in;
	AsanaAuthTokenData user;
	String refresh_token;
	
	public AsanaAuthToken(){
		
	}
	
	public String getAccess_token() {
		return access_token;
	}
	
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	
	public String getToken_type() {
		return token_type;
	}
	
	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}
	
	public int getExpires_in() {
		return expires_in;
	}
	
	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}
	
	public AsanaAuthTokenData getData() {
		return user;
	}
	
	public void setData(AsanaAuthTokenData user) {
		this.user = user;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}	
}
