package trunk.modules.asana.maps;

public class AsanaTaskUpdate {
	
	AsanaTaskDataUpdate data;
	String error;
	
	public AsanaTaskUpdate(){
		
	}

	public AsanaTaskDataUpdate getData() {
		return data;
	}

	public void setData(AsanaTaskDataUpdate data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}	
}
