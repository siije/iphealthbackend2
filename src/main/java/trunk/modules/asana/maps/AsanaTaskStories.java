package trunk.modules.asana.maps;

public class AsanaTaskStories {
	
	AsanaTaskStoriesData[] data;
	String error;
	
	public AsanaTaskStories(){
		
	}

	public AsanaTaskStoriesData[] getData() {
		return data;
	}

	public void setData(AsanaTaskStoriesData[] data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}	
}
