package trunk.modules.asana.maps;

public class AsanaGenericData {
	
	long id;
	String name;
	
	public AsanaGenericData(){
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
