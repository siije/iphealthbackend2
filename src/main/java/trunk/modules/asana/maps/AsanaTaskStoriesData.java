package trunk.modules.asana.maps;

public class AsanaTaskStoriesData {
	String created_at;
	AsanaGenericData created_by;
	long id;
	String text;
	String type;
	
	public AsanaTaskStoriesData(){
		
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public AsanaGenericData getCreated_by() {
		return created_by;
	}

	public void setCreated_by(AsanaGenericData created_by) {
		this.created_by = created_by;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
