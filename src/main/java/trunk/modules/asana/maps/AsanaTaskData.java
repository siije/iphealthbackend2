package trunk.modules.asana.maps;

public class AsanaTaskData {

	long id;
	AsanaGenericData assignee;
	String assignee_status;
	String created_at;
	boolean completed;
	String completed_at;
	String due_on;
	AsanaGenericData[] followers;
	boolean hearted;
	AsanaGenericData[] hearts;
	String name;
	String notes;
	int num_hearts;
	AsanaGenericData[] projects;
	AsanaGenericData parent;
	AsanaGenericData workspace;
	
	public AsanaTaskData(){
		
	}

	public long getId() {
		return id;
	}

	public void setAssignee(long id) {
		this.id = id;
	}
	
	public AsanaGenericData getAssignee() {
		return assignee;
	}

	public void setAssignee(AsanaGenericData assignee) {
		this.assignee = assignee;
	}

	public String getAssignee_status() {
		return assignee_status;
	}

	public void setAssignee_status(String assignee_status) {
		this.assignee_status = assignee_status;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public String getCompleted_at() {
		return completed_at;
	}

	public void setCompleted_at(String completed_at) {
		this.completed_at = completed_at;
	}

	public String getDue_on() {
		return due_on;
	}

	public void setDue_on(String due_on) {
		this.due_on = due_on;
	}

	public AsanaGenericData[] getFollowers() {
		return followers;
	}

	public void setFollowers(AsanaGenericData[] followers) {
		this.followers = followers;
	}

	public boolean isHearted() {
		return hearted;
	}

	public void setHearted(boolean hearted) {
		this.hearted = hearted;
	}

	public AsanaGenericData[] getHearts() {
		return hearts;
	}

	public void setHearts(AsanaGenericData[] hearts) {
		this.hearts = hearts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getNum_hearts() {
		return num_hearts;
	}

	public void setNum_hearts(int num_hearts) {
		this.num_hearts = num_hearts;
	}

	public AsanaGenericData[] getProjects() {
		return projects;
	}

	public void setProjects(AsanaGenericData[] projects) {
		this.projects = projects;
	}

	public AsanaGenericData getParent() {
		return parent;
	}

	public void setParent(AsanaGenericData parent) {
		this.parent = parent;
	}

	public AsanaGenericData getWorkspace() {
		return workspace;
	}

	public void setWorkspace(AsanaGenericData workspace) {
		this.workspace = workspace;
	}
}
