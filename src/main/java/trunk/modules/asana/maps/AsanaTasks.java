package trunk.modules.asana.maps;

public class AsanaTasks {
	
	AsanaTasksData[] data;
	String error;
	
	public AsanaTasks(){
		
	}

	public AsanaTasksData[] getData() {
		return data;
	}

	public void setData(AsanaTasksData[] data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}	
}
