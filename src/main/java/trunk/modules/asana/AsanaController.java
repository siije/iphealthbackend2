package trunk.modules.asana;

import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.api.utils.ApiInteraction;
import trunk.modules.asana.dao.AsanaDao;
import trunk.modules.asana.maps.AsanaAuthToken;
import trunk.modules.asana.maps.AsanaTask;
import trunk.modules.asana.maps.AsanaTaskDataUpdate;
import trunk.modules.asana.maps.AsanaTaskStories;
import trunk.modules.asana.maps.AsanaTaskUpdate;
import trunk.modules.asana.maps.AsanaTasksData;
import trunk.modules.asana.maps.AsanaTasks;

@Controller
public class AsanaController extends TrunkModuleBase implements
		IAsanaController {

	private String getAuthTokenDB(User u, boolean config) {
		// Get Asana user authentication key from database
		AsanaDao aDao = ApplicationContextProvider.getContext().getBean(
				AsanaDao.class);
		ApiUserBean asanaUser = aDao.getAsanaUserAuthKey(u.getId());

		String accessTokenJson = asanaUser.getAsana_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u, AsanaResult asanaResult,
			boolean renew, String refreshToken) {
		// Generate parameters url
		asanaResult.generateTokenParamsUrl(renew, refreshToken);

		// Get authentication token
		String accessTokenJson = ApiInteraction.getAccessToken(asanaResult);

		// Save if not error, and return response
		if (!accessTokenJson.contains("Error")) {
			Gson gson = new Gson();
			AsanaAuthToken asanaAuthToken = gson.fromJson(accessTokenJson,
					AsanaAuthToken.class);

			AsanaDao aDao = ApplicationContextProvider.getContext().getBean(
					AsanaDao.class);

			if (renew) {
				asanaAuthToken.setRefresh_token(refreshToken);
				accessTokenJson = gson.toJson(asanaAuthToken);
			}

			aDao.saveAsanaUserAuthKey(u.getId(), accessTokenJson);
			return asanaAuthToken.getAccess_token();
		} else {
			// Save error for debug
			return accessTokenJson;
		}
	}

	@Override
	public AsanaResult configAsana(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		AsanaResult asanaResult = new AsanaResult();
		asanaResult.setAuthCode(getAuthTokenDB(u, true));

		// Authentication code can be null or not, verified in the front end
		return asanaResult;
	}

	@Override
	public AsanaResult redirectAsana(String authCode, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		AsanaResult asanaResult = new AsanaResult();
		asanaResult.setAuthCode(authCode);

		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson == null) {
			// Create a new token
			asanaResult.setAccessToken(createRenewSaveAuthTokenDB(u,
					asanaResult, false, null));
		} else {
			// There is a token already
			Gson gson = new Gson();
			AsanaAuthToken asanaAuthToken = gson.fromJson(accessTokenJson,
					AsanaAuthToken.class);

			asanaResult.setAccessToken(asanaAuthToken.getAccess_token());
		}

		// Access Token can be with or without error, verified in the front end
		return asanaResult;
	}

	@Override
	public AsanaTasks tasksAsana(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Asana user authentication key from database
		AsanaResult asanaResult = new AsanaResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		AsanaTasks tasks = new AsanaTasks();
		if (accessTokenJson != null) {
			// Deserialize class into AsanaAuthToken object
			Gson gson = new Gson();
			AsanaAuthToken asanaAuthToken = gson.fromJson(accessTokenJson,
					AsanaAuthToken.class);

			// Get projects
			String projectsJson = ApiInteraction.HTTPRequest(
					asanaResult.getProjectsUrl(), "GET", null, "Bearer ",
					asanaAuthToken.getAccess_token(), null, null, false, null);

			// Verify if token is not expired
			if (projectsJson.contains("Error")
					&& projectsJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, asanaResult,
						true, asanaAuthToken.getRefresh_token());

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get tasks again
					projectsJson = ApiInteraction.HTTPRequest(
							asanaResult.getProjectsUrl(), "GET", null,
							"Bearer ", accessTokenJson, null, null, false, null);

					// Save new authentication token in class for further use
					asanaAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					tasks.setError(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (tasks.getError() != null)
				return tasks;

			// If there is no error, get tasks
			if (!projectsJson.contains("Error")) {
				AsanaTasks projects = gson.fromJson(projectsJson,
						AsanaTasks.class);

				List<AsanaTasksData> tempTasks = new LinkedList<AsanaTasksData>();
				for (AsanaTasksData project : projects.getData()) {
					// Get projects
					String tasksJson = ApiInteraction.HTTPRequest(
							asanaResult.getTasksUrl(), "GET", null, "Bearer ",
							asanaAuthToken.getAccess_token(), "?project="
									+ String.valueOf(project.getId()), null,
							false, null);

					if (!tasksJson.contains("Error")) {
						AsanaTasks tempTasksPart = gson.fromJson(tasksJson,
								AsanaTasks.class);

						for (AsanaTasksData taskData : tempTasksPart.getData()) {
							taskData.setProjectId(project.getId());
							taskData.setProjectName(project.getName());
							tempTasks.add(taskData);
						}

						tasks.setError(null);
					} else {
						tasks.setError(tasksJson + " TASKS");
						return tasks;
					}
				}

				AsanaTasksData[] tempTasksTotal = new AsanaTasksData[tempTasks
						.size()];
				tempTasks.toArray(tempTasksTotal);
				tasks.setData(tempTasksTotal);
			} else {
				tasks.setError(projectsJson + " PROJECTS");
			}
		} else {
			tasks.setError("NO_AUTH_TOKEN");
		}

		// Tasks can be with or without error, verified in the front end
		return tasks;
	}

	@Override
	public AsanaTask taskAsana(String id, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Asana user authentication key from database
		AsanaResult asanaResult = new AsanaResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		AsanaTask task = new AsanaTask();
		if (accessTokenJson != null) {
			// Deserialize class into AsanaAuthToken object
			Gson gson = new Gson();
			AsanaAuthToken asanaAuthToken = gson.fromJson(accessTokenJson,
					AsanaAuthToken.class);

			// Get task
			String taskJson = ApiInteraction.HTTPRequest(
					asanaResult.getTasksUrl() + "/" + id, "GET", null,
					"Bearer ", asanaAuthToken.getAccess_token(), null, null,
					false, null);

			// Verify if token is not expired
			if (taskJson.contains("Error")
					&& taskJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, asanaResult,
						true, asanaAuthToken.getRefresh_token());

				// If no error
				if (!accessTokenJson.contains("Error")) {
					// Get task again
					taskJson = ApiInteraction.HTTPRequest(
							asanaResult.getTasksUrl() + "/" + id, "GET", null,
							"Bearer ", accessTokenJson, null, null, false, null);

					// Save new authentication token in class for further use
					asanaAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					task.setError(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (task.getError() != null)
				return task;

			// If there is no error, get tasks
			if (!taskJson.contains("Error")) {
				task = gson.fromJson(taskJson, AsanaTask.class);

				String taskStoriesJson = ApiInteraction.HTTPRequest(
						asanaResult.getTasksUrl() + "/" + id + "/stories",
						"GET", null, "Bearer ",
						asanaAuthToken.getAccess_token(), null, null, false, null);

				if (!taskStoriesJson.contains("Error")) {
					AsanaTaskStories stories = gson.fromJson(taskStoriesJson,
							AsanaTaskStories.class);

					task.setStories(stories);
					task.setError(null);
				} else {
					task.setError(taskStoriesJson + " TASK STORIES");
				}
			} else {
				task.setError(taskJson + " TASK");
			}
		} else {
			task.setError("NO_AUTH_TOKEN");
		}

		// Tasks can be with or without error, verified in the front end
		return task;
	}

	@Override
	public AsanaTask updateTaskAsana(String id, String name, String notes,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Asana user authentication key from database
		AsanaResult asanaResult = new AsanaResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		AsanaTask task = new AsanaTask();
		if (accessTokenJson != null) {
			// Deserialize class into AsanaAuthToken object
			Gson gson = new Gson();
			AsanaAuthToken asanaAuthToken = gson.fromJson(accessTokenJson,
					AsanaAuthToken.class);

			// Asana task object definition, should be with anonymous type
			AsanaTaskUpdate asanaTask = new AsanaTaskUpdate();
			asanaTask.setData(new AsanaTaskDataUpdate());
			asanaTask.getData().setName(name);
			asanaTask.getData().setNotes(notes);

			// Update task
			String taskUpdateJson = ApiInteraction.HTTPRequest(
					asanaResult.getTasksUrl() + "/" + id, "PUT", asanaTask,
					"Bearer ", asanaAuthToken.getAccess_token(), null, null,
					false, null);

			// Verify if token is not expired
			if (taskUpdateJson.contains("Error")
					&& taskUpdateJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, asanaResult,
						true, asanaAuthToken.getRefresh_token());

				// Save if not error, and return response
				if (!accessTokenJson.contains("Error")) {
					// Update task again
					taskUpdateJson = ApiInteraction.HTTPRequest(
							asanaResult.getTasksUrl() + "/" + id, "PUT",
							asanaTask, "Bearer ", accessTokenJson, null, null,
							false, null);

					// Save new authentication token in class for further use
					asanaAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					task.setError(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (task.getError() != null)
				return task;

			// If there is no error, get task
			if (!taskUpdateJson.contains("Error")) {
				task = gson.fromJson(taskUpdateJson, AsanaTask.class);

				task.setError(null);
			} else {
				task.setError(taskUpdateJson + " TASK UPDATE");
			}
		} else {
			task.setError("NO_AUTH_TOKEN");
		}

		// Tasks can be with or without error, verified in the front end
		return task;
	}

	@Override
	public AsanaTaskStories addCommentTaskAsana(String id, String comment,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Get Asana user authentication key from database
		AsanaResult asanaResult = new AsanaResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		AsanaTaskStories taskStories = new AsanaTaskStories();
		if (accessTokenJson != null) {
			// Deserialize class into AsanaAuthToken object
			Gson gson = new Gson();
			AsanaAuthToken asanaAuthToken = gson.fromJson(accessTokenJson,
					AsanaAuthToken.class);

			// Add comment
			String taskAddCommentJson = ApiInteraction.HTTPRequest(
					asanaResult.getTasksUrl() + "/" + id + "/stories", "POST",
					null, "Bearer ", asanaAuthToken.getAccess_token(), "?text="
							+ URLEncoder.encode(comment),
					"application/x-www-form-urlencoded", false, null);

			// Verify if token is not expired
			if (taskAddCommentJson.contains("Error")
					&& taskAddCommentJson.contains("Not Authorized")) {
				// Renew token
				accessTokenJson = createRenewSaveAuthTokenDB(u, asanaResult,
						true, asanaAuthToken.getRefresh_token());

				// Save if not error, and return response
				if (!accessTokenJson.contains("Error")) {
					// Update task again
					taskAddCommentJson = ApiInteraction.HTTPRequest(
							asanaResult.getTasksUrl() + "/" + id + "/stories",
							"POST", null, "Bearer ",
							asanaAuthToken.getAccess_token(), "?text="
									+ comment,
							"application/x-www-form-urlencoded", false, null);

					// Save new authentication token in class for further use
					asanaAuthToken.setAccess_token(accessTokenJson);
				} else {
					// Save error for debug
					taskStories.setError(accessTokenJson + " RENEW TOKEN");
				}
			}

			// Send if previous error
			if (taskStories.getError() != null)
				return taskStories;

			// If there is no error, get stories again
			if (!taskAddCommentJson.contains("Error")) {
				String taskStoriesJson = ApiInteraction.HTTPRequest(
						asanaResult.getTasksUrl() + "/" + id + "/stories",
						"GET", null, "Bearer ",
						asanaAuthToken.getAccess_token(), null, null, false, null);

				if (!taskStoriesJson.contains("Error")) {
					taskStories = gson.fromJson(taskStoriesJson,
							AsanaTaskStories.class);

					taskStories.setError(null);
				} else {
					taskStories.setError(taskStoriesJson
							+ " TASK ADD - GET STORIES");
				}
			} else {
				taskStories.setError(taskAddCommentJson + " TASK ADD COMMENT");
			}
		} else {
			taskStories.setError("NO_AUTH_TOKEN");
		}

		// Tasks can be with or without error, verified in the front end
		return taskStories;
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/asana.css"));
		ui.addJsFile(new JsFile(ui, "/asana.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-envelope");
		msm.setName("Asana Integration");
		msm.setId("asanaMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/asana.html");
		ssm.setId("asanaMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Tasks");
		ssm2.setHref("/tasks");
		ssm2.setTemplate("/tasks.html");
		ssm2.setId("asanaMainMultiSub2");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
	}
}
