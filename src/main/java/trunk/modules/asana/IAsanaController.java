package trunk.modules.asana;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.asana.maps.AsanaTask;
import trunk.modules.asana.maps.AsanaTaskStories;
import trunk.modules.asana.maps.AsanaTasks;

@RequestMapping("/asana")
@TrunkModuleFeature(id = "asana", displayName = "Asana", enabled = true)
public interface IAsanaController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "Asana Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	AsanaResult configAsana(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "Asana Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	AsanaResult redirectAsana(
			@RequestParam(value = "authCode", required = true) String authCode,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "tasks", displayName = "Asana Tasks")
	@RequestMapping(value = "/tasks", method = RequestMethod.GET)
	@ResponseBody
	AsanaTasks tasksAsana(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "task", displayName = "Asana Task")
	@RequestMapping(value = "/task", method = RequestMethod.GET)
	@ResponseBody
	AsanaTask taskAsana(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "task", displayName = "Asana Update Task")
	@RequestMapping(value = "/task", method = RequestMethod.POST)
	@ResponseBody
	AsanaTask updateTaskAsana(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "notes", required = true) String notes,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "taskComment", displayName = "Asana Add Comment in Task")
	@RequestMapping(value = "/taskComment", method = RequestMethod.POST)
	@ResponseBody
	AsanaTaskStories addCommentTaskAsana(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "comment", required = true) String comment,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
