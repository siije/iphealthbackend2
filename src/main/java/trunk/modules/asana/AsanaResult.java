package trunk.modules.asana;

import trunk.modules.api.utils.ApiResultBase;

public class AsanaResult extends ApiResultBase {
	
	private String tasksUrl = "https://app.asana.com/api/1.0/tasks";
	private String projectsUrl = "https://app.asana.com/api/1.0/projects";

	public AsanaResult() {
		// Initialize variables for Asana API
		appName = "iphealthasana";
		clientId = "21309559657278";
		clientSecret = "3723cd15a731034fe8fb4e9eec6c71f5";
		redirectUrl = "http://localhost:8989/redirectAsana.html";
		baseAuthUrl = "https://app.asana.com/-/oauth_authorize";
		baseTokenUrl = "https://app.asana.com/-/oauth_token";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		StringBuilder str = new StringBuilder();
		str.append(baseAuthUrl);
		str.append("?response_type=code");
		str.append("&app_name=");
		str.append(appName);
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);

		authUrl = str.toString();
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {
		StringBuilder str = new StringBuilder();
		
		if(renew){
			str.append("grant_type=refresh_token");
			str.append("&refresh_token=");
			str.append(refreshToken);
		}
		else
		{
			str.append("grant_type=authorization_code");
			str.append("&code=");
			str.append(authCode);
		}
		
		str.append("&redirect_uri=");
		str.append(redirectUrl);
		str.append("&client_id=");
		str.append(clientId);
		str.append("&client_secret=");
		str.append(clientSecret);

		tokenParamsUrl = str.toString();
	}

	public String getTasksUrl() {
		return tasksUrl;
	}

	public void setTasksUrl(String tasksUrl) {
		this.tasksUrl = tasksUrl;
	}

	public String getProjectsUrl() {
		return projectsUrl;
	}

	public void setProjectsUrl(String projectsUrl) {
		this.projectsUrl = projectsUrl;
	}	
}
