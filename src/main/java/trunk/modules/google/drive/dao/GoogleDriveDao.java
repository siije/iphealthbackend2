package trunk.modules.google.drive.dao;

import java.sql.Types;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.api.beans.ApiUserBean;

@Repository
public class GoogleDriveDao extends JdbcDaoSupportBase {
	public ApiUserBean getGoogleDriveUserAuthKey(int uid) {
		return invokeStoredProc(ApiUserBean.class, "get_user_by_id",
				new StoredProcParam(Types.INTEGER, "uid", uid));		
	}
	
	public void saveGoogleDriveUserAuthKey(int uid, String googledriveKey){
		invokeStoredProc(ApiUserBean.class, "update_user_googledrive_key",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "googledriveKey", googledriveKey));
	}
}

