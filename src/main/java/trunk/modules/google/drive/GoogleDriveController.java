package trunk.modules.google.drive;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.google.gson.Gson;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.JsFile;
import trunk.interceptors.ApplicationContextProvider;
import trunk.modules.api.beans.ApiUserBean;
import trunk.modules.google.drive.dao.GoogleDriveDao;
import trunk.modules.google.drive.maps.GoogleDriveAuthToken;
import trunk.modules.google.drive.maps.GoogleDriveFileMap;

@Controller
public class GoogleDriveController extends TrunkModuleBase implements
		IGoogleDriveController {

	private GoogleAuthorizationCodeFlow getGoogleAuthFlow(
			GoogleDriveResult googleDriveResult, HttpServletRequest req) {

		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, jsonFactory, googleDriveResult.getClientId(),
				googleDriveResult.getClientSecret(),
				Arrays.asList(DriveScopes.DRIVE)).setAccessType("offline")
				.setApprovalPrompt("force").build();

		return flow;
	}

	private Drive getClient(GoogleCredential credential, HttpServletRequest req) {
		try {
			HttpTransport httpTransport = new NetHttpTransport();
			JsonFactory jsonFactory = new JacksonFactory();
			GoogleDriveResult googleDriveResult = new GoogleDriveResult();

			Drive service = new Drive.Builder(httpTransport, jsonFactory,
					credential).setApplicationName(
					googleDriveResult.getAppName()).build();

			return service;
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String getAuthTokenDB(User u, boolean config) {
		// Get Google Drive user authentication key from database
		GoogleDriveDao gdDao = ApplicationContextProvider.getContext().getBean(
				GoogleDriveDao.class);
		ApiUserBean googleDriveUser = gdDao
				.getGoogleDriveUserAuthKey(u.getId());

		String accessTokenJson = googleDriveUser.getGoogledrive_key();

		if (config) {
			if (accessTokenJson != null) {
				return "ALREADY_AUTH";
			} else
				return null;
		} else {
			return accessTokenJson;
		}
	}

	private String createRenewSaveAuthTokenDB(User u,
			GoogleDriveResult googleDriveResult, String code,
			HttpServletRequest req) {

		try {
			// Same functionality
			GoogleAuthorizationCodeFlow flow = getGoogleAuthFlow(
					googleDriveResult, req);

			GoogleTokenResponse response = flow.newTokenRequest(code)
					.setRedirectUri(googleDriveResult.getRedirectUrl())
					.execute();

			Credential credential = flow.createAndStoreCredential(response,
					null);

			// Create new auth token map
			GoogleDriveAuthToken googleDriveAuthToken = new GoogleDriveAuthToken();

			googleDriveAuthToken.setAccess_token(credential.getAccessToken());
			googleDriveAuthToken.setExpires_in(credential
					.getExpirationTimeMilliseconds());
			googleDriveAuthToken.setRefresh_token(credential.getRefreshToken());
			googleDriveAuthToken.setToken_type("Bearer");

			// Save if not error, and return response
			Gson gson = new Gson();
			String accessTokenJson = gson.toJson(googleDriveAuthToken);

			GoogleDriveDao gdDao = ApplicationContextProvider.getContext()
					.getBean(GoogleDriveDao.class);

			gdDao.saveGoogleDriveUserAuthKey(u.getId(), accessTokenJson);
			return accessTokenJson;
		} catch (Exception e) {
			e.printStackTrace();
			// Save error for debug
			return "Error: " + e.getMessage();
		}
	}

	@Override
	public GoogleDriveResult configGoogleDrive(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		GoogleDriveResult googleDriveResult = new GoogleDriveResult();
		String authCode = getAuthTokenDB(u, true);

		googleDriveResult.setAuthCode(authCode);

		// 3 step flow in this case
		if (authCode == null) {
			try {
				String authUrl = getGoogleAuthFlow(googleDriveResult, req)
						.newAuthorizationUrl()
						.setRedirectUri(googleDriveResult.getRedirectUrl())
						.build();

				// Set new auth url based on google auth flow
				googleDriveResult.setAuthUrl(authUrl);

			} catch (Exception e) {
				e.printStackTrace();
				googleDriveResult.setAuthCode("Error:" + e.getMessage());
			}
		}

		// Authentication code can be null or not, verified in the front end
		return googleDriveResult;
	}

	@Override
	public GoogleDriveResult redirectGoogleDrive(String code,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Decode code
		code = URLDecoder.decode(code);

		// Token already granted
		GoogleDriveResult googleDriveResult = new GoogleDriveResult();
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson == null) {
			// Create a new token
			accessTokenJson = createRenewSaveAuthTokenDB(u, googleDriveResult,
					code, req);

			googleDriveResult.setAccessToken(accessTokenJson);

		} else {
			googleDriveResult.setAccessToken(accessTokenJson);
		}

		// Access Token can be with or without error, verified in the front end
		return googleDriveResult;
	}

	@Override
	public List<GoogleDriveFileMap> getFilesGoogleDrive(String id,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<GoogleDriveFileMap> files = new LinkedList<GoogleDriveFileMap>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GoogleDriveAuthToken googleDriveAuthToken = gson.fromJson(
					accessTokenJson, GoogleDriveAuthToken.class);

			try {

				FileList listing = getClient(
						getGoogleCredential(googleDriveAuthToken), req).files()
						.list().setQ("'" + id + "' in parents").execute();

				if (listing != null) {
					for (File file : listing.getItems()) {
						files.add(new GoogleDriveFileMap(
								file.getId(),
								file.getTitle(),
								id,
								file.getDescription(),
								file.getMimeType().contains(
										"application/vnd.google-apps.folder") ? false
										: true));
					}
				}
			} catch (IOException e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken();
					return getFilesGoogleDrive(id, req);
				}

				GoogleDriveFileMap errorEntry = new GoogleDriveFileMap();
				errorEntry.setError("Error:" + e.getMessage());
				files.add(errorEntry);
			}
		} else {
			GoogleDriveFileMap errorEntry = new GoogleDriveFileMap();
			errorEntry.setError("NO_AUTH_TOKEN");
			files.add(errorEntry);
		}

		return files;
	}

	@Override
	public GoogleDriveFileMap parentFolderGoogleDrive(String id,
			HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GoogleDriveAuthToken googleDriveAuthToken = gson.fromJson(
					accessTokenJson, GoogleDriveAuthToken.class);

			try {
				if (id != "root") {
					File file = getClient(
							getGoogleCredential(googleDriveAuthToken), req)
							.files().get(id).execute();

					if (file.getParents().size() >= 1) {
						ParentReference parent = file.getParents().get(0);
						File parentFile = getClient(
								getGoogleCredential(googleDriveAuthToken), req)
								.files().get(parent.getId()).execute();

						return new GoogleDriveFileMap(parent.getId(),
								parentFile.getTitle(), "", "", false);
					} else {
						return null;
					}
				} else {
					return null;
				}
			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken();
					return parentFolderGoogleDrive(id, req);
				}

				System.out.println("Error: " + e.getMessage());
				return null;
			}
		} else {
			System.out.println("NO_AUTH_TOKEN");
			return null;
		}
	}

	@Override
	public List<GoogleDriveFileMap> createFolderGoogleDrive(String id,
			String name, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<GoogleDriveFileMap> files = new LinkedList<GoogleDriveFileMap>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GoogleDriveAuthToken googleDriveAuthToken = gson.fromJson(
					accessTokenJson, GoogleDriveAuthToken.class);

			try {
				// Folders metadata
				File body = new File();
				body.setTitle(name);
				body.setDescription(null);
				body.setMimeType("application/vnd.google-apps.folder");

				// Set the parent folder.
				if (id != null) {
					body.setParents(Arrays.asList(new ParentReference()
							.setId(id)));
				}

				Drive service = getClient(
						getGoogleCredential(googleDriveAuthToken), req);
				service.files().insert(body).execute();

				return getFilesGoogleDrive(id, req);

			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken();
					return createFolderGoogleDrive(id, name, req);
				}

				GoogleDriveFileMap errorEntry = new GoogleDriveFileMap();
				errorEntry.setError("Error: " + e.getMessage());
				files.add(errorEntry);
			}
		} else {
			GoogleDriveFileMap errorEntry = new GoogleDriveFileMap();
			errorEntry.setError("NO_AUTH_TOKEN");
			files.add(errorEntry);
		}

		return files;
	}

	@Override
	public List<GoogleDriveFileMap> renameFolderGoogleDrive(String id,
			String name, HttpServletRequest req) {
		User u = SessionProvider.getUser(req);

		// Set authentication code
		String accessTokenJson = getAuthTokenDB(u, false);
		List<GoogleDriveFileMap> files = new LinkedList<GoogleDriveFileMap>();

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GoogleDriveAuthToken googleDriveAuthToken = gson.fromJson(
					accessTokenJson, GoogleDriveAuthToken.class);

			try {
				if (id.equals("root")) {
					return files;
				} else {
					Drive service = getClient(
							getGoogleCredential(googleDriveAuthToken), req);

					File newFile = new File();
					newFile.setTitle(name);

					Files.Patch patchRequest = service.files().patch(id,
							newFile);
					patchRequest.setFields("title");
					patchRequest.execute();

					File file = service.files().get(id).execute();
					if (file.getParents().size() >= 1) {
						ParentReference parent = file.getParents().get(0);

						return getFilesGoogleDrive(parent.getId(), req);
					}
				}

			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken();
					return renameFolderGoogleDrive(id, name, req);
				}

				GoogleDriveFileMap errorEntry = new GoogleDriveFileMap();
				errorEntry.setError("Error: " + e.getMessage());
				files.add(errorEntry);
			}
		} else {
			GoogleDriveFileMap errorEntry = new GoogleDriveFileMap();
			errorEntry.setError("NO_AUTH_TOKEN");
			files.add(errorEntry);
		}

		return files;
	}

	@Override
	public void downloadFileGoogleDrive(String id, HttpServletRequest req,
			HttpServletResponse res) {
		User u = SessionProvider.getUser(req);

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) {
			// There is a token already
			Gson gson = new Gson();
			GoogleDriveAuthToken googleDriveAuthToken = gson.fromJson(
					accessTokenJson, GoogleDriveAuthToken.class);

			try {
				Drive service = getClient(
						getGoogleCredential(googleDriveAuthToken), req);
				File file = service.files().get(id).execute();

				if (file.getDownloadUrl() != null
						&& file.getDownloadUrl().length() > 0) {
					try {
						HttpResponse resp = service
								.getRequestFactory()
								.buildGetRequest(
										new GenericUrl(file.getDownloadUrl()))
								.execute();

						resp.download(res.getOutputStream());
						res.setHeader("ContentType", "application/octet-stream");
						res.flushBuffer();
					} catch (IOException e) {
						// An error occurred.
						e.printStackTrace();
						System.out.println("Error: " + e.getMessage());
					}
				} else {
					// The file doesn't have any content stored on Drive.
					System.out.println("Error: no content stored on Drive");
				}
			} catch (IOException e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken();
					downloadFileGoogleDrive(id, req, res);
				}

				e.printStackTrace();
				System.out.println("Error: " + e.getMessage());
			}
		}
	}

	@Override
	public String saveFileGoogleDrive(MultipartFile file, String id,
			HttpServletRequest req) {

		User u = SessionProvider.getUser(req);

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) { // There is a token already
			Gson gson = new Gson();
			GoogleDriveAuthToken googleDriveAuthToken = gson.fromJson(
					accessTokenJson, GoogleDriveAuthToken.class);

			try {

				String fileName = file.getOriginalFilename();

				// File's metadata.
				File body = new File();
				body.setTitle(fileName);
				body.setDescription(null);
				body.setMimeType(file.getContentType());

				// Set the parent folder.
				if (id != null) {
					body.setParents(Arrays.asList(new ParentReference()
							.setId(id)));
				}

				InputStreamContent mediaContent = new InputStreamContent(
						file.getContentType(), file.getInputStream());
				mediaContent.setLength(file.getSize());

				Drive service = getClient(
						getGoogleCredential(googleDriveAuthToken), req);
				service.files().insert(body, mediaContent).execute();

				return "OK";
			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken();
					return saveFileGoogleDrive(file, id, req);
				}

				return "Error: " + e.getMessage();
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	@Override
	public String deleteFileFolderGoogleDrive(String id, boolean isFile,
			HttpServletRequest req) {

		User u = SessionProvider.getUser(req);

		// Get authentication code
		String accessTokenJson = getAuthTokenDB(u, false);

		if (accessTokenJson != null) { // There is a token already
			Gson gson = new Gson();
			GoogleDriveAuthToken googleDriveAuthToken = gson.fromJson(
					accessTokenJson, GoogleDriveAuthToken.class);

			try {
				Drive service = getClient(
						getGoogleCredential(googleDriveAuthToken), req);
				service.files().delete(id).execute();

				return "OK";
			} catch (Exception e) {
				if (e.getMessage().contains("401")) { // Not authorized
					RenewAccessToken();
					return deleteFileFolderGoogleDrive(id, isFile, req);
				}

				return "Error: " + e.getMessage() + id;
			}
		} else {
			return "NO_AUTH_TOKEN";
		}
	}

	@Override
	public void initializeUI(ModuleUI ui) {

		ui.addCssFile(new JsFile(ui, "/googledrive.css"));
		ui.addJsFile(new JsFile(ui, "/googledrive.js"));

		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-google");
		msm.setName("Google Drive Integration");
		msm.setId("googledriveMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Authentication");
		ssm.setHref("/");
		ssm.setTemplate("/googledrive.html");
		ssm.setId("googledriveMainMultiSub1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Files");
		ssm2.setHref("/files");
		ssm2.setTemplate("/files.html");
		ssm2.setId("googledriveMainMultiSub2");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
	}

	public GoogleCredential getGoogleCredential(
			GoogleDriveAuthToken googleDriveAuthToken) {
		GoogleDriveResult googleDriveResult = new GoogleDriveResult();
		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		// Same functionality
		GoogleCredential googleCredential = new GoogleCredential.Builder()
				.setClientSecrets(googleDriveResult.getClientId(),
						googleDriveResult.getClientSecret())
				.setJsonFactory(jsonFactory).setTransport(httpTransport)
				.build()
				.setRefreshToken(googleDriveAuthToken.getRefresh_token())
				.setAccessToken(googleDriveAuthToken.getAccess_token());

		return googleCredential;
	}

	public void RenewAccessToken() {
		System.out
				.println("Trying to renew when a user revoke access to the application");
	}
}
