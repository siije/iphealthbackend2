package trunk.modules.google.drive;

import trunk.modules.api.utils.ApiResultBase;

public class GoogleDriveResult extends ApiResultBase {
	
	public GoogleDriveResult() {
		// Initialize variables for Google Drive API
		appName = "iphealthdrive";
		clientId = "363247139036-tfigu85t7k3fr5gk42mfr51nehsqn9ue.apps.googleusercontent.com";
		clientSecret = "nXljXNYx0NjPkQbvwkxPdt8U";
		redirectUrl = "http://localhost:8989/redirectGoogleDrive.html";
		baseAuthUrl = "";
		baseTokenUrl = "";
		authUrl = null;
		tokenParamsUrl = null;
		authCode = null;
		accessToken = null;

		// Explicit call to generateAuthUrl from ApiResultBase
		generateAuthUrl();
	}

	// Override this function to generate the url for authentication
	@Override
	protected void generateAuthUrl() {
		authUrl = null;
	}

	// Override this function to generate the url for exchange token,
	// authentication code must not be null
	@Override
	public void generateTokenParamsUrl(boolean renew, String refreshToken) {		
		tokenParamsUrl = null;
	}
}
