package trunk.modules.google.drive;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.module.ITrunkModuleBase;
import trunk.modules.google.drive.maps.GoogleDriveFileMap;

@RequestMapping("/googledrive")
@TrunkModuleFeature(id = "googledrive", displayName = "Google Drive", enabled = true)
public interface IGoogleDriveController extends ITrunkModuleBase {

	@TrunkModuleFeature(id = "config", displayName = "Google Drive Configuration")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	@ResponseBody
	GoogleDriveResult configGoogleDrive(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "redirect", displayName = "Google Drive Redirect")
	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	@ResponseBody
	GoogleDriveResult redirectGoogleDrive(
			@RequestParam(value = "code", required = true) String code,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "files", displayName = "Google Drive Get Files")
	@RequestMapping(value = "/files", method = RequestMethod.GET)
	@ResponseBody
	List<GoogleDriveFileMap> getFilesGoogleDrive(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "parentFolder", displayName = "Google Drive Get Parent Folder")
	@RequestMapping(value = "/parentFolder", method = RequestMethod.GET)
	@ResponseBody
	GoogleDriveFileMap parentFolderGoogleDrive(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "createFolder", displayName = "Google Drive Create Folder")
	@RequestMapping(value = "/createFolder", method = RequestMethod.POST)
	@ResponseBody
	List<GoogleDriveFileMap> createFolderGoogleDrive(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "renameFolder", displayName = "Google Drive Rename Folder")
	@RequestMapping(value = "/renameFolder", method = RequestMethod.POST)
	@ResponseBody
	List<GoogleDriveFileMap> renameFolderGoogleDrive(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "file", displayName = "Google Drive Download File")
	@RequestMapping(value = "/file", method = RequestMethod.GET)
	@ResponseBody
	void downloadFileGoogleDrive(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);

	@TrunkModuleFeature(id = "file", displayName = "Google Drive Save File")
	@RequestMapping(value = "/file", method = RequestMethod.POST)
	@ResponseBody
	String saveFileGoogleDrive(
			@RequestParam(value = "file", required = true) MultipartFile file,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	@TrunkModuleFeature(id = "deleteFileFolder", displayName = "Google Drive Delete File or Folder")
	@RequestMapping(value = "/deleteFileFolder", method = RequestMethod.POST)
	@ResponseBody
	String deleteFileFolderGoogleDrive(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "isFile", required = true) boolean isFile,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
}
